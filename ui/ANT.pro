#-------------------------------------------------
#
# Project created by QtCreator 2012-02-03T17:37:35
#
#-------------------------------------------------

QT       += core gui

TARGET = ANT
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    hazardview.cpp \
    simworker.cc

HEADERS  += mainwindow.h \
    hazardview.h \
    simworker.h

FORMS    += mainwindow.ui

RESOURCES += \
    ant_icons.qrc
