#ifndef HAZARDVIEW_H
#define HAZARDVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>

class HazardView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit HazardView(QGraphicsScene* scene, QWidget *parent = 0);
             HazardView(QWidget* wid);

signals:

public slots:

protected:
             QGraphicsPixmapItem* m_piTopLeft;
             QGraphicsPixmapItem* m_piBottomRight;

};

#endif // HAZARDVIEW_H
