#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QProcess>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Init parameter checkboxes
    ui->color_red_checkBox->setCheckable(true);
    ui->color_blue_checkBox->setCheckable(true);
    ui->color_yellow_checkBox->setCheckable(true);
    ui->color_green_checkBox->setCheckable(true);
    ui->video_input_checkBox->setCheckable(true);
    ui->video_fuzzy_checkBox->setCheckable(true);
    ui->video_thresh_checkBox->setCheckable(true);

    //Default path parameters
    ui->color_red_checkBox->setChecked(true);
    ui->color_blue_checkBox->setChecked(false);
    ui->color_yellow_checkBox->setChecked(false);
    ui->color_green_checkBox->setChecked(false);

    //Default video parameters
    ui->video_input_checkBox->setChecked(true);
    ui->video_fuzzy_checkBox->setChecked(false);
    ui->video_thresh_checkBox->setChecked(false);

    //Init Graphics

    QGraphicsScene *scene = new QGraphicsScene();
    scene->addPixmap(QPixmap::fromImage(QImage(":/new/logos/ant_logo_med")));
    ui->logo_graphicsView->setScene(scene);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_navigate_Button_clicked()
{
    QString navProgram = "./../run.sh";
    QStringList navArg;
    navArg << "";

    QProcess *navProcess = new QProcess(this);
    navProcess->start(navProgram, navArg);
}

void MainWindow::on_way_Button_clicked()
{
    QString wayProgram = "./../stop.sh";
    QStringList wayArg;
    wayArg << "";

    QProcess *wayProcess = new QProcess(this);
    wayProcess->start(wayProgram, wayArg);
}

void MainWindow::on_stop_Button_clicked()
{
    QString stopProgram = "./../way.sh";
    QStringList stopArg;
    stopArg << "";

    QProcess *stopProcess = new QProcess(this);
    stopProcess->start(stopProgram, stopArg);
}
