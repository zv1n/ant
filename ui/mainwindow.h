#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_navigate_Button_clicked();

    void on_way_Button_clicked();

    void on_stop_Button_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
