#include "hazardview.h"
#include <QGraphicsPixmapItem>

HazardView::HazardView(QGraphicsScene* scene,  QWidget *parent) :
    QGraphicsView(scene, parent)
{
}

HazardView::HazardView(QWidget* wid):
    QGraphicsView(wid)
{
    QGraphicsScene* scene = new QGraphicsScene();

    m_piTopLeft = new QGraphicsPixmapItem();
    m_piTopLeft->setPixmap(QPixmap("./TL.png"));
    m_piTopLeft->setPos(0,0);

    m_piBottomRight = new QGraphicsPixmapItem();
    m_piBottomRight->setPixmap(QPixmap("./BR.png"));
    m_piBottomRight->setPos(10,10);

    scene->addItem(m_piTopLeft);
    scene->addItem(m_piBottomRight);

    setScene(scene);
}
