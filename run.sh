#!/bin/bash

HOSTNAME=$(hostname)

if [ $HOSTNAME == "rover" ]; then
CONFIG=./config/netbook.cfg 
else
CONFIG=./config/rover.cfg 
fi

DEFAULT_RISK_FAR_FILE=./path/to/default

function die() {
	echo $*
	exit 1
}

if [ $# -ne 1 ]; then
	echo "Loading default config: $CONFIG"
else
	CONFIG=$1
	echo "Loading user config: $CONFIG"
fi

lsmod | grep -q garmin
if [ $? -ne 0 ]; then
	sudo modprobe garmin_gps
fi

FTDI_TTY=`find /sys -name ttyUSB* | grep ftdi`
GARMIN_TTY=`find /sys -name ttyUSB* | grep garmin`

ROVER_TTY=`echo $FTDI_TTY | sed -e 's|.*/\(ttyUSB[0-9+]\)|/dev/\1|'`
GPS_TTY=`echo $GARMIN_TTY | sed -e 's|.*/\(ttyUSB[0-9+]\)|/dev/\1|'`

echo FTDI: $FTDI_TTY
echo Rover Device: $ROVER_TTY
echo GARMIN: $GARMIN_TTY
echo GPS Device: $GPS_TTY

if [ $HOSTNAME == "rover-mini" ]; then

#mount | grep -q logs
#if [ $? -ne 0 ]; then
#	mount ~/logs || die "Failed to mount the log dir!!"
#fi

sudo killall gpsd
gpsd -Nn $GPS_TTY&

fi

source $CONFIG || die "Failed to load config!"

OPTS="-p"

if [ "x$ROVER_TTY" == "x" ]; then
	OPTS="${OPTS} -i"
	echo "Running WITHOUT Serial Comms"
	[ $HOSTNAME == "rover-mini" ] && sleep 3
fi

if [ "x$GLOBAL_HAZARD_FILE" != "x" ] && [ -e $GLOBAL_HAZARD_FILE ]; then
	OPTS="${OPTS} -h $GLOBAL_HAZARD_FILE"
fi

if [ "x$WAYPOINT_FILE" != "x" ] && [ -e $WAYPOINT_FILE ]; then
	OPTS="${OPTS} -w $WAYPOINT_FILE"
fi

if [ "x$GPS_TTY" == "x" ]; then
	if [ "x$GPS_FILE" != "x" ] && [ -e $GPS_FILE ]; then
		OPTS="${OPTS} -g $GPS_FILE"
	fi
fi

if [ "x$BEARING" != "x" ]; then
	OPTS="${OPTS} -B $BEARING"
fi

if [ "x$SPEED" != "x" ]; then
	OPTS="${OPTS} -S $SPEED"
fi

#favor video devices over video files
if [ "x$VIDEO_DEVICE" != "x" ]; then
	grep -q video <<< $VIDEO_DEVICE
	if [ $? -ne 0 ]; then
		let DEVICE=$VIDEO_DEVICE 
		OPTS="${OPTS} -d $VIDEO_DEVICE"
	else
		OPTS="${OPTS} -v $VIDEO_DEVICE"
	fi
	echo "Video Device: $VIDEO_DEVICE"
elif [ "x$VIDEO_FILE" != "x" ]; then
	[ -f $VIDEO_FILE ] || die "'$VIDEO_FILE' Doesn't Exist!"
	OPTS="${OPTS} -f $VIDEO_FILE"
fi

#if [ "x$TERRAIN_RISK_PAR_FILE" != "x" ]; then
#	TERRAIN_RISK_PAR_FILE_OPTS="-p $TERRAIN_RISK_PAR_FILE"
#else
#	TERRAIN_RISK_PAR_FILE_OPTS="-p $DEFAULT_RISK_PAR_FILE"
#fi

if [ "x$DEBUG_LEVEL" != "x" ]; then
	OPTS="${OPTS} -l $DEBUG_LEVEL"
fi

if [ "x$FAILURE_LEVEL" != "x" ]; then
	OPTS="${OPTS} -e $FAILURE_LEVEL"
fi

if [ $NO_WAIT -ne 0 ]; then
	OPTS="${OPTS} -n"
fi

if [ "x$ROVER_TTY" != "x" ]; then
	[ -e $ROVER_TTY ] || die "Specified Rover TTY ($ROVER_TTY) does not exist!" 
	OPTS="${OPTS} -t ${ROVER_TTY}"
fi

if [ "x$LOG_CONFIG" != "x" ]; then
	[ -f $LOG_CONFIG ] ||  die "Invalid Log Configuration file specified!"
	OPTS="${OPTS} -c $LOG_CONFIG"
fi

echo "1000000" | sudo tee /proc/sys/kernel/sched_rt_period_us
echo "1000000" | sudo tee /proc/sys/kernel/sched_rt_runtime_us

echo "GPS File: $GPS_FILE"
echo "Video File: $VIDEO_FILE"
echo "PAR File: $TERRAIN_RISK_PAR_FILE"

echo "Running Simplesim..."
echo ./tools/simplesim 	${OPTS}

if [ $MODE == "gdb" ]; then
gdb --args ./tools/simplesim ${OPTS}
elif [ $MODE == "valgrind" ]; then
valgrind ./tools/simplesim ${OPTS}
else
sudo ./tools/simplesim ${OPTS}
fi
