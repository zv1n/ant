{{
┌───────────────────────────────────┬────────────────────────┬────────┬────────┐
│ InputServosRC_v1.0.spin           │ (C)2008 Parallax, Inc. │ V1.0   │ Apr 08 │
├───────────────────────────────────┴────────────────────────┴────────┴────────┤
│ Reads the pulse widths from an R/C radio system while verifying the order    │
│ and timing of the received pulses. Requires one available cog and one I/O    │
│ pin per radio channel.                                                       │
└──────────────────────────────────────────────────────────────────────────────┘

Usage:

 • Create a long array with one long per channel to be monitored plus one long
   to store the 'cnt' value during last valid transmission.
 • Connect all used channels of the R/C radio receiver to a sequential block of
   Propeller I/O pins with the lowest channel of the radio connecting to the 
   lowest numbered I/O pin of the Propeller.   
 • Call Start(@Array, FirstPin, Total) where 'Array' is a pointer to the memory
   array that the 'cnt' value and pulse widths of the last valid transmission 
   will be stored, 'FirstPin' is the number of the lowest I/O pin in the series 
   of pins connected to the radio receiver, and 'Total' is the total number of 
   channels connected from the radio receiver to the Propeller.   
 • Array[0] will return the value that 'cnt' (the global system counter) was at 
   when the last valid transmission was received.
 • Array[n], where 'n' indicates a channel from 1 to 'Total', will return the
   last valid pulse width (in clock cycles) received on that channel.          

  On startup, all values in the specified array are cleared to zero. When valid    
pulses are received on a channel, the pulse widths (measured in clock cycles)
are written to their corresponding channel in the array. Array[1] relates to 
channel 1 and so on. The value of 'cnt' is then stored at Array[0].
  Once a valid pulse is received, the value of Array[0] no longer reads zero, so 
a boolean test of Array[0] can be used to indicate whether or not a valid pulse
has been received since the object was started. To determine the time (in clock 
cycles) that has elapsed since the end of the last valid transmission, subtract 
Array[0] from the current 'cnt' value. (e.g "Time := cnt - Array[0]")
  When using this object, the time elapsed since a valid transmission should be
monitored so that, if radio communication is lost, proper failsafe actions can
be taken to ensure the safe operation of the device under radio control.  
  The constants 'MINIMUM' and 'MAXIMUM' contain the minimum and maximum pulse
widths, in clock cycles, that are considered to be valid. In order for a series 
of pulses to be considered valid, every channel starting at 'FirstPin' and 
ending at 'FirstPin' + 'Total' must receive a valid pulse, and be in sequential 
order. If any channels from the radio receiver to the Propeller are  
disconnected, or connected out of order, no pulse sequences will be considered
valid.
  If the radio receiver being used has a built in failsafe function, it is
recommended that it be set so that, when the signal is lost, it will either 
stop sending servo pulses, or it will cut the throttle, so that a no-signal or
throttle-cut condition can be detected by the parent object.  See the user
manual for the transmitter and receiver for how to modify the failsafe settings. 
}}                                                                           


CON

  MINIMUM       =  60_000
  MAXIMUM       = 180_000

  
VAR

  byte  cog
  long  Stack[40]  



PUB Start(LastValid, FirstPin, PWMcnt, RC_PIN, ROVER_PIN) | index

  if 0 =< PWMcnt and PWMcnt =< 8 and (FirstPin + PWMcnt ) =< 32
    repeat index from 0 to PWMcnt + 2
      long[LastValid][index]~              'zero out array
    'long[LastValid][RC_PIN]~
    'long[LastValid][ROVER_PIN]~
    return cog := cognew(ReadPulses(LastValid, FirstPin, PWMcnt,RC_PIN, ROVER_PIN), @Stack)
  else                          'Start cog if parameters are valid 
    return -1                   'otherwise return -1
                  


PUB Stop                        'Stop the currently running cog, if any

  if !cog                       'If cog was not set to -1
    cogstop(cog)                'Stop the cog


PRI ReadPulses(LastValidPointer, StartPin, PWMcnt, RC_PIN, ROVER_PIN) | i, Pulse[8], StartTime, LastCnt, pin, index

  repeat                                                  'only run when radio transmissions are present  
    waitpeq(0, |< StartPin, 0)                          'For first pulse
    waitpne(0, |< StartPin, 0)                          'Wait for rising edge
    Pulse[0] := cnt                                        'Prepare timer
    waitpeq(0, |< StartPin, 0)                          'Wait for falling edge
    Pulse[0] := (LastCnt := cnt) - Pulse[0]

    if PWMcnt > 0
      repeat i from  1 to PWMcnt-1                    'Read PWM pins
        waitpne(0, |< (StartPin + i), 0)                
        waitpeq(0, |< (StartPin + i), 0)                'Wait for falling edge
        Pulse[i] := cnt - LastCnt                       'Calculate pulse width 
        LastCnt += Pulse[i]
        'if Pulse[i] < MINIMUM or Pulse[i] > MAXIMUM     'If pulse is invalid
        '  Pulse[i] := 181_000
                     'then set invalid flag
   
    if RC_PIN <> 0
      waitpne(0, |< (StartPin + RC_PIN-1), 0)   
      waitpeq(0, |< (StartPin + RC_PIN-1), 0)   'Wait for falling edge
      Pulse[PWMcnt] := cnt - LastCnt                           'Calculate pulse width 
      LastCnt += Pulse[PWMcnt]

      if Pulse[PWMcnt] <= 100_000
        Pulse[PWMcnt] := 1
        
      else
        Pulse[PWMcnt] := 0
      
        
                         
    if ROVER_PIN <> 0        
      waitpne(0, |< (StartPin + ROVER_PIN-1), 0)
      waitpeq(0, |< (StartPin + ROVER_PIN-1), 0)   'Wait for falling edge
      
      Pulse[PWMcnt + 1] := cnt - LastCnt 'should be good 'Calculate pulse width
      LastCnt += Pulse[PWMcnt + 1]
      

      if Pulse[PWMcnt] == 1           
         Pulse[PWMcnt + 1] += Pulse[1]
         Pulse[PWMcnt + 1] -= Pulse[2]
         Pulse[PWMcnt + 1] += Pulse[3]
         Pulse[PWMcnt + 1] -= Pulse[4]  
        'if Pulse[PWMcnt + 1] <= 550_000
          'Pulse[PWMcnt + 1] := 1

      'elseif Pulse[PWMcnt] == 0 and Pulse[PWMcnt + 1] <= 550_000
       ' Pulse[PWMcnt + 1] := 0

      'elseif Pulse[PWMcnt] == 0                                     
        'Pulse[PWMcnt + 1] := 0
            
           
    longmove(LastValidPointer+4, @Pulse, (PWMcnt + 2))      'Write results
    long[LastValidPointer] := cnt | 1                 'and set LastValid



      