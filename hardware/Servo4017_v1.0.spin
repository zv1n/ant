{{
┌───────────────────────────────────┬────────────────────────┬────────┬────────┐
│ Servo4017.spin                    │ (C)2008 Parallax, Inc. │ V1.0   │ Apr 08 │
├───────────────────────────────────┴────────────────────────┴────────┴────────┤
│ Controls up to 8 servos using two Propeller I/O pins and a Johnson decade    │
│ counter (4017 or similar) IC. Requires one available cog and two I/O pins.   │
└──────────────────────────────────────────────────────────────────────────────┘
Schematic:

              
         10KΩ │┌────────┐ 
         │     └┤VDD   Q1├─ Servo 0
 MRPin ─┻──────┤MR    Q2├─ Servo 1
 CPPin ────────┤CP0   Q3├─ Servo 2
                │      Q4├─ Servo 3
                │      Q5├─ Servo 4
                │      Q6├─ Servo 5
              ┌─┤!CP1  Q7├─ Servo 6
              ├─┤VSS   Q8├─ Servo 7
              │ └────────┘
              


Usage:

 • Connect the CP0 and MR pins from a 4017 to two different Propeller I/O pins
   and connect !CP1 to Vss
 • Call Start(CP0, MR) where 'CP' and 'MR' are the Propeller I/O pins that are
   respectively connected to CP0 and MR of the 4017
 • Call Set(Servo, Position) where 'Servo' is a number from 0 to 7 indicating
   which servo will be moved and 'Position' is a number from 60_000 to 180_000
   indicating the position to which the servo will be moved.
 • Call Enable(n) to enable all servos from 1 to 'n'      
}}


CON

    _clkmode = xtal1 + pll16x                           
    _xinfreq = 5_000_000                                


  MINTIME       =    60_000
  CENTER        =   120_000
  MAXTIME       =   180_000
  PERIOD        = 80_000_000 / 50


VAR

  long  Enabled
  long  Cog
  long  CP
  long  MR

  long  Address
  long  ServoData[8]
  long  Stack[40]


PUB Start(CPPin, MRPin) | Index 'Launch the servo engine in a new cog
                                
  CP := CPPin
  MR := MRPin
  Enabled := 0

  repeat Index from 0 to 7
    ServoData[Index] := CENTER

  return Cog := cognew(RunServos, @Stack)


PUB Stop                        'Stop the currently running cog, if any
                                
  if !Cog
    cogstop(Cog)


PUB Enable(Number)              'Enable servos 0 through 'Number' 

    Enabled := Number 

  
PUB Set(Number, Value)          'Set the position of servo 'Number' to 'Value'

    if MINTIME =< Value and Value =< MAXTIME and 0 =< Number and Number =< 7 
      return ServoData[Number] := Value
    

PRI RunServos | Index, NextCNT, StartTime, Iterations

  outa[MR]~~                    'Initialize pin states
  outa[CP]~
  dira[MR]~~
  dira[CP]~~

  NextCNT := MINTIME + cnt      'Initialize NextCNT value
  repeat
    StartTime := NextCNT        'Reset StartTime value
    Iterations := Enabled       'Set number of iterations
    if 1 =< Iterations and Iterations =< 8
      outa[MR]~                 'Ensure that the 4017 is held in not reset                       
      repeat Index from 0 to Iterations - 1
        outa[CP]~               'Falling edge is ignored 
        waitcnt(NextCNT)        'Wait for width of current pulse                 
        outa[CP]~~              'End current and start next servo pulse
        NextCNT += ServoData[Index]

    outa[CP]~                   'End of the last pulse       
    waitcnt(NextCNT)            'Hold the 4017 in reset until
    outa[MR]~~                  'the next pulse train starts 

    NextCNT := StartTime + PERIOD
