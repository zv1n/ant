{{
┌────────────────────────────────────┬────────────────────────┬───────┬────────┐
│ QuadRoverRadioNavigation_v1.0.spin │ (C)2008 Parallax, Inc. │ V1.0  │ Apr 08 │
├────────────────────────────────────┴────────────────────────┴───────┴────────┤
│ Processes inputs for brakes, turns, and forward velocity and then outputs    │
│ controls for the throttle servo as well as a pair of flow cutoff valves, a   │
│ pair of flow reverse valves, and a pair of brake servos for both the left    │
│ and right sides.                                                             │
└──────────────────────────────────────────────────────────────────────────────┘

  Before using the QuadRover, read all of the documentation that is provided for 
the QuadRover itself, as well as the documentation accompanying this program.
There is documentation at the beginning of this program as well as each object  
it uses. Read, understand, and follow the instructions provided. 

 • To use this program, a standard four channel R/C airplane or helicopter radio
   system is required. Channel one of the R/C system should be controlled by the
   right 'X' axis on the radio transmitter, channel two should be controlled by
   the right 'Y' axis, channel three should be controlled by the left 'Y' axis,
   and channel four should be controlled by the left 'X' axis. All axes should 
   be self centering except for the left 'Y' axis, which should hold its  
   position when no force is applied.   
 • Before using this object, follow the directions in 'SetTrim_v1.0.spin' to
   ensure that the R/C transmitter is functional and properly trimmed, then
   follow the directions in 'QuadRoverControl_v1.0.spin' to calibrate the
   throttle and brake servos.
 • Before running the QuadRover, the control setup should be thoroughly tested.
   Firmly mount the QuadRover on a stable elevated platform so that its wheels
   can spin freely and it cannot shake loose. With the engine off but the
   Propeller Control Board completely powered on (Propeller on and servos on), 
   move the joysticks on the R/C transmitter and check for corresponding 
   movements in the servos on the QuadRover. If everything works properly, start 
   the engine and run the same test again making sure that the wheels move as
   expected. Turn the R/C transmitter off and make sure that the engine stalls. 
   After everything is tested, the QuadRover should only be run in a safe, 
   controlled environment.        

  This program uses three of the four axes to control the QuadRover. The left 
'Y' axis controls the brakes. Keep the left joystick in a lowered position and
raise it to activate the brakes. The brakes work independently of all other 
controls, but as the brakes are applied the throttle will be limited.
  The right 'Y' axis controls the velocity of the QuadRover. As the joystick is 
pushed upward the QuadRover will accelerate forward, as the joystick is pushed 
downward the QuadRover will accelerate in reverse.  If the joystick is centered 
the QuadRover will idle.
  The right 'X' axis controls the degree of turning in the QuadRover.  If the
right 'Y' axis is in the center position, movement in the right 'X' axis will
cause the QuadRover to spin in place. If the QuadRover is being driven, either
forward or reverse, movements along the 'X' axis will cause the QuadRover to
make arcing turns while traveling.
  When controlling the QuadRover with an R/C system, care should be taken to
prevent sudden movements on the control sticks. Practice will be needed to 
become proficient at controlling the QuadRover.


            Default R/C transmitter controls:

     Left joystick:           Right joystick:
                               
        Brake on               Full forward
                                    
            │               Arc left │ Arc right   
            │                       \│/
            │          Rotate left ─╋─ Rotate right
            │                       /│\
            │          Arc back left │ Arc back right                                            
            ┴                        
        Brake off              Full reverse
       

}}
CON

  _clkmode = xtal1 + pll16x
  _xinfreq = 5_000_000

  'Should be the same value that the throttle stick was trimmed to in 'SetTrim'
  THROTTLE_CUT  = 160_000

  'Sets the amount of slop that is considered to be the center of an axis
  DEADZONE      = 4_096

  'Maximum time since last reception before a signal is considered invalid
  TIMEOUT       = 80_000_000 / 4
  
  '1.5 ms servo pulse at 80 MHz
  CENTER        = 120_000

  'Servo pulse input pins
  FIRST_CHANNEL = 8
  PWM_CHANNELS  = 4

  TESTB = 0

  'Serial Pins
  SERIAL_TX_PIN = 30
  SERIAL_RX_PIN = 31
  SERIAL_MODE   = 0
  SERIAL_BAUD   = 9_600
  
  'Radio input channels
  LAST_VALID    = 0             'cnt time at end of last transmission
  RIGHT_X       = 1             'Aileron
  RIGHT_Y       = 2             'Elevator
  LEFT_Y        = 3             'Throttle
  LEFT_X        = 4             'Rudder
  RC_ENABLE     = 5
  ROVER_ENABLE  = 6

  'Handshake
  'HANDSHAKE_KEY = 0F0F
   
OBJ

  Radio            : "InputServosRC_v1.0.spin"
  QuadRoverControl : "QuadRoverControl_v1.0.spin"
  Serial           : "FullDuplexSerial.spin"

VAR
 long recv_index

PUB Main | RadioData[PWM_CHANNELS + 3] , i, rxByte, brake, turn, throttle, radio_err, servo_err, key
{{
  Starts 'InputServosRC' and 'QuadRoverControl' in seperate cogs, 
  continuously reads in data from the radio transmitter, processes
  it, then outputs it to 'QuadRoverControl' 
}}

  radio_err := Radio.Start(@RadioData, FIRST_CHANNEL, PWM_CHANNELS, RC_ENABLE, ROVER_ENABLE)
  QuadRoverControl.Start

  ' Start the radio receiver and QuadRover controlling cogs
  Serial.start(SERIAL_RX_PIN, SERIAL_TX_PIN, SERIAL_MODE, SERIAL_BAUD)
  Serial.rxflush

  if radio_err == -1
    repeat
      Serial.str(string("radio error "))

  RadioData[RC_ENABLE] := 0

{{epeat
    ' Initial Handshaking
    key = Serial.rx_int32(1000);
    if(key == HANDSHAKE_KEY)
        quit
}}

  ' Continuosly read in radio data, format it, and send it to 'QuadRoverControl'  
  repeat          
    ' If the signal is valid, the inputs from 'InputServosRC' are processed then
    ' sent to 'QuadRoverControl'     
    'if RadioData[ROVER_ENABLE]

      ' Enters continuous loop until the radio transmission has ended. This
      ' provides a RC overide for the rover at any point in time
      if RadioData[RC_ENABLE] <> 0        
        ' When the left joystick is completely lowered, if the trim was
        ' correctly set, it will read slightly less than the throttle-cut point.
        ' The following equation makes that point zero, and any upward
        ' movement in the joystick will create a reduction in the position value
        ' returned, which will create a corresponding increase in the brake value
        ' that is sent to 'QuadRoverControl'.  If the joystick is below the
        ' throttle-cut point, this equation will return a negative value, and
        ' 'QuadRoverControl' will cut the throttle to the engine.
        QuadRoverControl.lock
        brake := (THROTTLE_CUT - RadioData[LEFT_Y]) / 2
        QuadRoverControl.SetBrake(brake)
         
        ' The right joystick's 'X' axis is shifted so that the center value is
        ' zero, instead of 120,000 as is returned by 'InputServosRC'.
        ' dead-zone correction is applied, then it is sent to 'QuadRoverControl'
        turn := DeadzoneCorrect(RadioData[RIGHT_X] - CENTER)
        QuadRoverControl.SetTurn(turn)
         
        ' The right joystick's 'Y' axis is shifted so that the center value is
        ' zero, instead of 120,000 as is returned by 'InputServosRC',
        ' dead-zone correction is applied, then it is sent to 'QuadRoverControl'
        throttle := DeadzoneCorrect(-RadioData[RIGHT_Y] + CENTER)
        QuadRoverControl.SetVelocity(throttle)
        QuadRoverControl.unlock
        {Serial.str(string("Radio: "))
        Serial.dec(cnt)
        Serial.str(string(", "))
        Serial.dec(brake)
        Serial.str(string(", "))
        Serial.dec(turn)
        Serial.str(string(", "))
        Serial.dec(throttle)
        Serial.str(string(", "))
        Serial.dec(RadioData[RC_ENABLE])
        Serial.str(string(", "))
        Serial.dec(RadioData[ROVER_ENABLE])
        Serial.newline
         }
     
      if Serial.rxpeek <> -1    
        ReceiveSerialCommmand(RadioData[RC_ENABLE])
{         
    else
      ' If the signal is invalid, set the brake to a negative number to force a
      ' throttle-cut condition 
      QuadRoverControl.SetBrake(-1)
 }                                                         
    'Serial.str(string("I am waiting for a command"))
  
     

    ' Wait for 1/50th of a second so that the loop is not run more often than 
    ' 'InputServosRC' can read in servo positions or 'QuadRoverControl' can
    ' output them 
    waitcnt(clkfreq / 50 + cnt)

PUB DeadzoneCorrect(Value)
{{
  Creates a deadzone at the center of the axis where the value will always read
  zero. As the input value moves from the center of the axis, the output value
  returned will be the distance from the edge of the deadzone, instead of the
  distance from the center of the axis    
}}

  if Value > DEADZONE           ' If 'Value' is greater than 'DEADZONE' return                                            
    Value -= DEADZONE           ' the difference between 'Value' and 'DEADZONE'  
  elseif Value < -DEADZONE      ' If 'Value' is less than 'DEADZONE' return        
    Value += DEADZONE           ' the difference between 'Value' and '-DEADZONE'
  else                          ' If value is within the deadzone return 0
    Value~                      

  return Value


PUB ReceiveSerialCommmand(enable) | eStop, brake, turn, throttle, err, ms, rxByte
{{
    Recieves and parses commands from serial port
}}
  ms := 2000                     
  rxByte := Serial.rxcheck   
  if rxByte == $3A                
     eStop := Serial.rx_int16(ms)
     brake := Serial.rx_int16(ms)
     turn := Serial.rx_int16(ms)
     throttle := Serial.rx_int16(ms)

     err := 0
     
     if eStop == -1
        err := 1
     elseif brake == -1
        err := 2
     elseif turn == -1
        err := 3
     elseif throttle == -1
        err := 4
     elseif enable == 1
        err := $ff

     {{if turn != 16000 || throttle != 16000
        if  TESTB
          brake = 0;
        else
           brake = 32000
        TESTB = !TESTB
}}
     if err == 0                       
        Serial.tx($3A)       
        Serial.tx(eStop)    
        Serial.tx(eStop>>8)    
        Serial.tx(brake)   
        Serial.tx(brake>>8)        
        Serial.tx(turn)   
        Serial.tx(turn>>8)        
        Serial.tx(throttle)   
        Serial.tx(throttle>>8)
                              
        if eStop
          EmergencyStop
        else
          QuadRoverControl.lock
          QuadRoverControl.SetBrake(brake)
          QuadRoverControl.SetVelocity(throttle)
          QuadRoverControl.SetTurn(turn)
          QuadRoverControl.unlock

     else                            
        Serial.str(string("#")) ' Return a # when the timer elapses for one or more character   
        Serial.tx(err)

PUB EmergencyStop
{{
   Kills the throttle and gradually applies the brakes to bring the rover to a stop.
   Should also ignore all commands until the propeller is restarted. Writes string "!!"
   to signal that rover has stopped and not to transmit any more commands.
}}

  QuadRoverControl.SetBrake(-1)
  
