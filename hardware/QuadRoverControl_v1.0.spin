{{
┌───────────────────────────────────┬────────────────────────┬────────┬────────┐
│ QuadRoverControl_v1.0.spin        │ (C)2008 Parallax, Inc. │ V1.0   │ Apr 08 │
├───────────────────────────────────┴────────────────────────┴────────┴────────┤
│ Processes inputs for brakes, turn, and forward velocity and then outputs     │
│ controls for the throttle servo as well as pairs of flow cutoff valves, flow │
│ reverse valves, and brake servos for both the left and right sides.          │
└──────────────────────────────────────────────────────────────────────────────┘

To calibrate the 'IDLE', 'STALL', 'LBRAKEOFFSET', and 'RBRAKEOFFSET' constants, 
follow these instructions: 

 • When testing the QuadRover, keep it elevated and firmly mounted on a
   workbench so that the wheels can spin freely.
 • Before using this object, follow the directions in 'SetTrim_v1.0.spin' to
   ensure that the R/C transmitter is functional and properly trimmed.  
 • The constants 'IDLE', 'STALL', 'LBRAKEOFFSET', and 'RBRAKEOFFSET' indicate an
   offset from the servo's center position. Generally, the servos are mounted so
   that their center position applies no brake and no throttle, and an increase
   in position creates a corresponding increase in brake or throttle. Because of
   variation in servos and mounting positions, these constants may need to be
   changed.
 • An increase in 'IDLE' increases the position that the servo holds while the 
   engine is idle. All throttle movements are relative to the 'IDLE' position, 
   so any change in 'IDLE' will be reflected across the entire power curve.   
 • To test changes made to this object, save it, then compile and program
   'QuadRoverRadioNavigation_v1.0.spin' into the Propeller Control Board. This
   object needs to be in the same folder as 'QuadRoverRadioNavigation_v1.0.spin'
   
  After the trims on the R/C transmitter have been set using SetTrim_v1.0, and
the QuadRover has been firmly mounted on an elevated platform so the wheels can        
spin freely, load 'QuadRoverRadioNavigation_v1.0.spin' into EEPROM on the
Propeller Control Board on the QuadRover. With the Propeller Control Board 
completely turned on, (Propeller on and servos on) turn the radio on. The 
throttle should move from the 'STALL' position to the 'IDLE' when the QuadRover 
receives a signal. Turn the radio back off. The throttle should move from the 
'IDLE' position to the 'STALL' when the signal is lost. Once the QuadRover's 
ability to detect a no-signal condition has been verified, the engine can be 
started. Turn the radio back on so that the servo is in the 'IDLE' position. 
Follow the instructions in the manufacturers manual to start the engine. If the 
engine stalls too easily, 'IDLE' is set to low. If the wheels begin to turn 
while the throttle servo is held at idle, 'IDLE' is set to high. Adjust the idle 
point by changing the number following "IDLE =" in the 'CON' section below. An 
increase will give the engine more throttle while it is idling and a decrease 
will give the engine less throttle. Make adjustments in the range of 1,000 to 
2,000 units at a time, and test the idle point again. To test changes made to the  
'IDLE' constant, save this file, then load 'QuadRoverRadioNavigation_v1.0.spin' 
into EEPROM on the QuadRover. Changes will only take effect if this file is in 
the same folder as 'QuadRoverRadioNavigation_v1.0.spin'.
  Once a valid idle point has been set, test the stall function by turning the
transmitter off while the engine is running. The engine should stall when the
signal is lost. If the engine does not stall, change the number following
"STALL =" in the 'CON' section below. Changes to the 'STALL' constant should be
made in a similar manner as changes to 'IDLE'. Again, to apply changes, this 
file should be saved and 'QuadRoverRadioNavigation_v1.0.spin' must be loaded 
into the QuadRover's EEPROM. To test changes to 'STALL', start the engine with 
the R/C transmitter on, then while the engine is idling turn the R/C transmitter 
off. The engine should stall.  If not, make more adjustments, reprogram, and 
test again. Negative numbers can be used if 'IDLE' or 'STALL' are to high.
  To test the brakes, turn on the R/C transmitter, completely turn on the 
Propeller Control Board, and leave the engine off. Move the left joystick on the 
R/C transmitter all the way up. Both brakes should engage. Try manually turning 
each wheel on the QuadRover to test the brakes. If the brake on either side does 
not fully engage, increase the 'LBRAKEOFFSET' or 'RBRAKEOFFSET' constants to
increase the braking power for the left or right brake respectively. After the
constants are changed, follow the steps described above to reprogram the new
constants for the QuadRover.       
        
 
}}
CON

  'Calibration values
  IDLE          = 5_000         
  STALL         = 0             
  LBRAKEOFFSET  = 0
  RBRAKEOFFSET  = 0
  MAX_THROTTLE  = 25_000        

  '1.5 ms servo pulse at 80 MHz
  CENTER        = 120_000

  'Servo driver output pins
  CLK_4017      = 23   
  RES_4017      = 22

  'Servo output channels
  THROTTLE      = 0
  BRAKE_L       = 1
  BRAKE_R       = 2

  'Solenoid output pins
  L_OFF         = 0
  R_OFF         = 1
  L_REV         = 2
  R_REV         = 3

  POSX_16       =  32_767
  NEGX_16       = -32_768

  MAXDELTA      = 25           'Maximum ramping speed, higher = faster ramping


OBJ

  Servo            : "Servo4017_v1.0.spin"


VAR

  byte  Cog
  byte  ThrottleEnabled
  byte  SemID

  long  Stack[40]

  long  NewVelocity
  long  NewTurn
  long  Brake



PUB Start                       ' Run in a new cog
  SemID := locknew
  return Cog := cognew(Main, @Stack)
  

PUB Stop                        ' Stop the currently running cog, if any
                                
  if !Cog
    cogstop(Cog)


PUB SetVelocity(Velocity)       ' Set the QuadRover's forward velocity
{
 Velocity is a 16-bit signed integer; positive is forward, negative is reverse
 Values beyond -32_768 to 32_767 will be limited to the maximum range.
}

  NewVelocity := NEGX_16 #> Velocity <# POSX_16
  ThrottleEnabled~~


PUB SetTurn(Turn)               ' Set how much the QuadRover is turning    
{
 Turn is a 16-bit signed integer; positive is left, negative is right 
 Values beyond -32_768 to 32_767 will be limited to the maximum range.
}

  NewTurn := NEGX_16 #> Turn <# POSX_16


PUB SetBrake(NewBrake)          ' Set how much the QuadRover is braking
{
 Turn is a 16-bit signed integer; zero is off, 32,767 is full on
 Negative values cut the throttle but do not modify the brake position
 Values beyond -32_768 to 32_767 will be limited to the maximum range.
}
  
  if NewBrake > 0
    Brake := 0 #> NewBrake <# POSX_16
  else
    ThrottleEnabled~

PUB lock

  repeat until not lockset(SemID)

PUB unlock

  lockclr(SemID)

PRI Main | LeftBrake, RightBrake, Fuel, TurnFlag, Velocity, Turn, TurnD

  ' Start the servo output cog and center all servos
  Servo.Start(CLK_4017, RES_4017)
  Servo.Set(THROTTLE, CENTER + STALL)
  Servo.Set(BRAKE_L, CENTER)
  Servo.Set(BRAKE_R, CENTER)
  ' but only activate the throttle servo
  Servo.Enable(1)

  dira[L_OFF]~~
  dira[R_OFF]~~
  dira[L_REV]~~
  dira[R_REV]~~

  ' Clear 'ThrottleEnabled', 'Velocity', 'Turn', and 'Brake'
  ThrottleEnabled := Velocity := Turn := Brake~
  
  repeat
    ' Limit rate of change in the 'Velocity' and 'Turn' positions to prevent
    ' quick changes in throttle and reversal of the flow valves while the
    ' QuadRover is moving
    lock
    Velocity += (NewVelocity - Velocity) <# MAXDELTA #> (-MAXDELTA)  

    TurnD := (NewTurn - Turn)
    TurnD <#= MAXDELTA
    TurnD #>= (-MAXDELTA)
    Turn += TurnD

    ' TurnFlag will be '1' if turning left, '0' if turning right    
    TurnFlag := Turn >> 15

    ' Both brakes will be set to at least the value in 'Brake'
    LeftBrake := RightBrake := Brake
    
    ifnot Velocity              ' Not moving            
      Fuel := ||Turn
      outa[R_OFF] := outa[L_OFF]~
      if Turn                   ' Turning in place
        outa[R_REV] := !outa[L_REV] := TurnFlag  
    else                        ' Moving
      Fuel := (||Velocity + ||Turn) <# POSX_16
      outa[R_REV] := outa[L_REV] := (Velocity >> 15) 
      ifnot Turn                ' Driving straight
        outa[R_OFF] := outa[L_OFF]~
      else
        outa[R_OFF] := !outa[L_OFF] := TurnFlag   
        if Turn > 0             ' Turning right
          RightBrake #>= Turn
        else                    ' Turning left
          LeftBrake #>= -Turn
     
    ' Limit the throttle if the brakes are applied
    Fuel <#= MAX_THROTTLE <# (POSX_16 - Brake)
    
    if ThrottleEnabled
      Servo.Enable(3)
      Servo.Set(BRAKE_L, constant(CENTER - LBRAKEOFFSET) - LeftBrake)
      Servo.Set(BRAKE_R, constant(CENTER + RBRAKEOFFSET) + RightBrake)
      Servo.Set(THROTTLE, constant(CENTER + IDLE) + ||Fuel)
    else
      Servo.Set(THROTTLE, constant(CENTER + STALL))
      Velocity := Turn~
    unlock
