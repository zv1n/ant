{{
┌───────────────────────────────────┬────────────────────────┬────────┬────────┐
│ SetTrim_v1.0.spin                 │ (C)2008 Parallax, Inc. │ V1.0   │ Apr 08 │
├───────────────────────────────────┴────────────────────────┴────────┴────────┤
│ Runs InputServosRC.spin and displays data from a standard four channel       │ 
│ airplane or helicopter radio control system.                                 │ 
└──────────────────────────────────────────────────────────────────────────────┘

To set the trim settings on an R/C radio transmitter, follow these instructions: 

Setup:

 • Using 10KΩ series protection resistors, connect channels one through four,
   from an RC receiver, to four consecutive Propeller I/O pins. Channel one
   should be connected to the lowest numbered I/O pin in the series and the
   constant 'FIRST_CHANNEL' should be set to that I/O pin.  Channels two through
   four should be connected incrementally to the next tree I/O pins.
 • To use this program, press F11 to load it to EEPROM, then, with a serial
   terminal, open the com port that is connected to the Propeller Control Board
   at 9,600 baud with 8 data bits, 1 stop bit, and no parity.

Usage:
n
  With the radio transmitter turned off and a terminal program connected to the
Propeller, the following text will be displayed:

  No Signal
  Right X: Not set
  Right Y: Not set
  Left  Y: Not set
  Left  X: Not set

  When a radio signal is present, the top line will display the elapsed time
since the last valid signal was received, otherwise the text "No Signal" will be
displayed. The four lines following display the 'X' and 'Y' coordinates of the
right and left joysticks on the R/C radio transmitter.
  When the radio transmitter is turned on and a valid signal is received, the
text "No Signal" will be replaced with "Time since last signal:" followed by the
number of clock cycles that have passed since the last valid signal was
received. Following the left and right 'X' and 'Y' labels, the current positions
of the R/C transmitter joysticks' axes will be displayed. The values represent
the pulse widths that the radio receiver is sending to the Propeller. A value of
120,000 indicates a 1.5 ms pulse and a centered joystick. Following the position
value, a label is shown indicating the direction of the joystick such as "Up",
"Down", "Left", "Right", or "Center".
  Individually move each joystick up, down, left, then right and make sure that
the text displayed matches the actual position of the joystick. If the displayed 
movement is opposite the actual movement on any axis, that axis is being  
inverted by the transmitter. Follow the instructions provided by the 
manufacturer of the R/C transmitter to set that axis to a non-inverting state.
  Once all four axes are set to be non-inverted, the trims can be set. With the
right joystick in the center position, adjust the 'X' and 'Y' axis trims on the
radio transmitter until the corresponding positions reported at the terminal are
as close to 120,000 as possible. Then, with the left joystick centered along the
'X' axis, adjust the 'X' axis trim until its position is as close as possible to
120,000. Then move the left joystick down until it is in the lowest physical
position on its 'Y' axis. (The positional value reported on the terminal will
increase as the joystick is moved downward; this is normal.) With the joystick
in the lowered position, adjust the trim downward until the text "Throttle-cut"
is displayed as the position of the left 'Y' axis. Move the trim up slightly, so
that "Throttle-cut" does not appear. The numerical position of the axis should
be just under 160,000.
  Turn the radio off and make sure that the number listed after "Time since last
signal:" climbs steadily. If the radio receiver being used has certain built-in 
failsafe functions, the receiver may continue to provide position pulses to the  
Propeller, even though it is not really a valid signal. When this occurs, the  
Propeller will not be able to detect that the signal was lost. If this is the  
case, the receiver should be set so that, when a signal is lost, it will either 
stop sending servo pulses or it will cut the throttle. Follow the instructions 
provided by the manufacturer of the R/C transmitter to ensure that thereceiver  
will either stop driving the servos or cut the throttle when the signal is lost;   
then run this program again to ensure that when the radio transmitter is turned 
off either the "Time since last signal" value climbs or the text "Throttle-cut"
appears.
}}

  
CON

  _clkmode = xtal1 + pll16x
  _xinfreq = 5_000_000

  'Position 
  THROTTLE_CUT  = 160_000

  'Maximum time since last reception before a signal is considered invalid
  TIMEOUT       = 80_000_000 / 4

  'Servo pulse input pins
  FIRST_CHANNEL = 8
  CHANNELS      = 4
  
  'Radio input channels
  LAST_VALID    = 0             'cnt time at end of last transmission
  RIGHT_X       = 1             'Aileron
  RIGHT_Y       = 2             'Elevator
  LEFT_Y        = 3             'Throttle
  LEFT_X        = 4             'Rudder


  '1.5 ms servo pulse at 80 MHz
  CENTER        = 120_000
  LOW           = CENTER - 10_000  
  HIGH          = CENTER + 10_000

VAR
  long  val

OBJ

  Term          : "FullDuplexSerial.spin"
  Radio         : "InputServosRC_v1.0(1).spin"


PUB Main | RadioData[CHANNELS + 1]

  'Start InputServosRC.spin and a serial terminal
  Radio.Start(@RadioData, FIRST_CHANNEL, CHANNELS)
  Term.Start(31, 30, 0, 9_600)

  Term.tx(0)
  repeat  'check for a valid signal
    if RadioData and (cnt - RadioData) < TIMEOUT

      'Show the number of clock cycles since the last transmission
      Term.str(string(1, "Time since last signal: "))
      Term.dec(cnt - RadioData)

      'Show the readings of all four radio channels
      Term.str(string(13, "Right X: "))
      ShowX(RadioData[RIGHT_X])
      Term.str(string(13, "Right Y: "))
      ShowY(RadioData[RIGHT_Y])
      Term.str(string(13, "Left  Y: "))
      if RadioData[LEFT_Y] > THROTTLE_CUT               'Check for throttle cut
        Term.str(string("Throttle-cut"))    
      else
        ShowY(RadioData[LEFT_Y])
      Term.str(string(13, "Left  X: "))
      ShowX(RadioData[LEFT_X])
      val := 0
      repeat
        val := val+1
      until val == 50000
    else 'If there isn't a vlid signal, report it
      Term.str(string(1, "No Signal                          "))
                 

PUB ShowX(Value)                'Format and display 'X' position of a joystick

  ifnot Value 
    Term.str(string("Not set"))
  else
    Term.dec(Value)
    Term.str(string("       ", 9))
    Case Value
      1..LOW:
        Term.str(string("Left  "))
      constant(LOW + 1)..constant(HIGH - 1):
        Term.str(string("Center"))
      other:      
        Term.str(string("Right "))


PUB ShowY(Value)                'Format and display 'Y' position of a joystick

  ifnot Value 
    Term.str(string("Not set"))
  else
    Term.dec(Value)
    Term.str(string("       ", 9))
    Case Value
      1..LOW:
        Term.str(string("Up    "))
      constant(LOW + 1)..constant(HIGH - 1):
        Term.str(string("Center"))
      other:      
        Term.str(string("Down  "))
