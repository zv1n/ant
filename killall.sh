#!/bin/bash

pids=$(ps aux | grep simplesim | awk '{print $2}')

for pid in $pids; do

sudo kill -9 $pid

done
