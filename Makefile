.PHONY: all

all:
	-@$(MAKE) -C nmealib 
	@$(MAKE) -C ant
#	@$(MAKE) -C ui
	-@$(MAKE) -C tools 

clean:
	-@$(MAKE) -C nmealib clean
	@$(MAKE) -C ant clean
#	-@$(MAKE) -C ui clean
	-@$(MAKE) -C tools clean
