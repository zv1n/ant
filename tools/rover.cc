#include <ant/comm_serial.h>
#include <ant/commandset_rover.h>

#include <opencv/highgui.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <ant/debug.h>
#include <unistd.h>
#include <stdlib.h>
#include <termios.h>

using namespace ant;
using namespace std;

void
msleep(float sec)
{
    int seconds = (int)sec;
    sec -= (float)seconds;
    sec *= 1000000;
    sleep(seconds);
    usleep(sec);
}

#define LEVEL(cmd) (cmd & 0x00FF)

enum KB_CMDS {
    KB_QUIT = 0x0100,
    KB_CLEAR = 0x0200,
    KB_SETLEVEL = 0x0300,
    KB_FWD = 0x0400,
    KB_REV = 0x0500,
    KB_LEFT = 0x0600,
    KB_RIGHT = 0x0700,
    KB_NONE = 0x0800,
    KB_MASK = 0xFF00
};

static char
getch(void)
{
    char buf = 0;
    struct termios old;
	memset(&old, 0, sizeof(old));
    if (tcgetattr(0, &old) < 0)
        perror("tcsetattr()");
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0)
        perror("tcsetattr ICANON");
    if (read(0, &buf, 1) < 0)
        perror ("read()");
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0)
        perror ("tcsetattr ~ICANON");
    return (buf);
}

int
get_key_command()
{
    int ch = getch();

    if (ch >= '1' && ch <= '9') {
        ch -= '1';
        ch |= KB_SETLEVEL;
        return ch;
    }

    if (ch == 0x1b && getch() == 0x5b) {
        ch = getch();

        if (ch == 0x41)
            return KB_FWD;
        if (ch == 0x42)
            return KB_REV;
        if (ch == 0x43)
            return KB_RIGHT;
        if (ch == 0x44)
            return KB_LEFT;
    }

    if (ch == ' ')
        return KB_CLEAR;

    if (ch == 'q')
        return KB_QUIT;

    return KB_NONE;
}


int main(int argc, char** argv)
{

    comm_serial serial;
    commandset_rover command_set(&serial);
    int n = 0;
    string command;
    int16_t es = 0;
    int16_t b = 0;
    int16_t t = 0;
    int16_t th = 0;
    char* cmd_path = NULL;
//	ant::std.set_level(ant::ALL);
//	ant::err.set_level(ant::ALL);

    enum OPTS { VERBOSE = 0x1, KEYBOARD_MODE = 0x2 };
    struct {
        char* path;
        int baud;
        int parity;
        int stopbits;
        int ops;
    } options = {NULL, 9600, 0, 1, 0};

    int ch = 0;

    if(argc < 2) {
        cout<<"usage: rover [-v]  [-b baud] [-k] [-p parity] [-s stop bits] [-o command file] serial-port"<<endl;
        return 0;
    }

    while ((ch = getopt(argc, argv, "vb:p:s:o:k")) != -1) {
        switch(ch) {
        case 'v':
            options.ops |= VERBOSE;
            break;
        case 'b':
            options.baud = atoi(optarg);
            break;
        case 'k':
            options.ops |= KEYBOARD_MODE;
            break;
        case 'p':
            if (strcasecmp(optarg, "even") == 0)
                options.parity = 1;
            else if (strcasecmp(optarg, "odd") == 0)
                options.parity = 2;
            else
                options.parity = 0;
            break;
        case 's':
            options.stopbits = atoi(optarg);
            break;
        case 'o':
            cmd_path = optarg;
            break;
        }
    }

    argv += optind;
    argc -= optind;

    if (argc < 1) {
        cout<<"usage: rover [-v] [-k] [-b baud] [-p parity] [-s stop bits] [-o output file] serial-port"<<endl;
        exit(1);
    }

    options.path = argv[0];

    n = serial.init(options.path);
    if(n < 1) {
        cout<<"unable to open serial port."<<endl;
        return 1;
    }

    if(cmd_path) {
        sleep(1);
        int linecount = 0;
        ifstream infile;
        infile.open(cmd_path);
        string line = "";
        while(getline(infile, line)) {
            stringstream line_stream(line);
            string cell = "";
            if (line.length() > 0 && line[0] == '#')
                continue;
            linecount++;
            for(int i = 0; i < 5; i++) {
                getline(line_stream, cell, ',');
                // note: the last cell must be terminated by a ','
                // or else there will be undefined behavior at the
                // atoi call
                switch(i) {
                case 0:
                    command_set.set_estop((int16_t)atoi(cell.c_str()));
                    break;
                case 1:
                    command_set.set_brake((int16_t)atoi(cell.c_str()));
                    break;
                case 2:
                    command_set.set_turn((int16_t)atoi(cell.c_str()));
                    break;
                case 3:
                    command_set.set_throttle((int16_t)atoi(cell.c_str()));
                    std::cout  << "Pass: "<< linecount << std::endl;
                    command_set.sendCommand();
                    break;
                case 4:
                    float sleep_time = atof(cell.c_str());
                    if(sleep > 0) {
                        cout<<"sleeping for "<<sleep_time
                            <<" seconds until execution of next command."
                            <<endl;
                        msleep(sleep_time);
                    }
                    break;
                }
            }
        }
    } else if ((options.ops & KEYBOARD_MODE) == KEYBOARD_MODE) {
        int cmd = 0;
        float brake = 1.0;
        float turn = 0;
        float throttle = 0;
        float level = 0;

        printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        printf("Keyboard Control Mode:\n");
        printf("\tArrow keys initiate a direction.\n");
        printf("\tSpace clears the current command and set the brakes.\n");
        printf("\tNumbers 1-9 will set throttle.\n");
        printf("\t\t1 is 0%% throttle.\n");
        printf("\t\t9 is 100%% throttle.\n");
        printf("\tPress 'q' to quit!\n");
        printf("Example: 5, Up, Right: Half power forward right-arching turn.\n");
        printf("Example: 3, Right: Low power right-skid turn.\n\n");

        while(1) {
read:
            cmd = get_key_command();
            switch(cmd&KB_MASK) {
            case KB_CLEAR:
                brake = 1.0f;
                throttle = 0;
                turn = 0;
                break;
            case KB_SETLEVEL:
                level = (float)LEVEL(cmd) / 8.0f;
                if (level == 0.0f)
                    level = 0.01f;
                printf("Set Level: %f\n", level);
                goto read;
            case KB_FWD:
                turn = 0.0;
                brake = 0.0;
                throttle = level;
                break;
            case KB_REV:
                turn = 0.0;
                brake = 0.0;
                throttle = -level;
                break;
            case KB_LEFT:
                turn = -level;
                brake = 0.0;
                break;
            case KB_RIGHT:
                turn = level;
                brake = 0.0;
                break;
            case KB_QUIT:
                return 0;
            case KB_NONE:
                goto read;
            }

            printf("Sending:\n");
            printf("Throttle: %f\n", throttle);
            printf("Turn: %f\n", turn);
            printf("Brake: %f\n", brake);
            command_set.set_estop(0);
            command_set.set_brakef(brake);
            command_set.set_bearing(turn);
            command_set.set_speed(throttle);
            command_set.sendCommand();
        }
    } else {
        while(1) {
            cout<<endl<<"enter emergency stop, brake, turn, throttle:"<<endl;
            cout<<"emergency stop:";
            cin>>es;
            command_set.set_estop(es);
            cout<<"brake:";
            cin>>b;
            command_set.set_brake(b);
            cout<<"turn:";
            cin>>t;
            command_set.set_turn(t);
            cout<<"throttle:";
            cin>>th;
            command_set.set_throttle(th);
            /*	send_err =*/
            command_set.sendCommand();
            //	if(send_err != 0)
            //	    cout<<"sendCommand error number: "<<send_err<<endl;

        }
    }
    return 0;
}
