#include <sys/time.h>
#include <sys/resource.h>

#include <iostream>
#include <signal.h>

#include <ant/filter.h>
#include <ant/collector.h>
#include <ant/input.h>
#include <ant/processor.h>
#include <ant/fuzzy_control.h>
#include <ant/collection.h>
#include <ant/sim_controller.h>
#include <ant/log_manager.h>
#include <ant/comm_serial.h>
#include <ant/commandset_rover.h>
#include <ant/rover_controller.h>
#define DEBUGGING 0
#include <ant/debug.h>

#include <ant/enums.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <getopt.h>

using namespace std;
using namespace cv;

ant::collection g_output_collection;
ant::collection g_control_collection;

bool g_term_signal = false;
int g_waitval = 0;

void
term_handler(int sig)
{
	(void) sig;
	g_term_signal = true;
}

struct sim_options {
    const char* app_name;
    int log_level;
    int err_level;
    int device;
	std::string devstring;
    int nosse;
    const char* log_file;
    const char* err_file;

    int nodelay;
    int simulation;
    int ignoreserialerrors;
	int opencvcap;
	int profiling;
	int width;
	int height;
	float forceSpeed;
	float forceBearing;

    const char* lcfile;
    const char* media_file;
    const char* gps_file;
	const char* haz_file;
	const char* waypoint_file;
    const char* tty_path;
};

struct sim_collectors {
	ant::controller* rc;
	ant::collector* gpsc;
	ant::collector* navc;
	ant::collector* ghc;
	ant::filter* ghi;
	ant::filter* video;
}; 

static void
usage(const struct sim_options& opts, int res)
{
    printf("usage: %s [-n] [-l level] [-e level] [-o log file] [-z err file]\n"
           "\t[-c log config file] [-g GPS file] [-h hazard file] [-i] [-s] \n"
           "\t[-d device|-f media file]\n", opts.app_name);
    printf("usage: %s [-g GPS file] <media file>\n", opts.app_name);
    printf("Options:\n");
    printf("  -f\t\t-Path to desired media file.\n");
    printf("  -g\t\t-Path to desired GPS file.\n");
	printf("  -h\t\t-Path to hazard file.\n");
	printf("  -w\t\t-Path to waypoints file.\n");
    printf("  -d\t\t-Number of desired capture device.\n");
    printf("  -n\t\t-No Delay - Start video feed immediately.\n");
    printf("  -c\t\t-Log config file to use.\n");
    printf("  -i\t\t-Ignore serial connection errors.\n");
    printf("  -s\t\t-Run as a simulation.\n");
    printf("  -l level\t-Set log file minimum level.\n");
    printf("  -e level\t-Set err file minimum level.\n");
    printf("  -o path\t-Set log file path (default: stdout).\n");
    printf("  -z path\t-Set err file path (default: stderr).\n");
    printf("  -t path\t-Set path to tty.\n");
    printf("Levels:\n");
    printf("Name\t\t\tValue\tDescription\n");
    printf("  ALL\t\t\t0\tAll output is printed (ULTRA VERBOSE).\n");
    printf("  TLDR\t\t\t1\tAlmost all output is printed.\n");
    printf("  MINOR\t\t\t2\tLess pertinent output is printed.\n");
    printf("  MAJOR\t\t\t3\tPertinent output is printed.\n");
    printf("  DANGER_WILL_ROBINSON\t4\tOnly critical output is printed (DEFAULT).\n");
    printf("  NONE\t\t\t>4\tNo debug output is printed.\n");
    exit(res);
}

const char* g_level_strs[] =
{ "ALL", "TLDR", "MINOR", "MAJOR", "DANGER_WILL_ROBINSON", "NONE" };

const int g_ls_cnt = sizeof(g_level_strs)/sizeof(*g_level_strs);

/**
 * Retreive the value associated with a given name.
 */
static int
get_level(const char* str)
{
    if (!str)
        return -1;

    if (isdigit(str[0]))
        return atoi(str);

    for (int i=0; i<g_ls_cnt; i++) {
        if (strcasecmp(g_level_strs[i], str) == 0)
            return i;
    }

    return ant::MAJOR;
}

#if 0
/**
 * Retreive the name associated with a given value.
 */
static const char*
get_level_str(int level)
{
    if (level < 0 || level >= g_ls_cnt)
        return NULL;

    return g_level_strs[level];
}
#endif

static int
parse_opts(int argc, char** argv, struct sim_options& opts)
{
    int ch = 0;

    while ((ch = getopt(argc, argv, "nipsuv:l:c:o:S:B:e:z:w:f:d:g:h:t:x:y:"))
						 != -1) {
        switch(ch) {
        default:
        case '?':
            usage(opts, 1);
            break;
        case 'n':
            opts.nodelay = 1;
            break;
        case 'p':
            opts.profiling = 1;
            break;
		case 'u':
			opts.nosse = 1; 
			break;
        case 'i':
            opts.ignoreserialerrors = 1;
            break;
        case 'v':
            opts.devstring = optarg;
            break;
        case 'x':
            opts.width = atoi(optarg);
            break;
        case 'y':
            opts.height = atoi(optarg);
            break;
        case 's':
            opts.simulation = 1;
            break;
        case 'd':
			opts.opencvcap = 1;
            opts.device = atoi(optarg);
            break;
        case 'g':
            opts.gps_file = optarg;
            break;
		case 'w':
			opts.waypoint_file = optarg;
			break;
		case 'h':
			opts.haz_file = optarg;
			break;
        case 'c':
            opts.lcfile = optarg;
            break;
        case 'f':
            opts.media_file = optarg;
			g_waitval = 0;
            break;
        case 'S':
            opts.forceSpeed = atof(optarg);
            break;
        case 'B':
            opts.forceBearing = atof(optarg);
            break;
        case 'l':
            opts.log_level = get_level(optarg);
            break;
        case 'e':
            opts.err_level = get_level(optarg);
            break;
        case 'o':
            opts.log_file = optarg;
            break;
        case 'z':
            opts.err_file = optarg;
			break;
        case 't':
            opts.tty_path = optarg;
			break;
        }
    }

    argc -= optind;
    argv += optind;
    std::cout << argc << std::endl;

    if (opts.device > -1 && opts.media_file != NULL) {
        fprintf(stderr, "-d and -f are mutually exclusive!\n");
        usage(opts, 1);
    }

    if (!opts.waypoint_file) {
        fprintf(stderr, "-w MUST be provided!\n");
        usage(opts, 1);
    }

    ant::std.set_level(opts.log_level);
    ant::std.set_file(opts.log_file);
    ant::err.set_level(opts.err_level);
    ant::err.set_file(opts.err_file);

    return 0;
}

static void
add_haz_components(ant::processor& proc, struct sim_collectors& scs, 
							const struct sim_options& opts)
{
    scs.ghi = ant::create_input("global_hazard_input");
    if (!scs.ghi) {
        FAIL(ant::MAJOR, "Failed to create Global Hazard input object!\n");
        usage(opts, 1);
    }
    scs.ghi->set_name("GlobHazardInput");
	string haz_path = opts.haz_file;

	if (scs.ghi->set_property("Path", haz_path) != VALID) {
        FAIL(ant::MAJOR, "Failed to load Global Hazard input file!\n");
        usage(opts, 1);
	}

    scs.ghc = ant::create_collector("global_hazard_collector");
    if (!scs.ghc) {
        FAIL(ant::MAJOR, "Failed to create Global Hazard collector object!\n");
        usage(opts, 1);
    }
    scs.ghc->set_name("GlobHazardCollector");

    scs.ghc->set_property_const("Tap", 1);

    if (scs.ghc->set_property<ant::filter>("Hazard Input", scs.ghi)) {
        FAIL(ant::MAJOR, "Failed to set GH input!\n");
        usage(opts, 1);
    }

	if (!scs.gpsc) {
        FAIL(ant::MAJOR, "Failed to set nav collector object!\n");
        usage(opts, 1);
	}

	ant::filter* filt = scs.gpsc;
    if (scs.ghc->set_property<ant::filter>("GPS Input", filt)) {
        FAIL(ant::MAJOR, "Failed to set GH gps collector input!\n");
        usage(opts, 1);
    }

	filt = scs.navc;
    if (scs.ghc->set_property<ant::filter>("Nav Input", filt)) {
        FAIL(ant::MAJOR, "Failed to set GH nav collector input!\n");
        usage(opts, 1);
    }

    if (scs.ghc->set_property_const<ant::collection>("Output",
            &g_output_collection)) {
        FAIL(ant::MAJOR, "Failed to set Global Hazard collector output!\n");
        usage(opts, 1);
    }

	proc.add_collector(scs.ghc);
}

static void
add_nav_components(ant::processor& proc, struct sim_collectors& scs,
                   const struct sim_options& opts)
{
    scs.navc = ant::create_collector("nav_collector");
    if (!scs.navc) {
        FAIL(ant::MAJOR, "Failed to set Navigation collector object!\n");
        usage(opts, 1);
    }

	string waypoints = opts.waypoint_file;
	
	if (scs.navc->set_property("Path", waypoints) != VALID) {
        FAIL(ant::MAJOR, "Failed to set Navigation waypoints!\n");
        usage(opts, 1);
	}

    ant::filter* gpsc_filter = static_cast<ant::filter*>(scs.gpsc);

    scs.navc->set_name("NavCollector");
    if (scs.navc->set_property<ant::filter>("Navigation Input", gpsc_filter)) {
        FAIL(ant::MAJOR, "Failed to set Navigation collector input!\n");
        usage(opts, 1);
    }

    if (scs.navc->set_property_const<ant::collection>("Output",
            &g_output_collection)) {
        FAIL(ant::MAJOR, "Failed to set Navigation collector output!\n");
        usage(opts, 1);
    }

    proc.add_collector(scs.navc);

	printf("Configuring Global Hazards...\n");
	add_haz_components(proc, scs, opts);

}

static void
add_gps_components(ant::processor& proc, struct sim_options& opts, struct sim_collectors& scs)
{
    ant::filter* gpsi = NULL;
    if (opts.gps_file) {
        gpsi = ant::create_input("gps_sim");
		if (!gpsi) {
            FAIL(ant::MAJOR, "Failed to get GPS Sim object!\n");
            usage(opts, 1);
		}
        if (gpsi->set_property_const<std::string>("Path", opts.gps_file)) {
            FAIL(ant::MAJOR, "Failed to set GPS Sim Path!\n");
            usage(opts, 1);
        }
    } else
        gpsi = ant::create_input("gps");


    proc.add_input(gpsi);

    scs.gpsc = ant::create_collector("gps_collector");
    if (!scs.gpsc) {
        FAIL(ant::MAJOR, "Failed to set GPS collector object!\n");
        usage(opts, 1);
    }

    scs.gpsc->set_name("GPSRaw");
    if (scs.gpsc->set_property<ant::filter>("GPS Input", gpsi)) {
        FAIL(ant::MAJOR, "Failed to set GPS collector input!\n");
        usage(opts, 1);
    }

    if (scs.gpsc->set_property_const<ant::collection>("Output",
            &g_output_collection)) {
        FAIL(ant::MAJOR, "Failed to set GPS collector output!\n");
        usage(opts, 1);
    }

    proc.add_collector(scs.gpsc);

    printf("Configuring Navigation... \n");
    add_nav_components(proc, scs, opts);
}

static void
add_video_components(ant::processor& proc, struct sim_collectors& scs, 
		const struct sim_options& opts)
{

    ant::filter* input = NULL;
    if (opts.media_file != NULL) {
        DEBUG(MAJOR, "Using media file:\n%s\n", opts.media_file);
        input = ant::create_input("media");
		if(!input) {
            FAIL(ant::MAJOR, "Failed to create media input!\n");
            usage(opts, 1);
        }
        if (input->set_property_const<std::string>("Path", opts.media_file)) {
            FAIL(ant::MAJOR, "Failed to set media file!\n");
            usage(opts, 1);
        }
    } else if (opts.device > -1) {
        input = ant::create_input("videoin");
		if(!input) {
            FAIL(ant::MAJOR, "Failed to create video input!\n");
            usage(opts, 1);
        }
        if (input->set_property_const<int>("Device", opts.device)) {
            FAIL(ant::MAJOR, "Failed to set device!\n");
            usage(opts, 1);
        }
    } else  {
        input = ant::create_input("video_v4l");
		if(!input) {
            FAIL(ant::MAJOR, "Failed to create v4l input!\n");
            usage(opts, 1);
        }
		
		input->set_property_const<int>("Width", opts.width);
		input->set_property_const<int>("Height", opts.height);
		input->set_property_const<int>("SSE", opts.nosse == 0);

		if (!opts.devstring.length()) {
            FAIL(ant::MAJOR, "Invalid device string is specified!\n");
            usage(opts, 1);
		}

        if (input->set_property_const<std::string>("Device", opts.devstring)) {
            FAIL(ant::MAJOR, "Failed to set video device!\n");
            usage(opts, 1);
		}
	}

    input->set_property_const("Tap", 1);
    proc.add_input(input);

    ant::filter* erode = ant::create_filter("erode");
    if (!erode) {
        FAIL(ant::MAJOR, "Failed to create smooth filter!\n");
        usage(opts, 1);
    }

    erode->set_property<ant::filter>("Image", input);
    erode->set_property_const("Tap", 0);
    proc.add_filter(erode);
    ant::filter* smooth = ant::create_filter("smooth");
    if (!smooth) {
        FAIL(ant::MAJOR, "Failed to create smooth filter!\n");
        usage(opts, 1);
    }

    smooth->set_property<ant::filter>("Image", erode);
    smooth->set_property_const<int>("Type", ANT_GAUSSIAN);
    smooth->set_property_const<int>("Param1", 7);
    smooth->set_property_const<int>("Param2", 7);
    smooth->set_property_const<double>("Param3", 0.0);
    smooth->set_property_const<double>("Param4", 0.0);
    smooth->set_property_const("Tap", 0); 

    proc.add_filter(smooth);
/*
        ant::filter* filter = ant::create_filter("gray");
        if (!filter) {
            FAIL(ant::MAJOR, "Failed to create gray filter!\n");
            usage(opts, 1);
        }
 //   proc.add_filter(smooth);*/
    ant::filter* filter = ant::create_filter("hsv");
    if (!filter) {
        FAIL(ant::MAJOR, "Failed to create hsv filter!\n");
        usage(opts, 1);
    }

    filter->set_property<ant::filter>("Image", smooth);
    filter->set_property_const("Tap", 0);
    proc.add_filter(filter);

    /*
        ant::filter* thresh = ant::create_filter("dynamic_threshold");
        if (!thresh) {
            FAIL(ant::MAJOR, "Failed to create dynamic_thresh filter!\n");
            usage(opts, 1);
        }
        thresh->set_property<ant::filter>("Image", filter);
        thresh->set_property_const("Tap", 0);
        proc.add_filter(thresh);
    */
    ant::filter* thresh = ant::create_filter("hsv_threshold");
    if (!thresh) {
        FAIL(ant::MAJOR, "Failed to create hsv_thresh filter!\n");
        usage(opts, 1);
    }
    thresh->set_property<ant::filter>("Image", filter);
    thresh->set_property_const("Tap", 0);
    thresh->set_property_const("Red", 1);
	thresh->set_property_const("Orange", 1);
    thresh->set_property_const("Blue", 1);
    thresh->set_property_const("Yellow", 1);
    thresh->set_property_const("Green", 0);
    proc.add_filter(thresh);
	scs.video = thresh;

    ant::collector* collect = ant::create_collector("region_avgstd");
    if (!collect) {
        FAIL(ant::MAJOR, "Failed to create Region AVG/STD Collector!\n");
        usage(opts, 1);
    }

    collect->set_property<ant::filter>("Image", thresh);
    if (collect->set_property_const<ant::collection>("Output",
            &g_output_collection)) {
        FAIL(ant::MAJOR, "Failed to set Region Collector COllection!\n");
        usage(opts, 1);
    }
    collect->set_property_const("Tap", 0);
    proc.add_collector(collect);

    if(opts.simulation) {
        ant::sim_controller* sim = new ant::sim_controller();
        sim->set_property<ant::filter>("Image", input);
        if (sim->set_property_const<ant::collection>("Input",
                &g_control_collection)) {
            FAIL(ant::MAJOR, "Failed to set Simulation Controller input "
                 "collection!\n");
            usage(opts, 1);
        }

        sim->set_property_const("Tap", 0);
        proc.add_controller(sim);
    }
}

static void
add_fc_components(ant::fuzzy_controller& ctrl, ant::processor& proc,
                  struct sim_collectors& scs, const struct sim_options& opts)
{
    if (proc.set_fuzzy_control(&ctrl))  {
        FAIL(ant::MAJOR, "Failed to set Fuzzy Controller!\n");
        usage(opts, 1);
    }

    ant::fuzzifier* fuz = ant::create_fuzzifier("gps_bearing_fuzzifier");
    if (!fuz)  {
        FAIL(ant::MAJOR, "Failed to set GPS Bearing Fuzzifier!\n");
        usage(opts, 1);
    }

    ctrl.add_fuzzifier(fuz);

    fuz = ant::create_fuzzifier("image_bearing_fuzzifier");
    if (!fuz)  {
        FAIL(ant::MAJOR, "Failed to set Image Bearing Fuzzifier!\n");
        usage(opts, 1);
    }

    ctrl.add_fuzzifier(fuz);

    fuz = ant::create_fuzzifier("global_hazard_fuz");
    if (!fuz)  {
        FAIL(ant::MAJOR, "Failed to set Image Bearing Fuzzifier!\n");
        usage(opts, 1);
    }

    ctrl.add_fuzzifier(fuz);

    fuz = ant::create_fuzzifier("image_speed_fuzzifier");
    if (!fuz)  {
        FAIL(ant::MAJOR, "Failed to set Image Speed Fuzzifier!\n");
        usage(opts, 1);
    }

    ctrl.add_fuzzifier(fuz);

    ant::defuzzifier* def = ant::create_defuzzifier("bearing_defuzzifier");
    if (!def) {
        FAIL(ant::MAJOR, "Failed to create Bearing Defuzzifier!\n");
        usage(opts, 1);
    }

    ctrl.add_defuzzifier(def);

    def = ant::create_defuzzifier("speed_defuzzifier");
    if (!def) {
        FAIL(ant::MAJOR, "Failed to create Bearing Defuzzifier!\n");
        usage(opts, 1);
    }

    ctrl.add_defuzzifier(def);

    if(!opts.simulation) {
        printf("Generating rover controller on %s\n", opts.tty_path);
        ant::comm_serial* serial = new ant::comm_serial();
        if(!serial) {
            FAIL(ant::MAJOR, "Failed to allocate serial port!\n");
            usage(opts,1);
        }

        std::string serialstr = "";
        if (opts.tty_path)
            serialstr = opts.tty_path;

        if(serial->init(serialstr) < 1) {
            FAIL(ant::MAJOR, "Failed to open serial port!\n");
            if (!opts.ignoreserialerrors)
                usage(opts,1);
        }

        ant::commandset_rover* cmds_r = new ant::commandset_rover(serial);
        if(!cmds_r) {
            FAIL(ant::MAJOR, "Failed to allocate cset_rover!\n");
            usage(opts,1);
        }

        scs.rc = new ant::rover_controller(cmds_r);
        if(!scs.rc) {
            FAIL(ant::MAJOR, "Failed to allocate Rover Controller!\n");
            usage(opts,1);
        }
    	scs.rc->set_property_const("Tap", 1);

        if (scs.rc->set_property_const<ant::collection>("Input",
                &g_control_collection)) {
            FAIL(ant::MAJOR, "Failed to set Rover Controller input "
                 "collection!\n");
            usage(opts, 1);
        }

		if (opts.forceSpeed >= -100.0)
			scs.rc->set_property_const<float>("ForceSpeed", opts.forceSpeed);
		if (opts.forceBearing >= -100.0)
			scs.rc->set_property_const<float>("ForceBearing", opts.forceBearing);

        if (scs.rc->set_property<ant::filter>("Image", scs.video)) {
            FAIL(ant::MAJOR, "Failed to set Rover Controller input "
                 "image!\n");
            usage(opts, 1);
		}

		ant::filter* rc_filter = static_cast<ant::filter*>(scs.rc);

		if(scs.navc->set_property<ant::filter>("Speed Input", rc_filter))
		{
		 	FAIL(ant::MAJOR, "Failed to set Speed input\n");
			usage(opts,1);
		}

        proc.add_controller(scs.rc);
    }
}

static void
insert_feedback_loop(struct sim_collectors& scs, const struct sim_options& opts)
{
	 ant::filter* rc_filter = static_cast<ant::filter*>(scs.rc);

	if (scs.navc->set_property<ant::filter>("Speed Input", rc_filter))
	{
		FAIL(ant::MAJOR, "Failed to set Speed input\n");
		usage(opts,1);
	}
}

int
main(int argc, char** argv)
{
	ant::init();

	signal(SIGABRT, term_handler);
	signal(SIGTERM, term_handler);
	signal(SIGINT, term_handler);
	signal(SIGSEGV, term_handler);

    g_output_collection.set_name("Video Output");
    g_control_collection.set_name("Control Output");

    ant::processor handler;
    ant::fuzzy_controller fctrl;
    ant::log_manager ss_log_manager(LMM_PASSIVE);

    fctrl.set_input(&g_output_collection);
    fctrl.set_output(&g_control_collection);

    struct sim_options opts = {
        argv[0], ant::DANGER_WILL_ROBINSON, ant::MINOR, -1,"",0,"stdout", 
		"stderr",0,0,0,0,0,640,480,0.0f,0.0f,0,0,0,0,0,0
    };
	
	opts.forceSpeed = -1000.0;
	opts.forceBearing = -1000.0;

    if (parse_opts(argc, argv, opts))
        usage(opts, 1);

    if (opts.lcfile) {
        int ret = ss_log_manager.load_from_file(opts.lcfile);

        if (ret < 0) {
            printf("Failed to load log config file: %s\n", opts.lcfile);
            usage(opts, 1);
        }

        ant::record::set_log_manager(&ss_log_manager);
    }

	handler.set_profiling(opts.profiling);

	struct sim_collectors scs = {0, 0, 0, 0, 0, 0};

    printf("Configuring GPS...\n");
    add_gps_components(handler, opts, scs);

    printf("Configuring Video Input...\n");
    add_video_components(handler, scs, opts);

    printf("Configuring Fuzzy Control...\n");
    add_fc_components(fctrl, handler, scs, opts);

	printf("Configuring Feedback Loop...\n");
	insert_feedback_loop(scs, opts);

    printf("Initialization Complete.\n");

    if (opts.nodelay == 0) {
        printf("Press space when ready.\n");
        while (waitKey(500) != ' ' && opts.nodelay == 0);
    }

	if (!getuid()) {
		//setpriority(PRIO_PROCESS, 0, -20);
		struct sched_param parm = {0};
		parm.sched_priority = 99;
		if (sched_setscheduler(getpid(), SCHED_FIFO, &parm) < 0) {
			FAIL(ant::MAJOR, "Failed to set realtime scheduler!\n");
			return 1;
		}
	}

    while (waitKey(5) != 'q' && !g_term_signal) {
		usleep(g_waitval * 1000);
        if (handler.process()) {
            printf("Handler Returned a Failure!\n");
            break;
        }

        printf("FPS: %02d\n", handler.fps());
        fflush(stdout);
        ss_log_manager.log_all();
        g_control_collection.dump();
    }

	if (g_term_signal) {
		printf("TERM Received! Shutting Down!\n");
	}

    return 0;
}
