#include <stdio.h>

#include <ant/debug.h>
#include <ant/response_table.h>
#include <ant/enums.h>

using namespace ant;

int 
main(int argc, char* argv[])
{
	(void)argc; (void)argv;
	ant::std.set_level(ALL);	
	ant::err.set_level(ALL);	

    mid_t response
		[MAX_INPUT_BEARING][MAX_INPUT_BEARING][MAX_INPUT_BEARING] = {
        {
			{ CCW_LARGE, CCW_LARGE, CCW_LARGE, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_LARGE, CCW_SMALL, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_SMALL, CCW_LARGE, 	CW_SMALL, 	CW_SMALL},
        	{ CW_SMALL, CW_SMALL, 	CW_SMALL, 	CW_LARGE, 	CW_LARGE},
        	{ CW_SMALL, CW_SMALL, 	CW_LARGE, 	CW_LARGE, 	CW_LARGE}
		},
        {
			{ CCW_LARGE, CCW_LARGE, CCW_LARGE, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_LARGE, CCW_SMALL, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_SMALL, CCW_SMALL, 	CW_SMALL, 	CW_SMALL},
        	{ CW_SMALL, CW_SMALL, 	CW_SMALL, 	CW_LARGE, 	CW_LARGE},
        	{ CW_SMALL, CW_SMALL, 	CW_LARGE, 	CW_LARGE, 	CW_LARGE}
		},
        {
			{ CCW_LARGE, CCW_LARGE, CCW_LARGE, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_LARGE, CCW_SMALL, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_SMALL, ZERO, 		CW_SMALL, 	CW_SMALL},
        	{ CW_SMALL, CW_SMALL, 	CW_SMALL, 	CW_LARGE, 	CW_LARGE},
        	{ CW_SMALL, CW_SMALL, 	CW_LARGE, 	CW_LARGE, 	CW_LARGE}
		},
        {
			{ CCW_LARGE, CCW_LARGE, CCW_LARGE, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_LARGE, CCW_SMALL, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_SMALL, CW_SMALL, 	CW_SMALL, 	CW_SMALL},
        	{ CW_SMALL, CW_SMALL, 	CW_SMALL, 	CW_LARGE, 	CW_LARGE},
        	{ CW_SMALL, CW_SMALL, 	CW_LARGE, 	CW_LARGE, 	CW_LARGE}
		},
        {
			{ CCW_LARGE, CCW_LARGE, CCW_LARGE, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_LARGE, CCW_SMALL, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_SMALL, CW_LARGE, 	CW_SMALL, 	CW_SMALL},
        	{ CW_SMALL, CW_SMALL, 	CW_SMALL, 	CW_LARGE, 	CW_LARGE},
        	{ CW_SMALL, CW_SMALL, 	CW_LARGE, 	CW_LARGE, 	CW_LARGE}
		}
    };

	mid_t ids[] = {LEFT_LARGE, LEFT_SMALL, ZERO, RIGHT_SMALL, RIGHT_LARGE};
	int values[] = {-100, -50, 0, 50, 100};
	int out[] = {-100, -50, 0, 50, 100};

	response_table table;

	table.add_dimension(5);
	table.add_dimension(5);
	table.add_dimension(5);

	table.set_table(125, &response[0][0][0]);

	member_record t[3];
	t[0].set_name("Bearing");
	t[0].set_values(5, &ids[0], &values[0]);

	t[1].set_name("FarVisual");
	t[1].set_values(5, &ids[0], &values[0]);

	t[2].set_name("GlobalHazard");
	t[2].set_values(5, &ids[0], &values[0]);

	int val1 = 0;
	int val2 = 0;
	int val3 = 0;

again:
	printf("Enter: %s,%s,%s:\n", t[0].name().c_str(), t[1].name().c_str(), 
	t[2].name().c_str());
	int ret = scanf("%d,%d,%d", &val1, &val2, &val3);
	if (ret < 0) {
		printf("Bad Scan!\n");
		exit(1);
	}
		

	t[0].map_input(val3);
	t[1].map_input(val2);
	t[2].map_input(val1);
	
	member_record result;
	result.set_name("result");
	result.set_values(5, &ids[0], &out[0]);

	result.cart_product(table, &t[0], &t[1], &t[2]);

	printf("Result: %f\n", result.defuzz());
	goto again;
	return 0;
}
