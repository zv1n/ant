#include <iostream>

#include <sys/stat.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

using namespace std;
using namespace cv;

#define WNAME "Video Capture"


void
usage(const char* name)
{
    cout << "usage: " << name << " <file output> [camera]" << endl;
    exit(1);
}

bool
find_camera(VideoCapture& dev)
{
    char ch = '\0';

    if (dev.isOpened())
        dev.release();

    Mat frame;
    for (int x=0; x<=215; x++) {
        printf("camera %d \n", x);

	if (!dev.open(x))
            continue;

       // dev >> frame;
        cout << "Do you wish to use this camera(" << x << ")?" << endl;
        cin >> ch;
        if (ch == 'y' || ch == 'Y')
            return true;

        dev.release();
    }

    return false;
}

int
main(int c, char** v)
{

    VideoCapture device;

    if (c==3) {
        int d = strtol(v[2], NULL, 10);
        if (!device.open(d)) {
            cout << "Failed to open specified camera:" << d << endl;
            usage(v[0]);	
        }
		else if(device.isOpened())
		    cout << "Camera opened successfully." << endl;
    }
	
	if (c < 2) {		
		usage(v[0]);
		exit(0);
	}

    namedWindow(WNAME, CV_WINDOW_NORMAL|CV_WINDOW_KEEPRATIO);

    if (!device.isOpened())
    {
	cout <<  "looking for camera" << endl;
	if (!find_camera(device)) 
	{
            cout << "Failed to find a valid camera." << endl;
            exit(1);
        }
	else
	    cout<< "Camera found." << endl;
    }

    int key = 0;
    Mat frame;
    device >> frame;

	
	if(frame.empty())
	{
		cout << "frame is empty!!" <<endl;
		exit(1);
	}

	printf("frame: %d %d \n", frame.rows, frame.cols);
	cout << v[1] <<endl;
	
    VideoWriter file(v[1], CV_FOURCC('D','I','V','X'), 15.0, frame.size(),
                    true);

    if (!file.isOpened())
	{
		cout << "VideoWriter could not be opened." << endl;
		exit(1);
	}

    while (key != 'q') {
        device >> frame;
        file << frame;
        imshow(WNAME, frame);
        key = waitKey(15);
    }

    destroyAllWindows();

    return 0;
}
