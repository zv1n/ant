#include <ant/gps.h>
#include <ant/gps_record.h>
#include <ant/debug.h>

#include <iostream>
#include <string>
#include <unistd.h>
#include <stdlib.h>

using namespace std;

void
usage(char* app)
{
    cout << "usage: " << app << " [-v]" << endl;
    exit(1);
}

int main(int argc, char** argv)
{
    ant::std.set_level(ant::ALL);
    ant::err.set_level(ant::ALL);

    ant::log_manager logger("../config/gps.lcf");
    ant::record::set_log_manager(&logger);

    int ch = 0;
    char* app = argv[0];
    int done = 0;

    ant::filter* gps_in = NULL;
    ant::gps_record* latlon;

    struct {
        char* path;
        int verbose;
    } options = {NULL, 0};

    while ((ch = getopt(argc, argv, "v")) != -1) {
        switch(ch) {
        case 'v':
            options.verbose = ant::ALL;
            break;
        default:
            usage(app);
        }
    }

    argv += optind;
    argc -= optind;

    // if (argc < 1) usage(app);

    // options.path = argv[0];
    ant::std.set_level(options.verbose);

    /* Add GPS Components! */
    gps_in = ant::create_input("gps");
    if (!gps_in) {
        FAIL(ant::DANGER_WILL_ROBINSON, "Failed to create GPS Input!\n");
        return 1;
    }

    gps_in->set_name("GPSInput");

    while (!done) {
        if (gps_in->process()) {
            FAIL(ant::DANGER_WILL_ROBINSON, "GPS Returned Failure!\n");
            done = true;
        }

        latlon = gps_in->current<ant::gps_record>();
        printf("Time: %d:%d:%d\n", latlon->hours(), latlon->minutes(),
               latlon->seconds());
        printf("Latitude: %lf Longitude: %lf Altitude: %lf\n",
               latlon->latitude(), latlon->longitude(), latlon->altitude());
        sleep(1);
    }

    return 0;
}

