/*
 *  V4L2 video capture example
 *
 *  This program can be used and distributed without restrictions.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <asm/types.h>          /* for videodev2.h */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <getopt.h>             /* getopt_long() */

#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <signal.h>
#include <linux/videodev2.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <pixfc-sse.h>

using namespace cv;

#define CLEAR(x) memset (&(x), 0, sizeof (x))

struct buffer {
        void *                  start;
        size_t                  length;
};

static char *           dev_name        = NULL;
static int              fd              = -1;
struct buffer *         buffers         = NULL;
static unsigned int     n_buffers       = 0;
struct PixFcSSE*		pfc_sse = NULL;

int IMG_WIDTH = 480;
int IMG_HEIGHT = 640;

#define YUV_SIZE    ((IMG_WIDTH*IMG_HEIGHT)<<1)-1    
#define RGB_SIZE	(IMG_WIDTH*IMG_HEIGHT) + YUV_SIZE
static void
errno_exit                      (const char *           s)
{	
        fprintf (stderr, "%s error %d, %s\n",
                 s, errno, strerror (errno));

        exit (EXIT_FAILURE);
}

static int
xioctl                          (int                    fd,
                                 int                    request,
                                 void *                 arg)
{
        int r;

        do r = ioctl (fd, request, arg);
        while (-1 == r && EINTR == errno);

        return r;
}

#define YUV2BGR(y, u, v, r, g, b)\
b = y + ((v*1436) >> 10);\
g = y - ((u*352 + v*731) >> 10);\
r = y + ((u*1814) >> 10);\
r = r < 0 ? 0 : r;\
g = g < 0 ? 0 : g;\
b = b < 0 ? 0 : b;\
r = r > 255 ? 255 : r;\
g = g > 255 ? 255 : g;\
b = b > 255 ? 255 : b


static void YUV422_to_RGB8(const uint8_t* src, uint8_t* &dest)
{
	register int i = YUV_SIZE;
	register int j = RGB_SIZE;
	register int y0, y1, u, v;
	register int r, g, b;

	if (!dest || !src)
		perror("YUV422_to_RGB8");

	while (i >= 0) 
	{
		v = (uint8_t) src[i--] -128;
		y1 = (uint8_t) src[i--];
		u = (uint8_t) src[i--] -128;
		y0 = (uint8_t) src[i--];
		YUV2BGR (y1, u, v, r, g, b);
		dest[j--] = b;
		dest[j--] = g;
		dest[j--] = r;
		YUV2BGR (y0, u, v, r, g, b);
		dest[j--] = b;
		dest[j--] = g;
		dest[j--] = r;
	}
}

static int
read_frame(uint8_t* frame)
{
    struct v4l2_buffer buf;
		
		CLEAR (buf);

	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;

    		if (-1 == xioctl (fd, VIDIOC_DQBUF, &buf)) 
			{
            	switch (errno) 
				{
            		case EAGAIN:
                    			return 0;

					case EIO:
					/* Could ignore EIO, see spec. */

					/* fall through */

					default:
					errno_exit ("VIDIOC_DQBUF");
				}
			}

            assert (buf.index < n_buffers);

	        //frame = buffers[buf.index].start;
			memcpy(frame, buffers[buf.index].start, buffers[buf.index].length);
			if (-1 == xioctl (fd, VIDIOC_QBUF, &buf))
				errno_exit ("VIDIOC_QBUF");

			return 1;
}
 
static void
mainloop(void)
{
		
			uint8_t* yuv_img = new uint8_t[YUV_SIZE];
		namedWindow("RGB", CV_WINDOW_NORMAL | CV_WINDOW_KEEPRATIO);
        for(int key = 0; key !='q'; key = waitKey(15)) 
		{
        	fd_set fds;
            struct timeval tv;
            int r;
			uint8_t* rgb_img = new uint8_t[RGB_SIZE];
			
            FD_ZERO (&fds);
            FD_SET (fd, &fds);

            /* Timeout. */
            tv.tv_sec = 2;
            tv.tv_usec = 0;

            r = select (fd + 1, &fds, NULL, NULL, &tv);

            if (-1 == r) 
			{
            	if (EINTR == errno)
                	continue;

				errno_exit ("select");
             }

             if (0 == r) 
			 {
			 	fprintf (stderr, "select timeout\n");
                exit (EXIT_FAILURE);
             }

			 read_frame (yuv_img);

			const int sizes[2] = {IMG_WIDTH, IMG_HEIGHT};
			
			if (!pfc_sse || !pfc_sse->convert) {
				YUV422_to_RGB8(yuv_img, rgb_img);
			} else
				pfc_sse->convert(pfc_sse, yuv_img, rgb_img);
		
			Mat rgb_mat(2, sizes, CV_8UC3, (void*)rgb_img, 0);
			imshow("RGB", rgb_mat);
			delete rgb_img;
        }
}

static void
stop_capturing                  (void)
{
        enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

		if (-1 == xioctl (fd, VIDIOC_STREAMOFF, &type))
			errno_exit ("VIDIOC_STREAMOFF");
}

static void
start_capturing                 (void)
{
        unsigned int i;
        enum v4l2_buf_type type;

		for (i = 0; i < n_buffers; ++i) 
		{
            struct v4l2_buffer buf;

        		CLEAR (buf);

        		buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        		buf.memory      = V4L2_MEMORY_MMAP;
        		buf.index       = i;

        		if (-1 == xioctl (fd, VIDIOC_QBUF, &buf))
                    		errno_exit ("VIDIOC_QBUF");
		}
		
		type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

		if (-1 == xioctl (fd, VIDIOC_STREAMON, &type))
			errno_exit ("VIDIOC_STREAMON");
}

static void
uninit_device(void)
{
	for (unsigned int i = 0; i < n_buffers; ++i)
		if (-1 == munmap (buffers[i].start, buffers[i].length))
			errno_exit ("munmap");
	free (buffers);

	destroy_pixfc(pfc_sse);
	pfc_sse = NULL;
}


static void
init_mmap(void)
{
	struct v4l2_requestbuffers req;



        CLEAR (req);

        req.count               = 4;
        req.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        req.memory              = V4L2_MEMORY_MMAP;

	if (-1 == xioctl (fd, VIDIOC_REQBUFS, &req)) {
                if (EINVAL == errno) {
                        fprintf (stderr, "%s does not support "
                                 "memory mapping\n", dev_name);
                        exit (EXIT_FAILURE);
                } else {
                        errno_exit ("VIDIOC_REQBUFS");
                }
        }

        if (req.count < 2) {
                fprintf (stderr, "Insufficient buffer memory on %s\n",
                         dev_name);
                exit (EXIT_FAILURE);
        }

        buffers = (buffer*)calloc (req.count, sizeof (*buffers));

        if (!buffers) {
                fprintf (stderr, "Out of memory\n");
                exit (EXIT_FAILURE);
        }

        for (n_buffers = 0; n_buffers < req.count; ++n_buffers) {
                struct v4l2_buffer buf;

                CLEAR (buf);

                buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buf.memory      = V4L2_MEMORY_MMAP;
                buf.index       = n_buffers;

                if (-1 == xioctl (fd, VIDIOC_QUERYBUF, &buf))
                        errno_exit ("VIDIOC_QUERYBUF");

                buffers[n_buffers].length = buf.length;
                buffers[n_buffers].start =
                        mmap (NULL /* start anywhere */,
                              buf.length,
                              PROT_READ | PROT_WRITE /* required */,
                              MAP_SHARED /* recommended */,
                              fd, buf.m.offset);

                if (MAP_FAILED == buffers[n_buffers].start)
                        errno_exit ("mmap");
        }
}


static void
init_device(int nosse)
{
        struct v4l2_capability cap;
        struct v4l2_format fmt;
	unsigned int min;

        if (-1 == xioctl (fd, VIDIOC_QUERYCAP, &cap)) {
                if (EINVAL == errno) {
                        fprintf (stderr, "%s is no V4L2 device\n",
                                 dev_name);
                        exit (EXIT_FAILURE);
                } else {
                        errno_exit ("VIDIOC_QUERYCAP");
                }
        }

        if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
                fprintf (stderr, "%s is no video capture device\n",
                         dev_name);
                exit (EXIT_FAILURE);
        }

		if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
			fprintf (stderr, "%s does not support streaming i/o\n",
				 dev_name);
			exit (EXIT_FAILURE);
		}


        /* Select video input, video standard and tune here. */


        CLEAR (fmt);
		if (!nosse)
			if (create_pixfc(&pfc_sse, PixFcYUYV, PixFcBGR24, IMG_WIDTH, 
						IMG_HEIGHT, PixFcFlag_SSE2Only))
			fprintf(stderr, "Error creating PixFC context!\n");

        fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        fmt.fmt.pix.width       = IMG_WIDTH;
        fmt.fmt.pix.height      = IMG_HEIGHT;
		fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
        fmt.fmt.pix.field       = V4L2_FIELD_ANY;

        if (-1 == xioctl (fd, VIDIOC_S_FMT, &fmt))
                errno_exit ("VIDIOC_S_FMT");

        /* Note VIDIOC_S_FMT may change width and height. */

	/* Buggy driver paranoia. */
	min = fmt.fmt.pix.width * 2;
	if (fmt.fmt.pix.bytesperline < min)
		fmt.fmt.pix.bytesperline = min;
	min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
	if (fmt.fmt.pix.sizeimage < min)
		fmt.fmt.pix.sizeimage = min;

		printf("%d %d\n", fmt.fmt.pix.width, fmt.fmt.pix.height);

		init_mmap ();

}

static void
close_device                    (void)
{
        if (-1 == close (fd))
	        errno_exit ("close");

        fd = -1;
}

static void
open_device                     (void)
{
        struct stat st; 

        if (-1 == stat (dev_name, &st)) {
                fprintf (stderr, "Cannot identify '%s': %d, %s\n",
                         dev_name, errno, strerror (errno));
                exit (EXIT_FAILURE);
        }

        if (!S_ISCHR (st.st_mode)) {
                fprintf (stderr, "%s is no device\n", dev_name);
                exit (EXIT_FAILURE);
        }

        fd = open (dev_name, O_RDWR /* required */ | O_NONBLOCK, 0);

        if (-1 == fd) {
                fprintf (stderr, "Cannot open '%s': %d, %s\n",
                         dev_name, errno, strerror (errno));
                exit (EXIT_FAILURE);
        }
}

static void
usage                           (FILE *                 fp,
                                 char **                argv)
{
        fprintf (fp,
                 "Usage: %s [options]\n\n"
                 "Options:\n"
                 "-d | --device name   Video device name [/dev/video]\n"
                 "-h | --help          Print this message\n"
                 "-m | --mmap          Use memory mapped buffers\n"
                 "-r | --read          Use read() calls\n"
                 "-u | --userp         Use application allocated buffers\n"
                 "",
		 argv[0]);
}

static const char short_options [] = "d:hn";

static const struct option
long_options [] = {
        { "device",     required_argument,      NULL,           'd' },
        { "help",       no_argument,            NULL,           'h' },
        { 0, 0, 0, 0 }
};

int
main                            (int                    argc,
                                 char **                argv)
{
		int nosse = 0;
        for (;;) {
                int index;
                int c;
                
                c = getopt_long (argc, argv,
                                 short_options, long_options,
                                 &index);

                if (-1 == c)
                        break;

                switch (c) {
                case 0: /* getopt_long() flag */
                        break;

                case 'd':
                        dev_name = optarg;
                        break;

				case 'n':
						nosse = 1;
						break;

                case 'h':
                        usage (stdout, argv);
                        exit (EXIT_SUCCESS);

                default:
                        usage (stderr, argv);
                        exit (EXIT_FAILURE);
                }
        }

        open_device ();

        init_device (nosse);

        start_capturing ();

        mainloop ();

        stop_capturing ();

        uninit_device ();

        close_device ();

        exit (EXIT_SUCCESS);

        return 0;
}
