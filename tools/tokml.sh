#!/bin/bash

# Take a user input file and dumps it into a KML file.
# The file should be of the form:
#
#	Latitude(float), Longitude(float), Altitude(float),
#	Latitude(float), Longitude(float), Altitude(float),
#	...
#

function die() {
	echo $*
	echo "usage: $(basename $0) <input coords file> <kml output file>"
	exit 1
}

[ $# -eq 2 ] || die
[ -f $1 ] || die "Input file does not exist!" 
if [ -f $2 ]; then
	read -n1 -p "Output file ($(basename $2)) already exists. Overwrite? [y/N]"\
		yno
	echo #newline

	[ "$yno" != "y" -a "$yno" != "Y" ] && die "File already exists!" 
fi

input=$1
output=$2

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" > $output
echo "<kml xmlns=\"http://www.opengis.net/kml/2.2\">" >> $output
echo "  <Document>" >> $output
echo "    <name>Paths</name>" >> $output
echo "    <description>ANT Rover GPS Log</description>" >> $output
echo "    <Style id=\"greenLine\">" >> $output
echo "      <LineStyle>" >> $output
echo "        <color>7f00ff00</color>" >> $output
echo "        <width>4</width>" >> $output
echo "      </LineStyle>" >> $output
echo "    </Style>" >> $output
echo "    <Placemark>" >> $output
echo "      <name>Absolute Extruded</name>" >> $output
echo "      <description>Green lines</description>" >> $output
echo "      <styleUrl>#greenLine</styleUrl>" >> $output
echo "      <LineString>" >> $output
echo "        <tessellate>1</tessellate>" >> $output
echo "        <altitudeMode>relativeToGround</altitudeMode>" >> $output
echo -n "        <coordinates>" >> $output

uniq $input | sed -e 's|[0-9]\{1,3\},[0-9]\{1,3\}$|1|g' >> $output

echo "        </coordinates>" >> $output
echo "      </LineString>" >> $output
echo "    </Placemark>" >> $output
echo "  </Document>" >> $output
echo "</kml>" >> $output
