#include <stdio.h>

#include <ant/debug.h>
#include <ant/response_table.h>
#include <ant/enums.h>

using namespace ant;

int 
main(int argc, char* argv[])
{
	(void)argc; (void)argv;
	ant::std.set_level(ALL);	
	ant::err.set_level(ALL);	

    mid_t response[MAX_OUTPUT_SPEED][MAX_OUTPUT_SPEED] = {
        /*FUZ_SPEED*/
        {STOP, IDLE, IDLE, IDLE, SLOW},
        {STOP, IDLE, IDLE, SLOW, SLOW},
        {STOP, IDLE, SLOW, SLOW, FAST},
        {STOP, IDLE, SLOW, FAST, FAST},
        {STOP, IDLE, SLOW, FAST, LUDACRIS_SPEED}
    };

	mid_t ids[] = {STOP, IDLE, SLOW, FAST, LUDACRIS_SPEED};
	int values[] = {0, 50, 100, 150, 200};

	response_table table;

	table.add_dimension(5);
	table.add_dimension(5);

	table.set_table(25, &response[0][0]);

	member_record t[2];
	t[0].set_name("Near");
	t[0].set_values(5, &ids[0], &values[0]);

	t[1].set_name("Far");
	t[1].set_values(5, &ids[0], &values[0]);

	int val1 = 0;
	int val2 = 0;

again:
	printf("Enter: %s,%s:\n", t[0].name().c_str(), t[1].name().c_str());	
	int ret = scanf("%d,%d", &val1, &val2);
	if (ret < 0) {
		printf("Bad Scan!\n");
		exit(1);
	}
		

	t[0].map_input(val1);
	t[1].map_input(val2);
	
	member_record result;
	result.set_name("result");
	result.set_values(5, &ids[0], &values[0]);
	result.cart_product(table, &t[0], &t[1]);

	printf("Result: %f\n", result.defuzz());
	goto again;
	return 0;
}
