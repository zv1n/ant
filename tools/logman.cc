#include <ant/log_manager.h>
#include <ant/std_records.h>
#include <ant/debug.h>

#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{
	(void)argc;(void)argv;
    ant::std.set_level(ant::ALL);
    ant::err.set_level(ant::ALL);

    ant::log_manager logger("../config/logman.lcf");
    ant::record::set_log_manager(&logger);

    ant::integer_record rec_orig;
    rec_orig.set_value(1);

    {
        ant::integer_record rec;
        rec.set_name("dandy");

        rec.update_from(&rec_orig);
    }

    return 0;
}
