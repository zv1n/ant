#include <iostream>

#include <sys/stat.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

using namespace std;
using namespace cv;

#define WNAME "Video Capture"


void
usage(const char* name)
{
    cout << "usage: " << name << " <file output>" << endl;
    exit(1);
}

int
main(int c, char** v)
{

    VideoCapture device;
    struct stat buf;
    if (c!=2)
        usage(v[0]);
    if (stat(v[1], &buf))
        usage(v[0]);

    namedWindow(WNAME, CV_WINDOW_NORMAL | CV_WINDOW_KEEPRATIO);

    device.open(v[1]);
    if (!device.isOpened()) {
        cout << "Failed to open specified file." << endl;
        return 1;
    }

    int key = 0;
    Mat frame;

    while (key != 'q') {
        device >> frame;
        if (!frame.cols || !frame.rows)
            goto out;
        imshow(WNAME, frame);
        key = waitKey(27);
    }
out:
    destroyAllWindows();

    return 0;
}
