#include <ant/log_manager.h>
#include <ant/collection.h>
#include <ant/string_helper.h>

#define DEBUGGING DEBUG_LOG_MANAGER
#include <ant/debug.h>

#include <stdlib.h>
#include <fstream>
#include <string.h>

#include <time.h>

using namespace std;

namespace ant
{

log_manager::log_manager(uint8_t mode)
{
    m_ucMode = mode;
    m_bLog = false;
    m_rMap.clear();
}

log_manager::log_manager(const std::string file, uint8_t mode)
{
    m_ucMode = mode;
    m_rMap.clear();
    if (load_from_file(file))
        FAIL(MAJOR, "Failed to open Log Configuration File!");
}

log_manager::~log_manager()
{
}

/* modify logs */
int
log_manager::load_from_file(const std::string file)
{
    ifstream fstrm(file.c_str(), ifstream::in);

    if (fstrm.fail()) {
        DEBUG(MINOR, "Failed to open file!\n");
        return -1;
    }

    string line;

    int ln = 0;
    int err = 0;
    int has_errors = 0;

    DEBUG(TLDR, "Loading Log Configuration from '%s'\n", file.c_str());

    while (fstrm.good()) {
        ln++;
        getline(fstrm, line);
        err = process_config(ln, line);

        if (err) {
            if (!has_errors)
                FAIL(MINOR, "When processing file: %s\n", file.c_str());
            FAIL(MINOR, "Failed to process line %d.\n", ln);
            has_errors++;
        }
    }

    return has_errors;
}

char*
log_manager::get_value(int x)
{
    static char* curwd = NULL;
    static char sdate[MAX_DATE] = {0};
    static char stime[MAX_TIME] = {0};

    if (!curwd) {
        time_t rawtime;
        struct tm * timeinfo;

        time(&rawtime);
        timeinfo = localtime ( &rawtime );

        curwd = getcwd(NULL,0);

        strftime (sdate, MAX_DATE, "%d%m%y", timeinfo);
        strftime (stime, MAX_TIME, "%H%M%S", timeinfo);
    }
    switch(x) {
    case CURWD:
        return curwd;
    case TIME:
        return &stime[0];
    case DATE:
        return &sdate[0];
    }

    return NULL;
}

int
log_manager::replace_variables(std::string& file)
{
    const char* variable = "$PWD$";
    size_t pos = 0;
    int cnt = 0;

    while (pos != std::string::npos) {
        pos = file.find(variable, pos);
        if (pos == std::string::npos)
            break;
        file.erase(pos, strlen(variable));
        file.insert(pos, log_manager::get_value(CURWD));
        cnt++;
    }

    variable = "$DATE$";
    pos = 0;
    while (pos != std::string::npos) {
        pos = file.find(variable, pos);
        if (pos == std::string::npos)
            break;
        file.erase(pos, strlen(variable));
        file.insert(pos, log_manager::get_value(DATE));
        cnt++;
    }

    variable = "$TIME$";
    pos = 0;
    while (pos != std::string::npos) {
        pos = file.find(variable, pos);
        if (pos == std::string::npos)
            break;
        file.erase(pos, strlen(variable));
        file.insert(pos, log_manager::get_value(TIME));
        cnt++;
    }

    return cnt;
}

int
log_manager::process_config(int line, std::string config)
{
	(void)line;
    record_map rmap = {0,0,NULL,""};
    std::string name;

    trim(config);

    if (config.length() == 0 || config[0] == '#')
        return VALID;

    size_t last_offset = 0;
    size_t next_offset = 0;

    for (int i=0; i<4; i++) {
        std::string item;

        next_offset = config.find_first_of(',', last_offset);
        if (next_offset == std::string::npos) {
            next_offset = config.length();
        }

        item = config.substr(last_offset, next_offset-last_offset);
        trim(item);

        if (item.length() == 0)
            goto next;

        switch (i) {
        case 0:
            name = item;
            if (exists(name)) {
                DEBUG(TLDR, "Duplicate Record '%s' on line '%d'\n",
                      name.c_str(), line);
                return -1;
            }
            break;
        case 1: {
            std::string file = item;
            replace_variables(file);
            rmap.file = file;
            break;
        }
        case 2:
            rmap.interval = strtol(item.c_str(), NULL, 10);
            break;
        case 3:
            rmap.flags = strtol(item.c_str(), NULL, 10);
            break;
        }
next:
        last_offset = next_offset+1;
    }

    m_rMap[name] = rmap;

    DEBUG(TLDR, "Name: '%s'\tInterval: %d\tFlags: %d\n", name.c_str(),
          rmap.interval, rmap.flags);
    DEBUG(TLDR, "File: '%s'\n", rmap.file.c_str());

    return VALID;
}

#define GET_MAP_BY_NAME(iter, name) \
	if (name.length() == 0) \
		return -1; \
	record_map_iter iter = m_rMap.find(name); \
	if (iter == m_rMap.end()) \
		return -1;

int
log_manager::set_flags(const std::string name, int flag)
{
    GET_MAP_BY_NAME(map, name);
    (*map).second.flags = flag;
    return VALID;
}

int
log_manager::get_flags(const std::string name, int& flag)
{
    GET_MAP_BY_NAME(map, name);
    flag = (*map).second.flags;
    return VALID;
}

int
log_manager::set_interval(const std::string name, int interval)
{
    GET_MAP_BY_NAME(map, name);
    (*map).second.interval = interval;
    return VALID;
}

int
log_manager::get_interval(const std::string name, int& interval)
{
    GET_MAP_BY_NAME(map, name);
    interval = (*map).second.interval;
    return VALID;
}

int
log_manager::set_file(const std::string name, const std::string file)
{
    GET_MAP_BY_NAME(map, name);
    if ((*map).second.file != file) {
        (*map).second.file = file;
        (*map).second.flags = LM_UPDATED_FILE;
    }
    return VALID;
}

int
log_manager::get_file(const std::string name, std::string& file)
{
    GET_MAP_BY_NAME(map, name);
    file = (*map).second.file;
    return VALID;
}

int
log_manager::register_record(record* rec) /* must be named! */
{
    if (!rec)
        return -1;

    std::string name = rec->name().c_str();
    if (name.length() == 0)
        return -1;

    DEBUG(TLDR, "Registration Request: %s\n", name.c_str());

    record_map_iter mi = m_rMap.find(name);
    if (mi != m_rMap.end()) {
        if ((*mi).second.entry)
            return -2;

        (*mi).second.entry = rec;
        return VALID;
    }

    record_map map;
    map.flags = LM_DISABLED;
    map.interval = 0;
    map.entry = rec;

    m_rMap[name] = map;
    return VALID;
}

int
log_manager::remove_record(record* rec)
{
    if (!rec)
        return -1;

    std::string name = rec->name();
    if (name.length() == 0)
        return -1;

    DEBUG(TLDR, "Removing Record: %s\n", name.c_str());

    record_map_iter rmap = m_rMap.find(name);
    if (rmap == m_rMap.end())
        return -1;

    (*rmap).second.entry = NULL;
    return VALID;
}

void
log_manager::clear()
{
    m_rMap.clear();
}

/* status */
void
log_manager::set_logging(bool log)
{
    m_bLog = log;
}

bool
log_manager::logging() const
{
    return m_bLog;
}

bool
log_manager::logging(const std::string name) const
{
    if (m_ucMode&LMM_PASSIVE)
        return false;

    const_record_map_iter iter = m_rMap.find(name);
    if (iter == m_rMap.end())
        return false;

    const record_map& map = (*iter).second;

    return (m_bLog && ((map.flags&LM_ENABLED) == LM_ENABLED));
}

record*
log_manager::get_record(const std::string name)
{
    record_map_iter rm = m_rMap.find(name);
    if (rm == m_rMap.end())
        return NULL;

    return (*rm).second.entry;
}

bool
log_manager::exists(const std::string name) const
{
    return !(m_rMap.find(name) == m_rMap.end());
}

int
log_manager::log_all()
{
    if (!(m_ucMode&LMM_PASSIVE))
        return INVALID;

    DEBUG(MAJOR, "Logging All Records\n");

    int ret = 0;
    record_map_iter iter = m_rMap.begin();

    DEBUG(MAJOR, "Records: %d\n", m_rMap.size());

    if (m_rMap.end() == iter)
        return INVALID;

    for (; iter != m_rMap.end(); iter++) {
        if (iter->second.flags&LM_DISABLED) {
            DEBUG(MAJOR, "Skipping: %s\n", iter->first.c_str());
            continue;
        }

        if(!iter->second.entry) {
            DEBUG(MAJOR, "No Record: %s\n", iter->first.c_str());
            continue;
        }

        DEBUG(MAJOR, "Logging: %s\n", iter->second.entry->name().c_str());
        ret |= iter->second.entry->log(true);
    }

    return ret;
}


}
