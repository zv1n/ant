#include <ant/rover_controller.h>
#include <ant/commandset_rover.h>

#define DEBUGGING DEBUG_ROVER_CONTROLLER
#include <ant/debug.h>

namespace ant
{
rover_controller::rover_controller(commandset* cs)
	:controller(&m_rSpeed), m_fInput(NULL)
{
    add_property<filter>(input_name(0), m_fInput);
	set_frame(&m_rHud);

    add_property<float>("ForceSpeed", m_fForceSpeed);
    add_property<float>("ForceBearing", m_fForceBearing);
	set_name("RoverController");
	m_rHud.set_name("ControllerHUD");

    m_cCommandset = cs;
	m_fForceBearing = -1000.0;
	m_fForceSpeed = -1000.0;
}

rover_controller::~rover_controller()
{

}

std::string
rover_controller::input_name(int x) const
{
    if (!x)
        return "Image";
    return "unused";
}

int rover_controller::process()
{
    collection* coll = collect(IN);
    if (!coll) {
        FAIL(DANGER_WILL_ROBINSON, "Failed to get input collection.\n");
        return INVALID;
    }

    float_record* rec_bear = coll->get<float_record>("FCOutputBearing");
    float_record* rec_speed = coll->get<float_record>("FCOutputSpeed");
    if (!rec_bear || !rec_speed) {
        FAIL(DANGER_WILL_ROBINSON, "Failed to retrieve requested records.\n");
        return INVALID;
    }

    commandset_rover* cr;
    cr = dynamic_cast<commandset_rover*>(m_cCommandset);
    if(!cr) {
        FAIL(DANGER_WILL_ROBINSON, "Failed to cast commandset to "
             "commandset_rover.\n");
        return INVALID;
    }
	
	set_command_bearing(rec_bear->value());
	set_command_speed(rec_speed->value());

	render_response();

	DEBUG(MAJOR, "Bearing:	%f\n", rec_bear->value());
	DEBUG(MAJOR, "Speed:	%f\n", rec_speed->value());

    return controller::process();
}

void
rover_controller::set_command_bearing(float bearing)
{
    commandset_rover* cr = dynamic_cast<commandset_rover*>(m_cCommandset);
    if(!cr) {
        FAIL(DANGER_WILL_ROBINSON, "Failed to cast commandset to "
             "commandset_rover.\n");
        return;
    }
	
	if (m_fForceBearing  < -200.0f) {
		m_rBearing.set_value(bearing);
    	cr->set_bearing(bearing);
	} else {
		FAIL(DANGER_WILL_ROBINSON, "Bearing Override!\n");
		m_rBearing.set_value(m_fForceBearing);
    	cr->set_bearing(m_fForceBearing);
	}
    //DEBUG(TLDR, "FC Output Bearing: %f\n", rec_bear->value());
}

void
rover_controller::set_command_speed(float speed)
{
    commandset_rover* cr = dynamic_cast<commandset_rover*>(m_cCommandset);
    if(!cr) {
        FAIL(DANGER_WILL_ROBINSON, "Failed to cast commandset to "
             "commandset_rover.\n");
        return;
    }
	
	if (m_fForceSpeed < -200.0f) {
		m_rSpeed.set_value(speed);
		cr->set_speed(speed);
	} else {
		FAIL(DANGER_WILL_ROBINSON, "Speed Override!\n");
		m_rSpeed.set_value(m_fForceSpeed);
		cr->set_speed(m_fForceSpeed);
	}
}

void
rover_controller::render_response()
{
	if (!m_fInput) {
		FAIL(DANGER_WILL_ROBINSON, "No input record specified!\n");
		return;
	}

    frame* inframe = m_fInput->current<frame>();
    if (!inframe) {
        FAIL(DANGER_WILL_ROBINSON, "Input record is not a frame record.\n");
        return;
    }

    cv::Mat& cep = m_rHud.data();
	if (inframe->data().channels() == 1)
		cv::cvtColor(inframe->data(), cep, CV_GRAY2BGR);
	else
		cep = inframe->data();

	static int frame = 0;

	frame++;
    std::stringstream ss;
    ss << frame;
	int lg = log(frame)/log(10);
    cv::putText(cep, ss.str(), cv::Point2i(cep.cols*(21-lg)/24, cep.rows*22/24),
                cv::FONT_HERSHEY_COMPLEX, 1.5f, CV_RGB(0,0,0), 4);

/*
    std::stringstream ss;
    ss << "Speed: ";
    ss << std::setprecision(std::numeric_limits<float>::digits10+1);
    ss << m_rSpeed.value();

    std::stringstream br;
    br << "Bearing: ";
    br << std::setprecision(std::numeric_limits<float>::digits10+1);
    br << m_rBearing.value();

    cv::putText(cep, ss.str(), cv::Point2i(10, 90),
                cv::FONT_HERSHEY_COMPLEX, 1.5f, CV_RGB(0,0,0), 4);
    cv::putText(cep, br.str(), cv::Point2i(10, 130),
                cv::FONT_HERSHEY_COMPLEX, 1.5f, CV_RGB(0,0,0), 4);
*/

	draw_speed(cep);
	draw_bearing(cep);
	draw_brakes(cep);
}

static void
band(cv::Mat& cep, cv::Point2i start, cv::Point2i max, cv::Point2i beam, 
	int cnt, float pcent, int lh)
{
	int sign = pcent<0?-1:1;
	int end = cnt * pcent * sign;

	for (int x=0; x<end; x++) {
		float pcnt = (float)x/(float)cnt;
		cv::Scalar color;
		switch(lh) {
		case 0:
			color = CV_RGB(255*pcnt, 255*(1-pcnt), 0);
			break;
		case 1:
			color = CV_RGB(255*(1-pcnt), 255*pcnt, 0);
			break;
		case 2:
			color = CV_RGB(0, 255, 0);
			break;
		case 3:
			color = CV_RGB(255, 0, 0);
			break;
		}

    	cv::line(cep, beam*x*sign+start, beam*x*sign+max, color, 2);
	}
}

void
rover_controller::draw_bearing(cv::Mat& cep)
{
	double bearing = m_rBearing.value();

	int xval = cep.cols/24;
	int yval = cep.rows/20;

    cv::Point2i	start(xval, yval);
    cv::Point2i end(xval, yval*2);
	cv::Point2i offset(1,0);
	cv::Point2i outline = offset*22*xval;

    cv::line(cep, start, outline + start, CV_RGB(255,0,0), 2);
    cv::line(cep, end, outline + end, CV_RGB(255,0,0), 2);
    cv::line(cep, start + offset*11*xval, end + offset*11*xval, 
				CV_RGB(0,255,0), 2);

    cv::Point2i	p1(xval*12, yval+2);
    cv::Point2i p2(xval*12, yval*2-2);
	band(cep, p1, p2, offset, 11*xval, bearing, 0);
}

void
rover_controller::draw_speed(cv::Mat& cep)
{
    commandset_rover* cr;
    cr = dynamic_cast<commandset_rover*>(m_cCommandset);
    if(!cr)
        return;

	double speed = cr->speed();
	int xval = cep.cols/24;
	int yval = cep.rows/20;

    cv::Point2i	start(xval*22, yval*3);
    cv::Point2i end(xval*23, yval*3);

	cv::Point2i offset(0,1);
	cv::Point2i outline = offset*12*yval;

    cv::line(cep, start, outline + start, CV_RGB(255,0,0), 2);
    cv::line(cep, end, outline + end, CV_RGB(255,0,0), 2);
    cv::line(cep, start + outline, end + outline, CV_RGB(255,0,0), 2);

    cv::Point2i	p1(start.x+2, start.y+yval*12);
    cv::Point2i p2(end.x-2, end.y+yval*12);
	band(cep, p1, p2, offset, 12*yval, -speed, 2);
}

void
rover_controller::draw_brakes(cv::Mat& cep)
{
	(void)cep;
#if USE_BRAKES
    commandset_rover* cr;
    cr = dynamic_cast<commandset_rover*>(m_cCommandset);
    if(!cr)
        return;

	double brake = cr->brake();
	int xval = cep.cols/24;
	int yval = cep.rows/20;

    cv::Point2i	start(xval*22, yval);
    cv::Point2i end(xval*23, yval);
	cv::Point2i offset(0,-1);

    cv::Point2i	p2(start.x+2, start.y+yval*2);
    cv::Point2i p1(end.x-2, end.y+yval*2);
	band(cep, p1, p2, offset, 12*yval, -brake, 3);
#endif
}


}
