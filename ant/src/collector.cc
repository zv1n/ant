#include <ant/collector.h>

#define DEBUGGING DEBUG_COLLECTOR
#include <ant/debug.h>

namespace ant
{

define_factory(collector, collector);

collector::collector(record* rec, int reqi, int reqe):
    filter(rec, reqi, reqe),
    m_inCollection(NULL),
    m_outCollection(NULL)
{
    add_property<collection>("Input", m_inCollection);
    add_property<collection>("Output", m_outCollection);
    set_preset("Input", this,
               (propset_fn)&collector::prop_remove_collection);
    set_preset("Output", this,
               (propset_fn)&collector::prop_remove_collection);
    set_postset("Input", this,
                (propset_fn)&collector::prop_set_collection);
    set_postset("Output", this,
                (propset_fn)&collector::prop_set_collection);
}

collector::~collector()
{
}

int
collector::prop_remove_collection(std::string& pname)
{
    DEBUG("prop_remove: %s", pname.c_str());

    if (pname == "Input")
        return remove_collection(IN);
    if (pname == "Output")
        return remove_collection(OUT);
    return INVALID;
}

int
collector::prop_set_collection(std::string& pname)
{
    DEBUG("prop_set: %s", pname.c_str());

    if (pname == "Input")
        return set_collection(IN);
    if (pname == "Output")
        return set_collection(OUT);
    return INVALID;
}

int
collector::remove_collection(int io)
{
    collection* col = collect(io);
    if (!col) {
        DEBUG("No collection set!\n");
        return VALID;
    }
    DEBUG("Removing record from old collection...\n");
	int ret = 0;
	for (unsigned int x=0; x<count(); x++) 
    	ret |= col->remove(current<record>(x));
	return ret;
}

int
collector::set_collection(int io)
{
    collection* col = collect(io);
    if (!col) {
        DEBUG("No collection set!");
        return VALID;
    }

    DEBUG("Adding record to new collection...");
	int ret = 0;
	for (unsigned int x=0; x<count(); x++)  {
    	record* rec = current<record>(x);
    	if (!rec)
			continue;
    	rec->new_collection_record();
    	DEBUG("Assigning record...\n");
    	ret |= col->add(rec);
	}

	return ret;
}

int
collector::set_input(collection* collect)
{
    m_inCollection = collect;
    return set_collection(IN);
}

int
collector::set_output(collection* collect)
{
    m_outCollection = collect;
    return set_collection(OUT);
}

collection*
collector::collect(int mode)
{
    switch(mode) {
    case IN:
        return m_inCollection;
    case OUT:
        return m_outCollection;
    }
    return NULL;
}

}
