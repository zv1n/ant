#include <ant/fuzzy_control.h>

#define DEBUGGING DEBUG_FUZZY_CONTROL
#include <ant/debug.h>

namespace ant
{

fuzzifier::fuzzifier(record* rec, int requi, int reque):
    fuzzy_element(rec,requi,reque)
{
}

fuzzifier::~fuzzifier()
{
}

defuzzifier::defuzzifier(record* rec, int requi, int reque):
    fuzzy_element(rec,requi,reque)
{
}

defuzzifier::~defuzzifier()
{
}

fuzzy_element::fuzzy_element(record* rec,int ri,int re):collector(rec,ri,re),m_pController(NULL) {};

fuzzy_controller*
fuzzy_element::controller()
{
    return m_pController;
}

int
fuzzy_element::set_controller(fuzzy_controller* fz)
{
    m_pController = fz;
    return (m_pController)?VALID:INVALID;
}


define_factory(fuzzifier, fuzzifier);
define_factory(defuzzifier, defuzzifier);

fuzzy_controller::fuzzy_controller():
    m_outputCollection(NULL)
{}

fuzzy_controller::~fuzzy_controller()
{}

int
fuzzy_controller::process()
{
    if (process_fuzzifiers()) {
        DEBUG("Failed to process fuzz...\n");
        return INVALID;
    }
    if (process_defuzzifiers()) {
        DEBUG("Failed to process defuzz...\n");
        return INVALID;
    }
    return VALID;
}

int
fuzzy_controller::process_fuzzifiers()
{
    std::vector<fuzzifier*>::iterator it;
    for (it=m_vFuzz.begin(); it<m_vFuzz.end(); it++)
        if ((*it)->process()) {
            FAIL(DANGER_WILL_ROBINSON, "Failure processing: %s\n",
                 (*it)->name().c_str());
            return INVALID;
        }
    return VALID;
}

int
fuzzy_controller::process_defuzzifiers()
{
    std::vector<defuzzifier*>::iterator it;
    for (it=m_vDefuzz.begin(); it<m_vDefuzz.end(); it++)
        if ((*it)->process()) {
            FAIL(DANGER_WILL_ROBINSON, "Failure processing: %s\n",
                 (*it)->name().c_str());
            return INVALID;
        }
    return VALID;
}

int
fuzzy_controller::add_fuzzifier(fuzzifier* def)
{
    if (!def)
        return INVALID;
    def->set_input(m_inputCollection);
    def->set_output(&m_cMemberSets);
    def->set_processor(get_processor());
    def->set_controller(this);
    m_vFuzz.push_back(def);
    return VALID;
}

fuzzifier*
fuzzy_controller::get_fuzzifier(std::string& name)
{
    std::vector<fuzzifier*>::iterator it;
    for (it=m_vFuzz.begin(); it<m_vFuzz.end(); it++)
        if ((*it)->name() == name)
            return *it;
    return NULL;
}

int
fuzzy_controller::delete_fuzzifier(std::string& name)
{
    std::vector<fuzzifier*>::iterator it;
    for (it=m_vFuzz.begin(); it<m_vFuzz.end(); it++)
        if ((*it)->name() == name) {
            (*it)->set_input(NULL);
            (*it)->set_output(NULL);
            m_vFuzz.erase(it);
            return VALID;
        }
    return INVALID;
}

int
fuzzy_controller::add_defuzzifier(defuzzifier* def)
{
    if (!def)
        return INVALID;
    def->set_input(&m_cMemberSets);
    def->set_output(m_outputCollection);
    def->set_processor(get_processor());
    def->set_controller(this);
    m_vDefuzz.push_back(def);
    return VALID;
}

defuzzifier*
fuzzy_controller::get_defuzzifier(std::string& name)
{
    std::vector<defuzzifier*>::iterator it;
    for (it=m_vDefuzz.begin(); it<m_vDefuzz.end(); it++)
        if ((*it)->name() == name)
            return *it;
    return NULL;
}

int
fuzzy_controller::delete_defuzzifier(std::string& name)
{
    std::vector<defuzzifier*>::iterator it;
    for (it=m_vDefuzz.begin(); it<m_vDefuzz.end(); it++)
        if ((*it)->name() == name) {
            (*it)->set_input(NULL);
            (*it)->set_output(NULL);
            m_vDefuzz.erase(it);
            return VALID;
        }
    return INVALID;
}

int
fuzzy_controller::set_processor(processor* proc)
{
    m_pProcessor = proc;
    return (m_pProcessor)?VALID:INVALID;
}

processor*
fuzzy_controller::get_processor()
{
    return m_pProcessor;
}

int
fuzzy_controller::set_input(collection* collect)
{
    m_inputCollection = collect;
    std::vector<fuzzifier*>::iterator it;
    for (it=m_vFuzz.begin(); it<m_vFuzz.end(); it++)
        (*it)->set_input(collect);
    return (collect)?VALID:INVALID;
}

int
fuzzy_controller::set_output(collection* collect)
{
    m_outputCollection = collect;
    std::vector<defuzzifier*>::iterator it;
    for (it=m_vDefuzz.begin(); it<m_vDefuzz.end(); it++)
        (*it)->set_output(collect);
    return (collect)?VALID:INVALID;
}

}

