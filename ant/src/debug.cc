#include <ant/debug.h>
#include <stdarg.h>

namespace ant
{

debug_logger err(stderr);
debug_logger std(stdout);

debug_logger::debug_logger(FILE* std):
    m_pFile(std)
{
}

debug_logger::~debug_logger()
{
    close();
}

int debug_logger::log(int level, const char* file, int line, const char* fmt,
                      ...)
{
    va_list va;

    if (level <= m_iLevel && level != -1) {
        return 0;
    }

	char l_fmt[256] = {0};

	snprintf(l_fmt, 256, "%s:%d) %s", file, line, fmt);

    va_start(va, fmt);
    int ret = vfprintf(m_pFile, l_fmt, va);
    va_end(va);
    return ret;
}

int debug_logger::set_file(const std::string filename)
{
    if (filename.size() < 1)
        return -1;

    if (filename == "stdout")
        m_pFile = stdout;
    else if (filename == "stderr")
        m_pFile = stderr;
    else
        m_pFile = fopen(filename.c_str(), "a+");

    return (m_pFile==NULL)?-1:0;
}

int debug_logger::close()
{
    if (m_pFile && m_pFile != stdout && m_pFile != stderr) {
        fclose(m_pFile);
        m_pFile = NULL;
        return 0;
    }
    m_pFile = NULL;
    return -1;
}

int debug_logger::flush()
{
    if (!m_pFile)
        return -1;
    fflush(m_pFile);
    return 0;
}

int debug_logger::isatty()
{
    if (!m_pFile)
        return -1;
    return (::isatty(fileno(m_pFile)))?0:-1;
}

int debug_logger::set_level(int level)
{
    m_iLevel = level;
    return level;
}

}
