#include <ant/gps_bearing_fuz.h>
#include <ant/member_record.h>

#define DEBUGGING DEBUG_FUZZIFIERS
#include <ant/debug.h>
#include <ant/enums.h>

namespace ant
{

#define MID_VALUE (0)

define_factory_class(gps_bearing_fuzzifier, fuzzifier, GPS_BEARING_FUZZ_UCID,
                     fuzzifier);

gps_bearing_fuzzifier::gps_bearing_fuzzifier():
    fuzzifier(&m_rGpsBearing)
{
    set_name("FGpsBearing");
    m_rGpsBearing.set_name("FCDestBearing");

    mid_t responses[MAX_INPUT_BEARING] = {
        LEFT_LARGE, LEFT_SMALL, ZERO, RIGHT_SMALL, RIGHT_LARGE
    };
    int	values[MAX_INPUT_BEARING] = {
        -60, -30, 0, 30, 60
    };

    m_rGpsBearing.set_values(MAX_INPUT_BEARING, &responses[0], &values[0]);
}

gps_bearing_fuzzifier::~gps_bearing_fuzzifier()
{
}

int
gps_bearing_fuzzifier::preprocess()
{
    return VALID;
}

int
gps_bearing_fuzzifier::fuzzify()
{
    collection* crisp = collect(IN);
    if (!crisp)
        return INVALID;

    collection* members = collect(OUT);
    if (!members)
        return INVALID;

    DEBUG(TLDR, "Mapping inputs...\n");
    integer_record fuzz;

	nav_record* nav = crisp->get<nav_record>("Navigation");	
	if (!nav) {
		FAIL(MAJOR, "Unable to get the nav_record from the collection!");
		return INVALID;
	}	

	if (nav->at_destination()) {
        FAIL(DANGER_WILL_ROBINSON, "\n\n\n!!!!!!!!!!! At Destination "
            "!!!!!!!!!!\n\n\n");
		fuzz.set_value(MID_VALUE);
	} else
    	fuzz.set_value(nav->rel_bearing());

    FAIL(TLDR, "Nav Bearing: %d\n", nav->rel_bearing());
    FAIL(TLDR, "True Bearing: %d\n", nav->true_bearing());
    FAIL(TLDR, "Valid Bearing: %d\n", nav->valid_bearing());

    m_rGpsBearing.map_input(&fuzz);

    return VALID;
}

}
