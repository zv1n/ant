#include <ant/frame.h>

#define DEBUGGING DEBUG_FRAME
#include <ant/debug.h>

namespace ant
{

frame::frame()
{
    m_rFrame = new cv::Mat();
}

frame::~frame()
{
    delete m_rFrame;
}

record*
frame::new_collection_record() const
{
    return static_cast<record*>(new frame(*this));
}

void
frame::dump() const
{
    DEBUG("Frame...\n");
}

int
frame::size() const
{
    return 1;
}

std::string
frame::type()
{
    return FRAME_RECORD_TYPE;
}

ucid_t
frame::record_ucid()
{
    return FRAME_RECORD_UCID;
}

ucid_t
frame::ucid() const
{
    return record_ucid();
}

int
frame::update_from(record* rec)
{
    frame* fr = record_cast<frame>(rec);
    if (!fr) {
        if (rec)
            DEBUG("Record NOT a Member Record: %llu != %llu\n",
                  (unsigned long long)res->ucid(),
                  (unsigned long long)MEMBER_RECORD_UCID);
        return INVALID;
    }

    if (!m_rFrame)
        m_rFrame = new cv::Mat(fr->data());
    else
        *m_rFrame = fr->data();
    return record::update_from(rec);
}

cv::Mat&
frame::data()
{
    return *m_rFrame;
}

int
frame::log(bool force) const
{
    IS_LOG_REQUIRED(force);

    if (!m_rFrame) {
        return INVALID;
	}

    if (!m_cvWriter.isOpened()) {
    	std::string path;

    	if (record::s_logManager->get_file(name().c_str(), path)) 
        	return INVALID;

    	if (path.length() == 0) 
        	return INVALID;

        /* we don't want to print if the video frame isn't populated */
        if (!m_rFrame->cols || !m_rFrame->rows) 
        	return INVALID;

        if (!m_cvWriter.open(path, CV_FOURCC('d','i','v','x'),
                             4.0, m_rFrame->size())) 
        	return INVALID;
    }

    if (m_rFrame->channels() == 1) {
        cv::Mat conv;
        cv::cvtColor(*m_rFrame, conv, CV_GRAY2BGR);
        m_cvWriter << conv;
    } else {
        m_cvWriter << *m_rFrame;
    }

    return VALID;
}

}
