#include <ant/gps.h>

#define DEBUGGING DEBUG_GPS
#include <ant/debug.h>

#include <sys/stat.h>
#include <vector>
#include <string>

#include <math.h>

#include <ctime>

using namespace std;

namespace ant
{
define_factory_class(gps, filter, GPS_FILTER_UCID, input);

gps::gps():
    filter(&m_rGps,0,0), m_bOpen(false)
{
    m_rGps.set_name("GPSLocation");
    open();
}

gps::~gps()
{
}

int
gps::process()
{
#if CONFIG_GPSD

    struct gps_data_t* data = NULL;
    int val  = 0;
    time_t time;
    tm* gm_time;

	if (!m_bOpen)
		goto out;

	val = m_gGpsSession.waiting();
    if (!val) {
        DEBUG(MAJOR, "%d GPS Feed Waiting!\n", val);
        goto out;
    }

    data = m_gGpsSession.poll();

    if (!data) {
        DEBUG(MAJOR, "Failed to get gps_data_t info.\n");
        goto out;
    }

    DEBUG(TLDR, "GPS: Longitude: %f\n", data->fix.longitude);
    DEBUG(TLDR, "GPS: Latitude: %f\n", data->fix.latitude);
    DEBUG(TLDR, "GPS: Altitude: %f\n", data->fix.altitude);
    DEBUG(TLDR, "GPS: Bearing: %f\n", data->fix.track);

    m_rGps.set_longitude(data->fix.longitude);
    m_rGps.set_latitude(data->fix.latitude);
    m_rGps.set_altitude(data->fix.altitude);
    m_rGps.set_reported_bearing(data->fix.track);

    time = (time_t)data->fix.time;
    gm_time = gmtime(&time);

    if (gm_time)
        m_rGps.set_time(gm_time->tm_hour, gm_time->tm_min, gm_time->tm_sec);
    else
        m_rGps.set_time(0,0,0);

out:
#else

    m_rGps.set_longitude(10000.0f);
    m_rGps.set_latitude(10000.0f);
    m_rGps.set_altitude(10000.0f);
    m_rGps.set_reported_bearing(10000.0f);
#endif

    return filter::process();
}

int
gps::open()
{
#if CONFIG_GPSD
    struct gps_data_t* data = NULL;

#if DEBUG_GPS
    m_gGpsSession.enable_debug(1, stdout);
#endif

    data = m_gGpsSession.open();

    if (!data) {
        FAIL(MAJOR, "Failed to open connection with GPSD!\n");
        return INVALID;
    }

    m_gGpsSession.stream(POLL_NONBLOCK | WATCH_ENABLE);

    if (m_gGpsSession.waiting()) {
        data = m_gGpsSession.poll();
    }

    m_bOpen = true;
#endif

    return VALID;
}

int
gps::is_open()
{
    /* check the GPS context */
    return (m_bOpen)?VALID:INVALID;
}

}
