#include <ant/gps_collector.h>

#define DEBUGGING DEBUG_GPS_COLLECTOR
#include <ant/debug.h>

#include <vector>
#include <string>

namespace ant
{

define_factory_class(gps_collector, collector, GPS_COLLECTOR_UCID, collector);

gps_collector::gps_collector():
    collector(&m_rLocation,1,0)
{
    m_rLocation.set_name("GPS");
    add_property<filter>(input_name(0), m_fInput);
}

gps_collector::~gps_collector()
{
}

std::string
gps_collector::input_name(int i)
{
    if (!i)
        return "GPS Input";
    return ant::unused;
}

int
gps_collector::process()
{
    if (!m_fInput) {
        DEBUG(TLDR, "No input supplied.\n");
        return INVALID;
    }

    record* gps = m_fInput->current<record>();
    if (!gps) {
        DEBUG(TLDR, "Record not a GPS!\n");
        return INVALID;
    }

    m_rLocation.update_from(gps);

    collection* out = collect(OUT);
    if (!out) {
        DEBUG(TLDR, "No output collection!\n");
        return INVALID;
    }

    DEBUG(TLDR, "Updating collection...\n");
    return out->update(&m_rLocation);
}

}
