#include <ant/propertyset.h>
#include <ant/hazards_record.h>
#include <ant/gps_record.h>

#define DEBUGGING DEBUG_HAZARDS_RECORD
#include <ant/debug.h>

#include <math.h>
#include <string.h>

namespace ant
{

hazards_record::hazards_record()
{
}

hazards_record::~hazards_record()
{
}

record*
hazards_record::new_collection_record() const
{
    return static_cast<record*>(new hazards_record(*this));
}

void
hazards_record::dump() const
{
    DUMP("Dumping: %s\n", name().c_str());
}

int
hazards_record::size() const
{
    return m_vhHazards.size();
}

std::string
hazards_record::type()
{
    return HAZARDS_RECORD_TYPE;
}

ucid_t
hazards_record::record_ucid()
{
    return HAZARDS_RECORD_UCID;
}

ucid_t
hazards_record::ucid() const
{
    return record_ucid();
}

int
hazards_record::update_from(record* rec)
{
    hazards_record* gps = record_cast<hazards_record>(rec);

    if (!gps) {
        DEBUG(TLDR, "Record type does not match Hazards Record!\n");
        return INVALID;
    }

    m_vhHazards = gps->m_vhHazards;
    return record::update_from(rec);
}

double
hazards_record::longitude(int idx) const
{
    if (idx < 0 || (unsigned)idx >= m_vhHazards.size())
        return nan("nan");
    return m_vhHazards[idx].lon;
}

double
hazards_record::latitude(int idx) const
{
    if (idx < 0 || (unsigned)idx >= m_vhHazards.size())
        return nan("nan");
    return m_vhHazards[idx].lat;
}

double
hazards_record::radius(int idx) const
{
    if (idx < 0 || (unsigned)idx >= m_vhHazards.size())
        return nan("nan");
    return m_vhHazards[idx].rad;
}

void
hazards_record::add_hazard(double lat, double lon, double rad)
{
    hazard_values newhaz = {lat, lon, rad};
    m_vhHazards.push_back(newhaz);
}

void
hazards_record::remove(int idx)
{
    m_vhHazards.erase(m_vhHazards.begin() + idx);
}

void
hazards_record::set_rel_position(gps_record* record)
{
    m_grPosition = record;
}

gps_record*
hazards_record::rel_position() const
{
    return m_grPosition;
}

double
hazards_record::distance(int idx) const
{
    if (idx < 0 || (unsigned)idx >= m_vhHazards.size() || !m_grPosition)
        return nan("nan");

    // copy to local variable to make code more readable
    double lat1 = m_grPosition->latitude()*M_PI/180.0;
    double lat2 = latitude(idx)*M_PI/180.0;
    double lon1 = m_grPosition->longitude()*M_PI/180.0;
    double lon2 = longitude(idx)*M_PI/180.0;
    const double R = 6372797.560856;  //radius of the earth in meters
    //uses haversine's formula to calculate distance between two points

    double dlon = lon2 - lon1;
    double dlat = lat2 - lat1;
    double a = sin(dlat/2)*sin(dlat/2)+
               cos(lat1)*cos(lat2)*sin(dlon/2)*sin(dlon/2);
    double c = 2*asin(sqrt(a));
    double d = R * c;

    DEBUG(TLDR, "Hazard: %d\n", idx);
    DEBUG(TLDR, "Hazard Distance: %lf\n", d);

    return d;
}

double
hazards_record::bearing(int idx) const
{
    if (idx < 0 || (unsigned)idx >= m_vhHazards.size() || !m_grPosition)
        return nan("nan");

	double bearing = m_grPosition->reported_bearing();
	if (isnan(bearing))
		bearing = 0.0f;

    // copy to local variable to make code more readable
    double lat1 = m_grPosition->latitude()*M_PI/180.0;
    double lat2 = latitude(idx)*M_PI/180.0;
    double lon1 = m_grPosition->longitude()*M_PI/180.0;
    double lon2 = longitude(idx)*M_PI/180.0;
    double dlon = lon2 - lon1;

    double y = sin(dlon)*cos(lat2);
    double x = cos(lat1)*sin(lat2) - sin(lat1)*cos(lat2)*cos(dlon);
    double bear = atan2(y,x)*180.0/M_PI;

   	FAIL(TLDR, "Relative Bearing: %lf\n", bear);

    return (bear - bearing);
}

int
hazards_record::log(bool force) const
{
	(void)force;
    return VALID;
}

}
