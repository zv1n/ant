#include <ant/gray.h>
#include <ant/frame.h>

#define DEBUGGING DEBUG_GRAY
#include <ant/debug.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

using namespace std;

namespace ant
{

define_factory_class(gray, filter, GRAY_FILTER_UCID, filter);

define_factory_class_description(gray,
                                 "Gray Filter\n"
                                 "* Input:\n"
                                 "\tImage Input to convert to Gray Scale.");

gray::gray():
    filter(&m_rFrame,1,0),
    m_fInput(NULL)
{
    set_name("Gray Filter");
    add_property<filter>(input_name(0), m_fInput);
    add_property("Description", gray::m_sDescription, RO);
	set_frame(&m_rFrame);
}

gray::~gray()
{
}

int
gray::process()
{
    if (!m_fInput)
        return INVALID;

    frame* from = m_fInput->current<frame>();
    if (!from)
        return INVALID;

    cv::cvtColor(from->data(), m_rFrame.data(), CV_BGR2GRAY);
    return filter::process();
}

string
gray::input_name(int i) const
{
    switch(i) {
    case 0:
        return "Image";
    }
    return ant::unused;
}

}
