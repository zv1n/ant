#include <ant/response_table.h>

#if DEBUG_RESPONSE_TABLE
#define DEBUG(x, ...) std.log(x, __func__, __line__, __VA_ARGS__)
#else
#define DEBUG(x, ...) do {} while(0)
#endif

#define FAIL(x, ...) err.log(x, __func__, __line__, __VA_ARGS__)

namespace ant
{

response_table::response_table()
{
}

response_table::~response_table()
{
}

int
response_table::add_dimension(int size)
{
    if (size <= 0)
        return INVALID;
    m_vSizes.push_back(size);
    return VALID;
}

unsigned int 
response_table::size() const 
{
	return m_vSizes.size();
}

int
response_table::get_dimension(int x, int &size) const
{
    if (x < 0 || x >= (signed)m_vSizes.size())
        return INVALID;
    size = m_vSizes[x];
    return VALID;
}

int
response_table::set_response(mid_t id, int dim1, ...)
{
    va_list ar;
    va_start(ar, dim1);
    int idx = index_of(dim1, ar);
    va_end(ar);
    if (idx < 0 || idx >= (signed)m_vIds.size())
        return INVALID;
    m_vIds[idx] = id;
    return VALID;
}

int
response_table::get_response(mid_t& id, int dim1, ...) const
{
    va_list ar;
    va_start(ar, dim1);
    int idx = index_of(dim1, ar);
    va_end(ar);
    if (idx < 0 || idx >= (signed)m_vIds.size())
        return INVALID;
    id = m_vIds[idx];
    return VALID;
}

int 
response_table::get_response(mid_t& id, const std::vector<int>& dims) const
{
	if (dims.size() != m_vSizes.size() || !m_vSizes.size())
		return INVALID;

    int radix = 1;
    int index = 0;
	int dv = 0;

    for (unsigned int x=0; x<m_vSizes.size(); x++) {
        dv = dims[x];

        if (dv < 0 || dv >= m_vSizes[x])
            return INVALID;

        index += radix*dv;
        radix *= m_vSizes[x];
    }

	id = m_vIds[index];	
	return VALID;	
}

mid_t
response_table::get_response(int dim1, ...) const
{
    va_list ar;
    va_start(ar, dim1);
    int idx = index_of(dim1, ar);
    va_end(ar);
    if (idx < 0 || idx >= (signed)m_vIds.size())
        return INVALID;
    return m_vIds[idx];
}

int
response_table::set_table(int size, mid_t* ids)
{
    int dimsize = 1;
    for (unsigned int x=0; x<m_vSizes.size(); x++)
        dimsize *= m_vSizes[x];

    if (size != dimsize)
        return INVALID;

    m_vIds.clear();
    for (int x=0; x<size; x++)
        m_vIds.push_back(ids[x]);

    return VALID;
}

int
response_table::index_of(int dim1, va_list va) const
{
    int dims = (signed)m_vSizes.size();
    int radix = 1;
    int index = dim1;
    for (int x=1; x<dims; x++) {
        radix *= m_vSizes[x];
        int dv = va_arg(va, int);
        if (dv < 0 || dv > m_vSizes[x])
            return INVALID;
        index += radix*dv;
    }
    return index;
}

}
