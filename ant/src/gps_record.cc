#include <ant/propertyset.h>
#include <ant/gps_record.h>

#define DEBUGGING DEBUG_GPS_RECORD
#include <ant/debug.h>

#include <math.h>

namespace ant
{

gps_record::gps_record()
{
}

gps_record::~gps_record()
{
}

record*
gps_record::new_collection_record() const
{
    return static_cast<record*>(new gps_record(*this));
}

void
gps_record::dump() const
{
    DUMP("Dumping: %s\n", name().c_str());
    DUMP("Longitude: %f\n", m_dLongitude);
    DUMP("Latitude: %f\n", m_dLatitude);
    DUMP("Altitude: %f\n", m_dAltitude);
    DUMP("Reported Bearing: %f\n", m_dReportedBearing);
}

int
gps_record::size() const
{
    return 1;
}

std::string
gps_record::type()
{
    return GPS_RECORD_TYPE;
}

ucid_t
gps_record::record_ucid()
{
    return GPS_RECORD_UCID;
}

ucid_t
gps_record::ucid() const
{
    return record_ucid();
}

int
gps_record::update_from(record* rec)
{
    gps_record* gps = record_cast<gps_record>(rec);

    if (!gps) {
        DEBUG(TLDR, "Record type does not match GPS Record!\n");
        return INVALID;
    }
    DEBUG(TLDR, "Long: %f\n", longitude());
    DEBUG(TLDR, "Lat: %f\n", latitude());
    DEBUG(TLDR, "Alt: %f\n", altitude());
    DEBUG(TLDR, "Bearing: %f\n", reported_bearing());

    set_longitude(gps->longitude());
    set_latitude(gps->latitude());
    set_altitude(gps->altitude());
    set_reported_bearing(gps->reported_bearing());
    set_time(gps->hours(), gps->minutes(), gps->seconds());

    return record::update_from(rec);
}

double
gps_record::longitude() const
{
    return m_dLongitude;
}

double
gps_record::latitude() const
{
    return m_dLatitude;
}

double
gps_record::altitude() const
{
    return m_dAltitude;
}

double
gps_record::reported_bearing() const
{
    return m_dReportedBearing;
}

uint8_t
gps_record::hours() const
{
    return m_uHours;
}

uint8_t
gps_record::minutes() const
{
    return m_uMinutes;
}

uint8_t
gps_record::seconds() const
{
    return m_uSeconds;
}

int
gps_record::set_longitude(double lon)
{
    m_dLongitude = lon;
    return VALID;
}

int
gps_record::set_latitude(double lat)
{
    m_dLatitude = lat;
    return VALID;
}

int
gps_record::set_altitude(double alt)
{
    m_dAltitude = alt;
    return VALID;
}

int
gps_record::set_reported_bearing(double br)
{
    m_dReportedBearing = br;
    return VALID;
}

int
gps_record::set_time(uint8_t h, uint8_t m, uint8_t s)
{
    m_uHours = h;
    m_uMinutes = m;
    m_uSeconds = s;
    return VALID;
}

int
gps_record::log(bool force) const
{
    IS_LOG_REQUIRED(force);

    char buff[256];
    int fd = get_fd();
    if (fd < 0)
        return INVALID;

    // Don't print shit if the input is invalid.
    if (isnan(longitude())) {
        return VALID;
    }

    snprintf(buff, sizeof(buff), "%3.9lf,%3.9lf,%d,%d\n", longitude(), latitude(),
             (int)altitude(), (int)reported_bearing());

    int ret = write(fd, buff, strlen(buff));
	if (ret < 0)
		return INVALID;

    DEBUG(TLDR, "Reported Bearing: %lf\n", reported_bearing());

    return VALID;
}

}
