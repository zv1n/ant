#include <ant/dim_redux.h>

#define DEBUGGING DEBUG_DIMREDUX
#include <ant/debug.h>

using namespace std;

namespace ant
{

define_factory_class(dimredux, filter, DIMREDUX_FILTER_UCID, filter);

define_factory_class_description(dimredux,
                                 "Dimension Reduction Filter (dimredux)\n"
                                 "* Raw Input:\n"
                                 "\tPreferably RAW Image Input;\n"
                                 "\tGray-Scale may reduce quality.");

dimredux::dimredux():
    filter(&m_rFrame,1,0)
{
    add_property<filter>(input_name(0), m_fInput);
    add_property("Description", dimredux::m_sDescription, RO);
}

dimredux::~dimredux()
{
}

int
dimredux::process()
{
    return filter::process();
}

string
dimredux::input_name(int i) const
{
    switch(i) {
    case 0:
        return "Raw Image";
    }
    return ant::unused;
}

}
