#include <ant/member_record.h>
#include <ant/response_table.h>
#include <ant/std_records.h>

#define DEBUGGING DEBUG_MEMBER_RECORD
#include <ant/debug.h>

#include <stdio.h>
#include <math.h>

#define INVALID_BASE (0x80000000)
#define INVALID_RESPONSE (NAN)

namespace ant
{

member::member(mid_t ms, int base):
    m_iMemberId(ms), m_iBase(base)
{
}

member::member(mid_t ms, float pc) :
    m_iMemberId(ms), m_iBase(INVALID_BASE)
{
    m_vPercent.push_back(pc);
}

member::member(mid_t ms, const std::vector<float>& percent, int base):
    m_iMemberId(ms), m_iBase(base)
{
    m_vPercent.clear();
    std::vector<float>::const_iterator pc;
    for (pc=percent.begin(); pc<percent.end(); pc++)
        m_vPercent.push_back(*pc);
}

member::member(const member& mem)
{
    m_iMemberId = mem.m_iMemberId;
    m_iBase = mem.m_iBase;
    std::vector<float>::const_iterator it;
    m_vPercent.clear();
    for (it=mem.m_vPercent.begin(); it<mem.m_vPercent.end(); it++)
        m_vPercent.push_back(*it);
}

member&
member::operator=(const member& mem)
{
    m_iMemberId = mem.m_iMemberId;
    m_iBase = mem.m_iBase;
    std::vector<float>::const_iterator it;
    m_vPercent.clear();
    for (it=mem.m_vPercent.begin(); it<mem.m_vPercent.end(); it++)
        m_vPercent.push_back(*it);
    return *this;
}

const std::vector<float>&
member::get_list() const
{
    return m_vPercent;
}

int
member::set_list(const std::vector<float>& list)
{
    m_vPercent = list;
    return VALID;
}

int
member::append(const std::vector<float>& list)
{
    m_vPercent.insert(m_vPercent.end(), list.begin(), list.end());
    return VALID;
}

const std::vector<float>::const_iterator
member::begin() const
{
    return m_vPercent.begin();
}

const std::vector<float>::const_iterator
member::end() const
{
    return m_vPercent.end();
}

float
member::converge(int type) const
{
    switch(type) {
    case member::MINIMUM:
        return min_converge();
    case member::PRODUCT:
        return prod_converge();
    }

    return -1.0f;
}

float
member::min_converge() const
{
    float min = 0.0f;
    std::vector<float>::const_iterator it;

    for (it=m_vPercent.begin(); it<m_vPercent.end(); it++)
        if (min > (*it) || min == 0.0f)
            min = (*it);

    return min;
}

void
member::add(float x)
{
    m_vPercent.push_back(x);
}

float
member::prod_converge() const
{
    float min = 1.0f;
    std::vector<float>::const_iterator it;

    if (!m_vPercent.size())
        return 0.0f;

    for (it=m_vPercent.begin(); it<m_vPercent.end(); it++)
        if ((*it) != 0.0f)
            min *= (*it);

    return min;
}

void
member::dump() const
{
#if DEBUG_DUMP
	log(fileno(stdout));
#endif
}

void
member::log(int fd) const
{
	char buff[256] = {0};
    std::vector<float>::const_iterator ft;
    for (ft=m_vPercent.begin(); ft<m_vPercent.end(); ft++) {
		int slen = snprintf(buff, sizeof(buff), "---\t%1.4f\t---\n", (*ft));
    	int ret = write(fd, buff, slen);
	if (ret < 0)
		FAIL(TLDR, "Failed to write to file\n");
	}
}

member_record::member_record():
    m_iUnion(member::MINIMUM),
	m_iActiveMembers(0)
{
}

member_record::member_record(const member_record& mem):
    record(mem),
    m_iUnion(mem.m_iUnion)
{
    member_record::const_iterator it;

    for (it=mem.m_vMembership.begin(); it<mem.m_vMembership.end(); it++)
        m_vMembership.push_back(*it);

	m_iActiveMembers = mem.member_count();

}

member_record::~member_record()
{
}

record*
member_record::new_collection_record() const
{
    return static_cast<record*>(new member_record(*this));
}

int
member_record::size() const
{
    return (int)m_vMembership.size();
}

std::string
member_record::type()
{
    return MEMBER_RECORD_TYPE;
}

ucid_t
member_record::record_ucid()
{
    return MEMBER_RECORD_UCID;
}

ucid_t
member_record::ucid() const
{
    return record_ucid();
}

int
member_record::update_from(record* res)
{
    if (!res || res->ucid() != MEMBER_RECORD_UCID) {
        DEBUG(TLDR, "Record: %p\n", res);
        if (res)
            FAIL(MINOR, "Record NOT a Member Record: %llu != %llu\n",
                 (unsigned long long)res->ucid(),
                 (unsigned long long)MEMBER_RECORD_UCID);
        return INVALID;
    }
    member_record* mr = static_cast<member_record*>(res);
    DEBUG(TLDR, "Updating Record: %s (%zu)\n", res->name().c_str(),
          mr->m_vMembership.size());

    if (mr == this)
        return VALID;

	/* Nastinesss... at some point this should be consolidated into a 
     * member operator for each member element of the record. 
	 */

    member_record::iterator it;
    for (it=mr->m_vMembership.begin(); it<mr->m_vMembership.end(); it++) {
        member_record::iterator mem = get_member((*it).member_id());
        if (mem < m_vMembership.end()) {
            (*mem).m_vPercent.clear();
            std::vector<float>::const_iterator pc;
            for (pc=(*it).begin(); pc<(*it).end(); pc++) {
                (*mem).m_vPercent.push_back(*pc);
            }
            continue;
        }

        m_vMembership.push_back(member((*it).member_id(), (*it).get_list()));
    }

	m_iActiveMembers = mr->member_count();

    return VALID;
}


int
member_record::set_union_type(int type)
{
    m_iUnion = type;
    return VALID;
}

int
member_record::union_type() const
{
    return m_iUnion;
}

int
member_record::set_defuzz_type(int type)
{
    m_iDefuzz = type;
    return VALID;
}

int
member_record::defuzz_type() const
{
    return m_iDefuzz;
}

mid_t
member_record::get_member_id(unsigned int x) const
{
	unsigned int it = 0;
	for (it=0; it < m_vMembership.size(); it++) {
		if (m_vMembership[it].size()) {
			if (!x--)
				return m_vMembership[it].member_id();
		}
	}
		
	return -1;
}

unsigned int
member_record::member_count() const 
{
	return m_iActiveMembers;
}

member_record::iterator
member_record::get_member(mid_t mid)
{
    std::vector<member>::iterator it;
    for (it=m_vMembership.begin(); it<m_vMembership.end(); it++)
        if ((*it).member_id() == mid) {
            return it;
        }

    return m_vMembership.end();
}

member_record::const_iterator
member_record::get_member(mid_t mid) const
{
    member_record::const_iterator it;
    for (it=m_vMembership.begin(); it<m_vMembership.end(); it++)
        if ((*it).member_id() == mid) {
            return it;
        }

    return m_vMembership.end();
}

int
member_record::set_member(const member& mem)
{
    DEBUG(TLDR, "Setting Member...\n");
    member_record::iterator it = get_member(mem.member_id());
    if (it != m_vMembership.end()) {
        DEBUG(TLDR, "Seting member list...\n");
        m_iActiveMembers -= it->size(); 
		it->set_list(mem.get_list());
		m_iActiveMembers += mem.size();
        return VALID;
    }

    DEBUG(TLDR, "Member Doesn't Exist! Adding member list...\n");
    m_vMembership.push_back(mem);
    return VALID;
}

int
member_record::set_values(int size, mid_t* ids, int* values)
{
    DEBUG(TLDR, "Setting Values...\n");
    m_vMembership.clear();
    for (int x=0; x<size; x++) {
        m_vMembership.push_back(member(ids[x], values[x]));
	}
	m_iActiveMembers = 0;
    return VALID;
}

int
member_record::add_member(mid_t memberId, float pc)
{
    member_record::iterator it = get_member(memberId);
	m_iActiveMembers++;

    if (it == m_vMembership.end()) {
        m_vMembership.push_back(member(memberId, pc));
        return VALID;
    }

    (*it).m_vPercent.push_back(pc);
    return VALID;
}

int
member_record::get_member(mid_t memberId, member& mem) const
{
    member_record::const_iterator it = get_member(memberId);
    if (it == m_vMembership.end())
        return INVALID;
    mem = *it;
    return VALID;
}

int
member_record::get_member(mid_t memberId, float& percent) const
{
    member_record::const_iterator it = get_member(memberId);
    if (it == m_vMembership.end())
        return INVALID;
    percent = (*it).percent(m_iUnion);
    return VALID;
}

int
member_record::clear()
{
    DEBUG(TLDR, "Removing all items from record: %s\n", name().c_str());
    member_record::iterator it;
    for (it=m_vMembership.begin(); it<m_vMembership.end(); it++)
        (*it).clear();
	m_iActiveMembers = 0;
    return VALID;
}

member_record::const_iterator
member_record::begin() const
{
    return m_vMembership.begin();
}

member_record::const_iterator
member_record::end() const
{
    return m_vMembership.end();
}

member_record::iterator
member_record::begin()
{
    return m_vMembership.begin();
}

member_record::iterator
member_record::end()
{
    return m_vMembership.end();
}

void
member_record::dump() const
{
#if DEBUG_DUMP
	log(fileno(stdout));
#endif
}

void
member_record::log(int fd) const
{
	char buff[256] = {0};
	int slen = snprintf(buff, sizeof(buff), "Name: %s\n", name().c_str());
   	int ret = write(fd, buff, slen);
	if (ret < 0)
		FAIL(TLDR, "Failed to write to file\n");
    ret = write(fd, "ID\tVALUES\tBASE\n", strlen("ID\tVALUES\tBASE\n"));
	if (ret < 0)
		FAIL(TLDR, "Failed to write to file\n");

    member_record::const_iterator it;
    for (it=m_vMembership.begin(); it<m_vMembership.end(); it++) {
		slen = snprintf(buff, sizeof(buff), "%03d\t%1.4f\t%d\n", 
						(*it).member_id(), (*it).percent(m_iUnion), 
						(*it).base());
    	ret = write(fd, buff, slen);
	if (ret < 0)
		FAIL(TLDR, "Failed to write to file\n");
        (*it).log(fd);
	}
}

float
member_record::defuzz(int type) const
{
    switch(type) {
    case SUM_OF_PRODUCTS:
        return defuzz_sop();
    }
    return INVALID_RESPONSE;
}

float
member_record::defuzz_sop() const
{
    DEBUG(TLDR, "Name: %s\n", name().c_str());
    DEBUG(TLDR, "ID\tVALUES\tBASE\n");
    member_record::const_iterator it;
    float sum = 0.0f;
    float products = 0.0f;
    for (it=m_vMembership.begin(); it<m_vMembership.end(); it++) {
        float conv = (*it).converge(m_iUnion);
        float base = (float)(*it).base();
        sum += conv;
        products += conv*base;
    }
    return (products/sum);
}

int
member_record::map_input(const integer_record* rec)
{
	if (!rec) {
		FAIL(TLDR, "Invalid input integer record!\n");
		return INVALID;
	}
	return map_input(rec->value());
}

/**
 * @fn int member_record::map_input(const integer_record* rec)
 * This function is used to map a given value to the fuzzification
 * function that it is associated with.
 *
 *  ___0  10  20  30 40___
 *     \  /\  /\  /\  /
 *      \/  \/  \/  \/
 *      /\  /\  /\  /\
 *     /  \/  \/  \/  \
 *  ----------------------
 * 
 * The value passed in is aligned with the values associated with the 
 * given member value set.  If the call were to map_input(15), with the
 * specified value set of {0,10,20,30,40}, the member set would contain the
 * members {(1,0.5),(2,0.5)}.
 */
int
member_record::map_input(int val)
{
	DEBUG(DANGER_WILL_ROBINSON, "Mapping %d from %s\n", val, name().c_str());
    clear();

    std::vector<member>::iterator btm = m_vMembership.begin();

    if (val < (*btm).base()) {
        (*btm).add(1.0f);
    	DEBUG(TLDR, "(%s) %d > *%d\n", name().c_str(), (*btm).base(), val);
		m_iActiveMembers++;
        return VALID;
    }

    for (; btm<m_vMembership.end(); btm++) {
        if (val == (*btm).base()) {
            (*btm).add(1.0f);
    		DEBUG(TLDR, "(%s) %d == *%d\n", name().c_str(), (*btm).base(), val);
			m_iActiveMembers++;
            return VALID;
        }

        if (val < (*btm).base()) {
            btm--;
            break;
        }

        if (btm+1 == m_vMembership.end()) {
            (*btm).add(1.0f);
    		DEBUG(TLDR, "(%s) *%d > %d\n", name().c_str(), (*btm).base(), val);
			m_iActiveMembers++;
            return VALID;
        }

    }

    int hi = (*(btm+1)).base();
    int low = (*btm).base();

    DEBUG(TLDR, "(%s) %d > *%d > %d\n", name().c_str(), hi, val, low);

    float low_percent = (float)(val-low)/(float)(hi-low);
    (*(btm+1)).add(low_percent);
    (*btm).add(1.0f-low_percent);
	m_iActiveMembers += 2;

    return VALID;
}

/**
 * @fn int member_record::cart_product(const repsonse_table& table, ...)
 * This functions is used to generate the output member_record of a given
 * reponse table and input member_records.
 * @param table Table to be used in resolving the output fuzzy members.
 * @param ... The list of pointers to member_records containing the input set.
 * @return Number of total output members.
 */
int 
member_record::cart_product(const response_table& table, ...)
{
	va_list ar;
	unsigned int size = table.size();
	unsigned int x = 0;
	std::vector<member_record*> inputs;
		
	if (size > 30) {
		FAIL(MAJOR, "Greater than 30 dimensions!!! Jesus Christ!\n");
		return 0;
	}

	/* Get the values passed into cart_product */
	va_start(ar, table);
	for (x=0; x<size; x++) {
		member_record* mem = va_arg(ar, member_record*);
		if (!mem) {
			FAIL(TLDR, "Null Member passed to cart_product!!\n");
			va_end(ar);
			return 0;
		}

		inputs.push_back(mem);
	}
	va_end(ar);

	return cart_product(table, inputs);
}

int 
member_record::cart_product(const response_table& table, 
							std::vector<member_record*>& inputs)
{
	std::vector<mid_t> entries(table.size());
	int ret = 0;

	clear();

	/* Recurse through all valid combinations of members */
	ret = recurse_table(table, inputs, entries, 0);
	if (ret) {
		FAIL(DANGER_WILL_ROBINSON, "Failed TL Recurse!\n");
		clear();
	}
	
	return m_vMembership.size();
}

int
member_record::recurse_table(const response_table& table, 
							std::vector<member_record*>& rec,
							std::vector<mid_t>& entries, unsigned int idx)
{
	int ret = 0;
	mid_t response = 0;

	if (entries.size() != table.size() ||
		rec.size() != table.size()) {
		FAIL(DANGER_WILL_ROBINSON, "Invalid sizes...\n");
		return INVALID;
	}

	if (idx == entries.size()) {
		table.get_response(response, entries);

		member mem;
    	member_record::iterator it = get_member(response);
    	if (it == m_vMembership.end()) {

			FAIL(DANGER_WILL_ROBINSON, "Last member! Response: %d\n",
					response);

    	    return INVALID;
		}

		unsigned int size = entries.size();

		for (unsigned int x=0; x<size; x++) {
			rec[x]->get_member(entries[x], mem);
			if (!(*it).size())
				m_iActiveMembers++;
			(*it).append(mem.get_list());
		}	
		return VALID;
	}

	if (!rec[idx]) {
		FAIL(DANGER_WILL_ROBINSON, "NULL record...\n");
		return INVALID;
	}

	unsigned int size = (unsigned int)rec[idx]->member_count();

	for (unsigned int x=0; x<size; x++) {
		response = rec[idx]->get_member_id(x);
		entries[idx] = response;

		ret = recurse_table(table, rec, entries, idx+1);
		if (ret) {
			FAIL(DANGER_WILL_ROBINSON, "Failed to recurse the tables!\n");
			return INVALID;
		}
	}

	return VALID;
}

int
member_record::log(bool force) const
{
    IS_LOG_REQUIRED(force);

    int fd = get_fd();
    if (fd < 0)
        return INVALID;

	log(fd);
    return VALID;
}

}
