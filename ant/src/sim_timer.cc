#include <ant/sim_timer.h>

#define DEBUGGING DEBUG_SIM_TIMER
#include <ant/debug.h>

#include <sys/time.h>

namespace ant
{

sim_timer::sim_timer(int interval):
    m_iInterval(interval)
{
    add_property("LastUpate", m_ulLastUpdate, READ);
    add_property("Interval", m_iInterval);
    set_postset("Interval", this, (propset_fn)&sim_timer::update_interval);
}

sim_timer::~sim_timer()
{
}

int
sim_timer::set_interval(int interval)
{
    m_iInterval = interval;
    m_ulLastUpdate = ~(0x0UL);

    if (m_iInterval < 0)
        return VALID;
    return INVALID;
}

int
sim_timer::update_interval(std::string& pname)
{
	(void)pname;
    if (m_iInterval < 0)
        return INVALID;
    return VALID;
}

int
sim_timer::interval()
{
    return m_iInterval;
}

unsigned long
sim_timer::get_msecs()
{
    struct timeval start;
    gettimeofday(&start, NULL);
    return (start.tv_sec*1000 + start.tv_usec/1000);
}

int
sim_timer::update()
{
    if (m_iInterval < 0)
        return -1;

    unsigned long currentUpdate = get_msecs();
    unsigned long delta = currentUpdate - m_ulLastUpdate;
    int skipCnt = delta/m_iInterval;

    DEBUG(TLDR, "Last Update: %u\n", m_ulLastUpdate);
    DEBUG(TLDR, "Current Update: %u\n", currentUpdate);
    DEBUG(TLDR, "Delta: %u\n", delta);
    DEBUG(TLDR, "Interval: %d\n", m_iInterval);

    /* if this is the first update, then only update 1 */
    if (m_ulLastUpdate == ~(0x0UL))
        skipCnt = 1;

    if (skipCnt == 0)
        return 0;

    m_ulLastUpdate = currentUpdate;

    return skipCnt;
}

}
