#include <ant/bearing_defuz.h>
#include <ant/member_record.h>
#include <ant/nav_record.h>

#define DEBUGGING DEBUG_BEARING_DEFUZ
#include <ant/debug.h>

namespace ant
{

define_factory_class(bearing_defuzzifier, defuzzifier, BEARING_DEFUZZ_UCID,
                     defuzzifier);

#define MAX_VALUE (1000)

/**
 * Constructor for the Bearing NearDefuzifier.  This constructor initializes all
 * necessary components which do not change between runs.  This list includes
 * the response table and the output range set.
 */
bearing_defuzzifier::bearing_defuzzifier():
    defuzzifier(&m_rBearing)
{
    mid_t near_responses[MAX_OUTPUT_BEARING] = {
        /*FUZ_BEARING*/
        /*LL,		 LS,		Z,	  RS,		RL 	*/
        CCW_LARGE, CCW_SMALL, ZERO, CW_SMALL, CW_LARGE
    };

    mid_t bhv_responses
		[MAX_INPUT_BEARING][MAX_INPUT_BEARING][MAX_INPUT_BEARING] = {
        {
			{ CCW_LARGE, CCW_LARGE, CCW_LARGE, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_LARGE, CCW_SMALL, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_SMALL, CCW_LARGE, 	CW_SMALL, 	CW_SMALL},
        	{ CW_SMALL, CW_SMALL, 	CW_SMALL, 	CW_LARGE, 	CW_LARGE},
        	{ CW_SMALL, CW_SMALL, 	CW_LARGE, 	CW_LARGE, 	CW_LARGE}
		},
        {
			{ CCW_LARGE, CCW_LARGE, CCW_LARGE, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_LARGE, CCW_SMALL, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_SMALL, CCW_SMALL, 	CW_SMALL, 	CW_SMALL},
        	{ CW_SMALL, CW_SMALL, 	CW_SMALL, 	CW_LARGE, 	CW_LARGE},
        	{ CW_SMALL, CW_SMALL, 	CW_LARGE, 	CW_LARGE, 	CW_LARGE}
		},
        {
			{ CCW_LARGE, CCW_LARGE, CCW_LARGE, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_LARGE, CCW_SMALL, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_SMALL, ZERO, 		CW_SMALL, 	CW_SMALL},
        	{ CW_SMALL, CW_SMALL, 	CW_SMALL, 	CW_LARGE, 	CW_LARGE},
        	{ CW_SMALL, CW_SMALL, 	CW_LARGE, 	CW_LARGE, 	CW_LARGE}
		},
        {
			{ CCW_LARGE, CCW_LARGE, CCW_LARGE, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_LARGE, CCW_SMALL, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_SMALL, CW_SMALL, 	CW_SMALL, 	CW_SMALL},
        	{ CW_SMALL, CW_SMALL, 	CW_SMALL, 	CW_LARGE, 	CW_LARGE},
        	{ CW_SMALL, CW_SMALL, 	CW_LARGE, 	CW_LARGE, 	CW_LARGE}
		},
        {
			{ CCW_LARGE, CCW_LARGE, CCW_LARGE, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_LARGE, CCW_SMALL, 	CCW_SMALL, 	CCW_SMALL},
        	{ CCW_LARGE, CCW_SMALL, CW_LARGE, 	CW_SMALL, 	CW_SMALL},
        	{ CW_SMALL, CW_SMALL, 	CW_SMALL, 	CW_LARGE, 	CW_LARGE},
        	{ CW_SMALL, CW_SMALL, 	CW_LARGE, 	CW_LARGE, 	CW_LARGE}
		}
    };

    /* Add a dimension of size 5 to the table */
    m_rtNearTable.add_dimension(MAX_INPUT_BEARING);

    m_rtBHVTable.add_dimension(MAX_INPUT_BEARING);
    m_rtBHVTable.add_dimension(MAX_INPUT_BEARING);
    m_rtBHVTable.add_dimension(MAX_INPUT_BEARING);

    m_rtNearTable.set_table(sizeof(near_responses)/sizeof(near_responses[0]), 
			&near_responses[0]);
    m_rtBHVTable.set_table(sizeof(bhv_responses)/sizeof(bhv_responses[0][0][0]),
			&bhv_responses[0][0][0]);

    int values[MAX_OUTPUT_BEARING] = {
        -1*MAX_VALUE, -1*MAX_VALUE*0.75, 0, MAX_VALUE*0.75, MAX_VALUE
    };

    m_rNearDefuz.set_values(MAX_OUTPUT_BEARING, &near_responses[0], &values[0]);
    m_rFarDefuz.set_values(MAX_OUTPUT_BEARING, &near_responses[0], &values[0]);

    set_name("DFRelativeBearing");

    m_rNearDefuz.set_name("FCNearBearingDEFUZZ");
    m_rFarDefuz.set_name("FCFarBearingDEFUZZ");
    m_rBearing.set_name("FCOutputBearing");

    DEBUG(TLDR, "Bearing Creation...\n");
}

bearing_defuzzifier::~bearing_defuzzifier()
{
}

/**
 * This function defines the inference mechanism between the
 * fuzzified inputs and the defuzzified outputs.  This is handled
 * using the response_table class.
 */
int
bearing_defuzzifier::inference()
{
    collection* members = collect(IN);
    if (!members) {
        FAIL(DANGER_WILL_ROBINSON, "Null input collection...\n");
        return INVALID;
    }

    member_record* img_near_bearing = 
		members->get<member_record>("FCImageNearBearing");
    if (!img_near_bearing) {
        FAIL(DANGER_WILL_ROBINSON, "NULL member record 'FCNearImageBearing'\n");
        return INVALID;
    }

    member_record* img_bearing = 
		members->get<member_record>("FCImageFarBearing");
    if (!img_bearing) {
        FAIL(DANGER_WILL_ROBINSON, "NULL member record 'FCFarImageBearing'\n");
        return INVALID;
    }

    member_record* gps_bearing = members->get<member_record>("FCDestBearing");
    if (!gps_bearing) {
        FAIL(DANGER_WILL_ROBINSON, "NULL member record 'FCDestBearing'\n");
        return INVALID;
    }

    member_record* gh_bearing = members->get<member_record>("FCGlobHazard");
    if (!gh_bearing) {
        FAIL(DANGER_WILL_ROBINSON, "NULL member record 'FCBearing'\n");
        return INVALID;
    }

	gh_bearing->dump();
	gps_bearing->dump();
	img_bearing->dump();
    m_rNearDefuz.clear();
    DEBUG(ALL, "Post Clear\n");

	float amnt = 0;
	img_near_bearing->get_member(ZERO, amnt);
	FAIL(DANGER_WILL_ROBINSON, "ZERO: %f\n", amnt);

	if (amnt != 1.0f) {
		DEBUG(MINOR, "Near Defuzz\n");
		m_rNearDefuz.cart_product(m_rtNearTable, img_near_bearing);
    	m_rNearDefuz.dump();
		m_bNear = true;
	} else {
		DEBUG(MINOR, "Far Defuzz\n");
		m_rFarDefuz.cart_product(m_rtBHVTable, gh_bearing, img_bearing, 
									gps_bearing);
		m_rFarDefuz.dump();
		m_bNear = false;
	}

    return VALID;
}

/**
 * This function handles the actual defuzzification by means of
 * the helper functions associated with the member_record class.
 */
int
bearing_defuzzifier::defuzzify()
{
    DEBUG(TLDR, "NearDefuzify: %s\n", m_rBearing.name().c_str());

    float sop = 0.0f;

	if (m_bNear)
		sop = m_rNearDefuz.defuzz(member_record::SUM_OF_PRODUCTS);
	else
		sop = m_rFarDefuz.defuzz(member_record::SUM_OF_PRODUCTS);


    m_rBearing.set_value(sop/(float)MAX_VALUE);
    DEBUG(TLDR, "Getting Update: %f\n", sop);
    FAIL(TLDR, "Bearing: %f\n", m_rBearing.value());

    collection* coll = collect(OUT);
    if (!coll) {
        FAIL(DANGER_WILL_ROBINSON, "No Output Collection!\n");
        return INVALID;
    }

    int ret = coll->update(&m_rBearing);
    DEBUG(MAJOR, "Collection Update: %d\n", ret);
    return ret;
}

}
