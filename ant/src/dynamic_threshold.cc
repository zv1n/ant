#include <ant/dynamic_threshold.h>
#include <ant/frame.h>

#define DEBUGGING DEBUG_DYN_THRESH
#include <ant/debug.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

using namespace std;

namespace ant
{

define_factory_class(dynamic_threshold, filter, DYN_THRESHOLD_FILTER_UCID, filter);

define_factory_class_description(dynamic_threshold,
                                 "Dynamic Threshold Filter\n"
                                 "* Input:\n"
                                 "\tImage Input to Apply Dynamic.");

dynamic_threshold::dynamic_threshold():
    filter(&m_rFrame,1,0),
    m_fInput(NULL), m_iFooterThresh(0),
    m_iMaxDelay(7), m_iFooterDelay(m_iMaxDelay)
{
#if DEBUG_DYN_THRESHOLD
    cout << "Creating dynamic_threshold filter..." << endl;
#endif
    set_name("Dynamic Threshold Filter");
    add_property<filter>(input_name(0), m_fInput);
    add_property("Description", dynamic_threshold::m_sDescription, RO);
    add_property("Max Delay", m_iMaxDelay);
}

dynamic_threshold::~dynamic_threshold()
{
}

int
dynamic_threshold::process()
{
    if (!m_fInput)
        return INVALID;

    frame* from = m_fInput->current<frame>();
    if (!from)
        return INVALID;

    int footerTresh_reserve;
    cv::Mat& input = from->data();
    cv::Mat& thresh = m_rFrame.data();
    cv::Scalar footerMean;
    cv::Scalar footerStdev;

    if (!input.rows || !input.cols)
        return INVALID;

    if (m_iFooterDelay >= m_iMaxDelay) {
        cv::Mat footerFrame;
        footerFrame = input;
        int height = footerFrame.rows * .15;
        int width = footerFrame.cols;
        int xpos = 0;
        int ypos = footerFrame.rows - height;
        cv::Mat roi(footerFrame, cv::Rect(xpos, ypos, width, height));

        cv::meanStdDev(footerFrame, footerMean, footerStdev);
        m_iFooterThresh = round(footerMean[0]);
        footerTresh_reserve = m_iFooterThresh;

        m_iFooterThresh = m_iFooterThresh * 1.5;
        if (m_iFooterThresh > 255)
            m_iFooterThresh = footerTresh_reserve;

        m_iFooterDelay = 0;
    }

    m_iFooterDelay++;
    cv::threshold(input, thresh, m_iFooterThresh, 255, CV_THRESH_BINARY_INV);
    return filter::process();
}

string
dynamic_threshold::input_name(int i) const
{
    switch(i) {
    case 0:
        return "Image";
    }
    return ant::unused;
}

}
