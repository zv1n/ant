#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <asm/types.h>    

#include <ant/filter.h>
#include <ant/video_v4l.h>
#include <ant/frame.h>

#define DEBUGGING DEBUG_VIDEO_V4L
#include <ant/debug.h>
#include <ant/sim_timer.h>

#include <opencv/cv.h>

#include <errno.h>
#include <vector>
#include <sstream>
#include <fcntl.h>
#include <unistd.h>
#include <malloc.h>

using namespace std;

static int
xioctl(int fd, int request, void *arg)
{
        int r;
        do r = ioctl (fd, request, arg);
        while (-1 == r && EINTR == errno);
        return r;
}

namespace ant
{

define_factory_class(video_v4l, filter, VIDEOIN_V4L_FILTER_UCID, input);

video_v4l::video_v4l(std::string cap):
    filter(&m_rFrame,0,0), m_upFrame(NULL)
{

    configure_properties();
	set_frame(&m_rFrame);
    filter::set_name("V4L Driver");
    if (cap.length() == 0) {
        return;
    }

	m_sDevice = cap;
	if (init_v4l()) 
		FAIL(DANGER_WILL_ROBINSON, "Failed to stat video device!\n");
}

video_v4l::video_v4l():
    filter(&m_rFrame,0,0), m_upFrame(NULL)
{
    filter::set_name("V4L Driver");
    configure_properties();
}

void
video_v4l::configure_properties()
{
    add_property("Width", m_iWidth);
    add_property("Height", m_iHeight);
    add_property("SSE", m_iUseSSE);
    add_property("Device", m_sDevice);
    set_postset("Device", this, (propset_fn)&ant::video_v4l::device_changed);

	m_iWidth = 480;
	m_iHeight = 640;
	m_iUseSSE = 1;
	m_iActive = 0;
	m_iDevice = -1;
	m_iBufferCount = 0;
	m_pfcConversion = NULL;
	m_rFrame.set_name("raw");
}

video_v4l::~video_v4l()
{
	cleanup_v4l();
}

int
video_v4l::process()
{
	if (m_iDevice < 0)
		return INVALID;

	int time = sim_timer::get_msecs();
	int ret = update_frame();
	int delta = sim_timer::get_msecs() - time;
	printf("Delta: %d\n", delta);
	return (ret | filter::process());
}

int
video_v4l::convert(uint8_t* buffer) 
{
    if (!m_pfcConversion || !(m_pfcConversion->convert)) {
		FAIL(DANGER_WILL_ROBINSON, "Conversion function is invalid!\n");
		return INVALID;
    } 

    cv::Mat& rgb_mat = m_rFrame.data();
    m_pfcConversion->convert(m_pfcConversion, buffer, rgb_mat.data);
	return VALID;
}

int
video_v4l::device_changed(std::string& name)
{
	(void)name;
    return init_v4l();
}

std::string
video_v4l::input_name(int i) const
{
	(void)i;
    return "unused";
}

int
video_v4l::open_device()
{
    struct stat st;

    if (-1 == stat(m_sDevice.c_str(), &st)) {
		FAIL(DANGER_WILL_ROBINSON, "Failed to stat video device!\n");
		return INVALID;
    }

    if (!S_ISCHR(st.st_mode)) {
		FAIL(DANGER_WILL_ROBINSON, "Specified file is not a char device!\n");
		return INVALID;
    }

    m_iDevice = open(m_sDevice.c_str(), O_RDWR | O_NONBLOCK, 0);

    if (-1 == m_iDevice) {
		FAIL(DANGER_WILL_ROBINSON, "Failed to open video device!\n");
		return INVALID;
    }

	return VALID;
}

int
video_v4l::init_device()
{
    struct v4l2_capability cap;
    struct v4l2_format fmt;

    if (-1 == xioctl(m_iDevice, VIDIOC_QUERYCAP, &cap)) {
		FAIL(DANGER_WILL_ROBINSON, "Failed to query capabilities!\n");
		return INVALID;
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
		FAIL(DANGER_WILL_ROBINSON, "Device does not support Video Capture!\n");
		return INVALID;
    }

    if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
		FAIL(DANGER_WILL_ROBINSON, "Device does not support Streaming!\n");
		return INVALID;
    }

    memset(&fmt, 0, sizeof(fmt));
    fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width       = m_iWidth;
    fmt.fmt.pix.height      = m_iHeight;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    fmt.fmt.pix.field       = V4L2_FIELD_ANY;

    if (-1 == xioctl(m_iDevice, VIDIOC_S_FMT, &fmt)) {
		FAIL(DANGER_WILL_ROBINSON, "Device does not support Streaming!\n");
		return INVALID;
    }

    /* Note VIDIOC_S_FMT may change width and height. */
	unsigned int min = fmt.fmt.pix.width * 2;
    if (fmt.fmt.pix.bytesperline < min)
        fmt.fmt.pix.bytesperline = min;
    min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
    if (fmt.fmt.pix.sizeimage < min)
        fmt.fmt.pix.sizeimage = min;

	m_iWidth = fmt.fmt.pix.height;
	m_iHeight = fmt.fmt.pix.width;

	m_rFrame.data().create(m_iWidth, m_iHeight, CV_8UC3);

	PixFcFlag type = m_iUseSSE? PixFcFlag_SSE2Only : PixFcFlag_NoSSE;
    if (create_pixfc(&m_pfcConversion, PixFcYUYV, PixFcBGR24, 
								m_iWidth, m_iHeight, type)) {
		FAIL(DANGER_WILL_ROBINSON, "Failed to create conversion object!\n");
		return INVALID;
	}

	return VALID;
}

int
video_v4l::init_v4l()
{
	cleanup_v4l();

	if (open_device()) {
		FAIL(DANGER_WILL_ROBINSON, "Failed to open video device!\n");
		return INVALID;
	}

	if (init_device()) {
		FAIL(DANGER_WILL_ROBINSON, "Failed to init video device!\n");
		return INVALID;
	}
	
    struct v4l2_requestbuffers req;

    memset(&req, 0, sizeof(req));

    req.count               = 1;
    req.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory              = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(m_iDevice, VIDIOC_REQBUFS, &req))  {
		FAIL(DANGER_WILL_ROBINSON, "Failed to configure video buffers!\n");
		return INVALID;
	}

    m_v4lBuffers = (v4l_buffer*)calloc(req.count, sizeof(*m_v4lBuffers));

    if (!m_v4lBuffers) {
        FAIL(DANGER_WILL_ROBINSON, "Out of memory\n");
		return INVALID;
    }

    for (m_iBufferCount = 0; m_iBufferCount < req.count; ++m_iBufferCount) {
        struct v4l2_buffer buf;

        memset(&buf, 0, sizeof(buf));

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = m_iBufferCount;

        if (-1 == xioctl (m_iDevice, VIDIOC_QUERYBUF, &buf)) {
			FAIL(DANGER_WILL_ROBINSON, "Failed to recieve video buffers!\n");
			return INVALID;
		}

        m_v4lBuffers[m_iBufferCount].length = buf.length;
        m_v4lBuffers[m_iBufferCount].start =
                        mmap (NULL /* start anywhere */,
                              buf.length,
                              PROT_READ | PROT_WRITE /* required */,
                              MAP_SHARED /* recommended */,
                              m_iDevice, buf.m.offset);

        if (MAP_FAILED == m_v4lBuffers[m_iBufferCount].start) {
			FAIL(DANGER_WILL_ROBINSON, "Failed to recieve video buffers!\n");
			return INVALID;
		}
    }

    enum v4l2_buf_type type;
    struct v4l2_buffer buf;

    for (unsigned int i = 0; i < m_iBufferCount; ++i) {
		memset(&buf, 0, sizeof(buf));

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = i;

        if (-1 == xioctl(m_iDevice, VIDIOC_QBUF, &buf)) {
			FAIL(DANGER_WILL_ROBINSON, "Failed to initialize video buffers!\n");
			return INVALID;
		}
	}

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (-1 == xioctl(m_iDevice, VIDIOC_STREAMON, &type)) {
		FAIL(DANGER_WILL_ROBINSON, "Failed to initialize video streams!\n");
		return INVALID;
	}

	m_iActive = 1;

	return VALID;
}

int
video_v4l::cleanup_v4l() 
{
	if (m_iDevice < 0) {
		FAIL(DANGER_WILL_ROBINSON, "Can't clean invalid device!\n");
		return VALID;
	}

	m_iActive = 0;

    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (-1 == xioctl(m_iDevice, VIDIOC_STREAMOFF, &type)) {
		FAIL(DANGER_WILL_ROBINSON, "Failed to initialize video streams!\n");
	}

  	for (unsigned int i = 0; i < m_iBufferCount; ++i)
        if (-1 == munmap(m_v4lBuffers[i].start, m_v4lBuffers[i].length)) {
			FAIL(DANGER_WILL_ROBINSON, "Failed to unmap video stream!\n");
		}

    free(m_v4lBuffers);

    destroy_pixfc(m_pfcConversion);
    m_pfcConversion = NULL;

  	close(m_iDevice);
    m_iDevice = -1;

	if (m_upFrame)
		delete [] m_upFrame;

	printf("V4L Driver Cleanup Success!\n");

	return VALID;
}

int
video_v4l::update_frame()
{
	struct v4l2_buffer buf;

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;
	int ret = VALID;

#if DRAIN_V4L_QUEUE
	/* Drane the Driver's Frame Queue... Still looking for a more elagent method
	*/
    while (-1 != xioctl(m_iDevice, VIDIOC_DQBUF, &buf)) {
    	if (buf.index >= m_iBufferCount) {
			FAIL(DANGER_WILL_ROBINSON, "Failed to DQBUF!\n");
			return INVALID;
		}
		if (!m_upFrame)
			m_upFrame = new uint8_t[m_v4lBuffers[buf.index].length*2];
		memcpy(m_upFrame, m_v4lBuffers[buf.index].start, 
					m_v4lBuffers[buf.index].length);
		if (-1 == xioctl(m_iDevice, VIDIOC_QBUF, &buf)) {
			FAIL(DANGER_WILL_ROBINSON, "Can't clean invalid device!\n");
			return INVALID;
		}
	}

	if (m_upFrame)
		ret = convert(m_upFrame);

#else

    while (-1 == xioctl(m_iDevice, VIDIOC_DQBUF, &buf))
		if (errno != EAGAIN) {
			FAIL(DANGER_WILL_ROBINSON, "Failed to DQBUF!\n");
			return INVALID;
		}


    if (buf.index >= m_iBufferCount) {
		FAIL(DANGER_WILL_ROBINSON, "Failed to DQBUF!\n");
		ret = INVALID;
	} else {
		ret = convert((uint8_t*)m_v4lBuffers[buf.index].start);
	}	
	
	if (-1 == xioctl(m_iDevice, VIDIOC_QBUF, &buf)) {
		FAIL(DANGER_WILL_ROBINSON, "Can't clean invalid device!\n");
		return INVALID;
	}
#endif

	return ret;
}

bool
video_v4l::is_opened()
{
    return m_iActive;
}


}
