#include <ant/sim_controller.h>

#define DEBUGGING DEBUG_SIM_CONTROLLER
#include <ant/debug.h>

#include <iomanip>
#include <math.h>

#define PI (3.141592653589793)

namespace ant
{

sim_controller::sim_controller():
    controller(&m_rFrame,1,0)
{
    add_property<filter>(input_name(0), m_fInput);
    set_name("Controller Simulation");
    m_rFrame.set_name("SC Raw Image");
	set_frame(&m_rFrame);
}

sim_controller::~sim_controller() {}

std::string
sim_controller::input_name(int x) const
{
    if (!x)
        return "Image";
    return "unused";
}

int
sim_controller::process()
{
    collection* coll = collect(IN);
    if (!coll) {
        FAIL(DANGER_WILL_ROBINSON, "Failed to get input collection.\n");
        return INVALID;
    }

    float_record* rec = coll->get<float_record>("FCOutputBearing");
    if (!rec) {
        FAIL(TLDR, "Failed to retrieve output bearing.\n");
        return INVALID;
    }

    if (!m_fInput) {
        FAIL(DANGER_WILL_ROBINSON, "Input frame not set!\n");
        return INVALID;
    }

    frame* inframe = m_fInput->current<frame>();
    if (!inframe) {
        FAIL(DANGER_WILL_ROBINSON, "Input record is not a frame record.\n");
        return INVALID;
    }

    cv::Mat& cep = m_rFrame.data();
    cep = inframe->data();

    cv::Point2i	start(cep.cols/2, cep.rows);
    cv::Point2i end(start);
    int len = 256;

    end.x += sin(rec->value()*PI)*len;
    end.y -= cos(rec->value()*PI)*len;

    std::stringstream ss;
    ss << "Value: ";
    ss << std::setprecision(std::numeric_limits<float>::digits10+1);
    ss << rec->value()*90.0;

    cv::putText(cep, ss.str(), cv::Point2i(10, 60),
                cv::FONT_HERSHEY_SIMPLEX, 2.5f, CV_RGB(255,0,0), 4);

    cv::line(cep, start, end, CV_RGB(255,0,0), 4);

    return controller::process();
}

}

