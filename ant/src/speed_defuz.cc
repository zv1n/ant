#include <ant/speed_defuz.h>
#include <ant/member_record.h>
#include <ant/nav_record.h>

#define DEBUGGING DEBUG_SPEED_DEFUZ
#include <ant/debug.h>

namespace ant
{

#define SPEED_INCR (0.1)

define_factory_class(speed_defuzzifier, defuzzifier, SPEED_DEFUZZ_UCID,
                     defuzzifier);

#define MAX_VALUE (1000)

/**
 * Constructor for the Speed Defuzzifier.  This constructor initializes all
 * necessary components which do not change between runs.  This list includes
 * the response table and the output range set.
 */
speed_defuzzifier::speed_defuzzifier():
    defuzzifier(&m_rSpeed)
{
    mid_t responses[MAX_OUTPUT_SPEED][MAX_OUTPUT_SPEED] = {
        /*FUZ_SPEED*/
        {STOP, STOP, IDLE, IDLE, SLOW},
        {STOP, IDLE, IDLE, SLOW, SLOW},
        {STOP, IDLE, SLOW, SLOW, FAST},
        {STOP, IDLE, SLOW, FAST, FAST},
        {STOP, IDLE, SLOW, FAST, LUDACRIS_SPEED}
    };

    mid_t ids[MAX_OUTPUT_SPEED] = {
        STOP, IDLE, SLOW, FAST, LUDACRIS_SPEED
        //LUDACRIS_SPEED, FAST, SLOW, IDLE, STOP
    };

    /* Add a dimension of size 5 to the table */
    m_rtTable.add_dimension(MAX_OUTPUT_SPEED);
    m_rtTable.add_dimension(MAX_OUTPUT_SPEED);

    m_rtTable.set_table(sizeof(responses)/sizeof(responses[0][0]), 
					&responses[0][0]);


    int values[MAX_OUTPUT_SPEED] = {
        0, 	
		SPEED_INCR*1.0*MAX_VALUE, 
		SPEED_INCR*3.0*MAX_VALUE, 
		SPEED_INCR*6.0*MAX_VALUE, 
		SPEED_INCR*10.0*MAX_VALUE
    };

    m_rDefuzz.set_values(MAX_INPUT_SPEED,&ids[0], &values[0]);

    set_name("DFRelativeSpeed");

    m_rDefuzz.set_name("FCSpeedDEFUZZ");
    m_rSpeed.set_name("FCOutputSpeed");

    DEBUG(TLDR, "Speed Creation...\n");

}

speed_defuzzifier::~speed_defuzzifier()
{
}

/**
 * This function defines the inference mechanism between the
 * fuzzified inputs and the defuzzified outputs.  This is handled
 * using the response_table class.
 */
int
speed_defuzzifier::inference()
{
    collection* members = collect(IN);
    if (!members) {
        FAIL(DANGER_WILL_ROBINSON, "Null input collection...\n");
        return INVALID;
    }

    member_record* nimgspeed = members->get<member_record>("FCNearImageSpeed");

    if (!nimgspeed) {
        FAIL(DANGER_WILL_ROBINSON, "NULL member record 'FCImageSpeed'\n");
	    return INVALID;
    }

    member_record* fimgspeed = members->get<member_record>("FCFarImageSpeed");

    if (!fimgspeed) {
        FAIL(DANGER_WILL_ROBINSON, "NULL member record 'FCImageSpeed'\n");
	    return INVALID;
    }

	fimgspeed->dump();
	nimgspeed->dump();
	m_rDefuzz.cart_product(m_rtTable, nimgspeed, fimgspeed);

    return VALID;
}

/**
 * This function handles the actual defuzzification by means of
 * the helper functions associated with the member_record class.
 */
int
speed_defuzzifier::defuzzify()
{
    DEBUG(TLDR, "Defuzzify: %s\n", m_rSpeed.name().c_str());

    float sop = m_rDefuzz.defuzz(member_record::SUM_OF_PRODUCTS);
	

    float speed = sop/(float)MAX_VALUE;
	speed *= 0.80;
	m_rSpeed.set_value(speed);
	m_rDefuzz.dump();
    FAIL(TLDR, "Speed Defuzz: %f\n", sop);
    FAIL(TLDR, "Speed: %f\n", m_rSpeed.value());

    collection* coll = collect(OUT);
    if (!coll) {
        FAIL(DANGER_WILL_ROBINSON, "No Output Collection!\n");
        return INVALID;
    }

    int ret = coll->update(&m_rSpeed);
    DEBUG(MAJOR, "Collection Update: %d\n", ret);
    return ret;
}

}
