#include <ant/minput.h>

#define DEBUGGING DEBUG_FILTER
#include <ant/debug.h>

#include <vector>
#include <string>

namespace ant
{

multi_input::multi_input(int required, int requested):
    m_iRequested(requested),m_iRequired(required)
{
}

multi_input::~multi_input()
{
}

int
multi_input::required_inputs() const
{
    return m_iRequired;
}

int
multi_input::requested_inputs() const
{
    return m_iRequested;
}

std::string
multi_input::input_name(int i) const
{
    (void)i;
    return ant::unused;
}

}
