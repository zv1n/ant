#include <ant/global_hazard_collector.h>
#include <ant/hazards_record.h>
#include <ant/gps_record.h>
#include <ant/nav_record.h>

#define DEBUGGING DEBUG_GLOBAL_HAZARDS
#include <ant/debug.h>

#include <vector>
#include <string>

#define wrap(x) \
		if (x) x %= 360; \
		if (x < 0) while(x<0) x+=360; \
		if (x) x %= 360

namespace ant
{

define_factory_class(global_hazard_collector, collector, GLOBAL_HAZARD_COLLECTOR_UCID, collector);

global_hazard_collector::global_hazard_collector():
    collector(NULL,2,0), m_ghInput(NULL), m_gpsInput(NULL)
{
    add_property<filter>(input_name(0), m_ghInput);
    add_property<filter>(input_name(1), m_gpsInput);
    add_property<filter>(input_name(2), m_navInput);

	m_iaDistances.set_name("GlobHazards");
	add_record(&m_iaDistances);
	m_rFrame.set_name("GlobalHazardHUD");
	set_frame(&m_rFrame);
}

global_hazard_collector::~global_hazard_collector()
{
}

std::string
global_hazard_collector::input_name(int i)
{
	switch(i) {
		case 0:
        	return "Hazard Input";
		case 1:
			return "GPS Input";
		case 2:
			return "Nav Input";
	}
    return ant::unused;
}

int
global_hazard_collector::process()
{
	int ret = 0;

	if (!m_ghInput || !m_gpsInput) {
		DEBUG(MINOR, "Required inputs not fulfilled!\n");
		return INVALID;
	}

    gps_record* gps = m_gpsInput->current<gps_record>();
    if (!gps) {
		DEBUG(MINOR, "Required gps record is not a gps record.\n");
    	return INVALID;
	}

	hazards_record* hazards = m_ghInput->current<hazards_record>();
	if (!hazards) {
		DEBUG(MINOR, "Required gps record is not a gps record.\n");
		return INVALID;
	}

	m_hrHazards.update_from(hazards);

	collection* out = collect(OUT);
	if (!out) {
		DEBUG(MINOR, "Output collection not set!\n");
		return INVALID;
	}

    int local[360];
    for (int x=0; x<360; x++)
		local[x] = HAZARD_MAX_DISTANCE;

    m_hrHazards.set_rel_position(gps);
    for (int x=0; x<m_hrHazards.size(); x++) {
        double dist = m_hrHazards.distance(x);
        double radius = m_hrHazards.radius(x);
        double bearing = m_hrHazards.bearing(x);
        double effective_distance = dist-radius;

        if (effective_distance > HAZARD_MAX_DISTANCE) {
            continue;
        }

        if (effective_distance < 0.0) {
            DEBUG(TLDR, "Shit, we are in a hazard zone...\n");
        }

        if (dist == 0.0) {
            continue;
        }

        double toa = radius/dist;
        double radians = atan(toa);
        double degrees = radians*180/M_PI;

        int index = (int)bearing;
        int offset = (int)degrees;

        for (int i=-offset; i<=offset; i++) {
            double doffset = (double)i/offset;
            double asoffset = acos(doffset);
            double depth = sin(asoffset);
            int val = dist - radius*depth;
            int idx = i+index;
			wrap(idx);
            if (local[idx] > val)
                local[idx] = val;
        }
    }

    m_iaDistances.set_values(360, &local[0]);
	ret = out->update(&m_iaDistances);

	render_hazards(m_iaDistances);

    return ret|collector::process();
}

int
global_hazard_collector::render_hazards(integer_array_record& rec) 
{
	int prop = 0;
	get_property("Tap", prop);

	if (!prop)
		return VALID;

    nav_record* nav = m_navInput->current<nav_record>();
    if (!nav) {
		DEBUG(MINOR, "Required nav record is not a gps record.\n");
    	return INVALID;
	}
	
#define WIDTH 480
#define HEIGHT 480
	cv::Mat& frame = m_rFrame.data();

	if (frame.cols == 0) {
		frame = cv::Mat(WIDTH, HEIGHT, CV_8UC3, cv::Scalar(255,255,255));	
	}

	memset(frame.data, 0, WIDTH*HEIGHT*3);

	cv::Scalar lcolor = nav->valid_bearing()
							?cv::Scalar(255,255,255)
							:cv::Scalar(0,0,255);

	cv::Point2i center(WIDTH/2, HEIGHT/2);
    cv::Point2i end(0,0);
    cv::Point2i haz(0,0);
	cv::Point2i start(0,0);

    std::stringstream br;
    br << "Waypoint: ";
    br << nav->waypoint();

    cv::putText(frame, br.str(), cv::Point2i(5,20),
                cv::FONT_HERSHEY_SIMPLEX, 0.5f, CV_RGB(255,255,255), 1);


    start.y = -cos(359.0*M_PI/180.0)*rec[359] * WIDTH / 300; 
    start.x = sin(359.0*M_PI/180.0)*rec[359] * WIDTH / 300;

	for (int i=0; i<360; i++) {
    	haz.y = -cos(((double)i)*M_PI/180.0) * 150 * WIDTH / 300;
    	haz.x = sin(((double)i)*M_PI/180.0) * 150 * WIDTH / 300;

		if (rec[i] < 150 && rec[(i+359)%360] < 150) 
			cv::line(frame, center+start, center+haz, CV_RGB(255,0,0), 2);

		start = haz;
	}

    start.y = -cos(359.0*M_PI/180.0)*rec[359] * WIDTH / 300; 
    start.x = sin(359.0*M_PI/180.0)*rec[359] * WIDTH / 300;

	for (int i=0; i<360; i++) {
    	end.y = -cos(((double)i)*M_PI/180.0) * rec[i] * WIDTH / 300;
    	end.x = sin(((double)i)*M_PI/180.0) * rec[i] * WIDTH / 300;

		cv::Scalar color = lcolor;
		if (rec[i] < 150 || rec[(i+359)%360] < 150) 
			color = CV_RGB(255,0,0);
		cv::line(frame, center+start, center+end, color, 2);

		start = end;
	}

/*    start.y = -cos(nav->true_bearing()*M_PI/180.0)*WIDTH/2;
    start.x = -sin(nav->true_bearing()*M_PI/180.0)*WIDTH/2;
	cv::line(frame, center+start, center, cv::Scalar(0, 0, 255), 2);
*/
    start.y = -cos(nav->rel_bearing()*M_PI/180.0)*WIDTH/2;
    start.x = sin(nav->rel_bearing()*M_PI/180.0)*WIDTH/2;
	cv::line(frame, center+start, center, cv::Scalar(0, 255, 0), 2);

    start.y = -WIDTH/4;
    start.x = 0;
	cv::line(frame, center+start, center, CV_RGB(100,100,255), 2);

	return VALID;
}

}
