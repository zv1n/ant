#include <ant/propertyset.h>
#include <ant/nav_record.h>

#define DEBUGGING DEBUG_NAV_RECORD
#include <ant/debug.h>

#include <math.h>

namespace ant
{

nav_record::nav_record()
{

}

nav_record::~nav_record()
{

}

record*
nav_record::new_collection_record() const
{
    return static_cast<record*>(new nav_record(*this));
}

void
nav_record::dump() const
{
#if DEBUG_DUMP
    printf("Dumping: %s\n", name().c_str());
    printf("        Distance to Destination: %f\n", m_dDistance);
    printf("Relative Bearing to Destination: %d\n", m_iRelBearing);
#endif 
}

int
nav_record::size() const
{
    return 1;
}

std::string
nav_record::type()
{
	return NAV_RECORD_TYPE;
}

int nav_record::update_from(record* rec)
{
    nav_record* nav = record_cast<nav_record>(rec);

    if(!nav) {
        DEBUG(TLDR, "Record type does not match Nav Record!\n");
        return INVALID;
    }

    DEBUG(TLDR, "Distance to Destination: %f\n", m_dDistance);
    DEBUG(TLDR, "Relative Bearing to Destination: %d\n", m_iRelBearing);

    set_distance(nav->distance());
    set_rel_bearing(nav->rel_bearing());

    return record::update_from(rec);
}

double
nav_record::distance() const
{
    return m_dDistance;
}

int
nav_record::rel_bearing() const
{
    return m_iRelBearing;
}

int
nav_record::true_bearing() const
{
	return m_iTrueBearing;
}

bool
nav_record::valid_bearing() const
{
	return m_bValBearing;
}

bool
nav_record::at_destination() const
{
	return m_bFinished;
}

int
nav_record::waypoint() const
{
	return m_iWaypoint;
}

int
nav_record::set_waypoint(int wpt)
{
    m_iWaypoint = wpt;
    return VALID;
}

int
nav_record::set_distance(double dist)
{
    m_dDistance = dist;
    return VALID;
}

int
nav_record::set_rel_bearing(int bear)
{
    m_iRelBearing = bear;
    return VALID;
}

int
nav_record::set_true_bearing(int bear)
{
	m_iTrueBearing = bear;
	return VALID;
}

int
nav_record::set_valid_bearing(bool bear)
{
	m_bValBearing = bear;
	return VALID;
}

int
nav_record::set_at_destination(bool dest)
{
	m_bFinished = dest;
	return VALID;
}

int
nav_record::log(bool force) const
{
    IS_LOG_REQUIRED(force);

    char buff[256];
    int fd = get_fd();
    if (fd < 0)
        return INVALID;

    // Don't print shit if the input is invalid.
    if (isnan(distance())) {
        return VALID;
    }

    int slen = snprintf(buff, sizeof(buff), "%lf@%d\n", distance(),
                        rel_bearing());
    int ret = write(fd, buff, slen);
	if (ret < 0)
		return INVALID;

    DEBUG(TLDR, "Reported Bearing: %d\n", rel_bearing());

    return VALID;
}

}
