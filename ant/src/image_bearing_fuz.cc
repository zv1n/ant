#include <ant/image_bearing_fuz.h>
#include <ant/member_record.h>
#include <ant/nav_record.h>

#define DEBUGGING DEBUG_IMAGE_FUZ
#include <ant/debug.h>
#include <ant/enums.h>

namespace ant
{

#define MID_VALUE (40)

define_factory_class(image_bearing_fuzzifier, fuzzifier, 
					IMAGE_BEARING_FUZZ_UCID, fuzzifier);

image_bearing_fuzzifier::image_bearing_fuzzifier():
    fuzzifier()
{
    set_name("FBearingImage");
    m_rNearImage.set_name("FCImageNearBearing");
    m_rNearFuzz.set_name("FCImageNearData");
    m_rFarImage.set_name("FCImageFarBearing");
    m_rFarFuzz.set_name("FCImageFarData");

	add_record(&m_rFarImage);
	add_record(&m_rNearImage);

    mid_t responses[MAX_INPUT_BEARING] = {
        LEFT_LARGE, LEFT_SMALL, ZERO, RIGHT_SMALL, RIGHT_LARGE
    };

    int	values[MAX_INPUT_BEARING] = {
        0, 20, 40, 60, 80
    };

    m_rNearImage.set_values(MAX_INPUT_BEARING, &responses[0], &values[0]);
    m_rFarImage.set_values(MAX_INPUT_BEARING, &responses[0], &values[0]);
}

image_bearing_fuzzifier::~image_bearing_fuzzifier()
{
}

int
image_bearing_fuzzifier::preprocess()
{
	int ret = 0;
    collection* crisp = collect(IN);
    if (!crisp)
        return INVALID;

    integer_array_record* pArray =
        crisp->get<integer_array_record>("ImageNearBearingRegion");
    if (!pArray) {
        FAIL(TLDR, "Failed to retrieve Near Bearing Region Integer Data.\n");
        return INVALID;
    }

	ret = process_iar(*pArray, m_rNearFuzz);
	if (ret) {
		FAIL(TLDR, "Failed process input Near Image region!\n");
		return INVALID;
	}

    FAIL(DANGER_WILL_ROBINSON, "FBearing (Near): %d\n", m_rNearFuzz.value());

    pArray = crisp->get<integer_array_record>("ImageFarBearingRegion");
    if (!pArray) {
        FAIL(TLDR, "Failed to retrieve Far Bearing Region Integer Data.\n");
        return INVALID;
	}

	ret = process_iar(*pArray, m_rFarFuzz);
	if (ret) {
		FAIL(TLDR, "Failed process input Far Image region!\n");
		return INVALID;
	}

    FAIL(DANGER_WILL_ROBINSON, "FBearing (Far): %d\n", m_rFarFuzz.value());

	return VALID;
}

int
image_bearing_fuzzifier::process_iar(integer_array_record& array, 
									integer_record& fuzz)
{
#define COUNT (5)

    int	val = 0;
    int lengthArray = array.size();

    if (lengthArray != COUNT) {
        FAIL(TLDR, "Invalid integer array received (size: %d).\n", 
				lengthArray);	
        return INVALID;
    }

    int minArray = array[2];
    int minIndex = 2;
    int multiple = 20;
    int	inc = multiple/2;
	int cnt = (COUNT-1)/2;
	int inv = 1;

    /* Find the minimum value in the array */
    for(int i=1; i<=cnt; i++) {
		for (int x=0; x<2; x++) {
			inv *= -1;
        	if(array[i*inv+cnt] < minArray) {
           		minArray = array[i];
           		minIndex = i*inv+cnt;
        	}
		}
    }

    /* If its the center element, accept it. */
    if (cnt == minIndex) {
        fuzz.set_value(minIndex * multiple);
        return VALID;
    }

    int maxIndex = 0;

    /* Calculate the first value */
    minIndex = 0;
    minArray = (array[0] + array[1])/2;

    /* Iterate through each subsequent value to look for the minmum average. */
    for (int i = 1; i<lengthArray-1; i++) {
        int avg = (array[i] + array[i+1])/2;
        if (avg < minArray) {
            minIndex = i;
            minArray = avg;
        }
    }

    /* Now that we know the minimum, assign true min and max index
     * for the pair. */
    if (array[minIndex] > array[minIndex + 1]) {
        maxIndex = minIndex;
        minIndex = minIndex + 1;
    } else
        maxIndex = minIndex + 1;

    /* if the greater valued index is greater than the lesser values index,
     * this will result in a positive, resulting in the adjustment being toward
     * the right.  If its the opposite (greater valued being less then the
     * lesser valued) the sign will be negative, resulting in a shift to the
     * left.
     */
    int sign = maxIndex - minIndex;

    /* Overall equation.  This will take the minimum value of the pairs, and
     * adjust within the bounds of the minimum range according to the ratio
         * of the two sections. */
    val = (int)((float)minIndex * (float)multiple +
                (float)(sign * inc) *
                ((float)array[minIndex]/(float)array[maxIndex]));

    fuzz.set_value(val);
    return VALID;
}

int
image_bearing_fuzzifier::fuzzify()
{
    collection* members = collect(OUT);
    if (!members)
        return INVALID;

    collection* crisp = collect(IN);
    if (!crisp)
        return INVALID;

	nav_record* nav = crisp->get<nav_record>("Navigation");
	if (!nav) {
		FAIL(TLDR, "Failed process input Near Image region!\n");
		return INVALID;
	}

	if (nav->at_destination()) {
		FAIL(DANGER_WILL_ROBINSON, "\n\n\n!!!!!!!!!!! At Destination "
			"!!!!!!!!!!\n\n\n");
		m_rNearImage.map_input(MID_VALUE);
    	members->update(&m_rNearImage);
		m_rFarImage.map_input(MID_VALUE);
    	members->update(&m_rFarImage);
		return VALID;
	}

    DEBUG(TLDR, "Mapping inputs...\n");
    m_rNearImage.map_input(&m_rNearFuzz);
    members->update(&m_rNearImage);

	DEBUG(TLDR, "Fuzzify Update...\n");
    m_rFarImage.map_input(&m_rFarFuzz);
    members->update(&m_rFarImage);

    DEBUG(TLDR, "Fuzzify Complete...\n");
    return VALID;
}

}
