#include <ant/propertyset.h>

#define DEBUGGING DEBUG_PROPERTYSET
#include <ant/debug.h>

#include <string>
#include <map>

namespace ant
{

property_set::property_set()
{
    add_property("Name", named_type::m_sName, RO);
}

struct property*
property_set::get_property_struct(const std::string& name) {
    std::map<std::string, struct property>::iterator it;

    it = m_mspProperties.find(name);

    if (it == m_mspProperties.end()) {
        std::cout << "Unknown property: '" << name << "'" << std::endl;
        return NULL;
    }
    return (&(it->second));
}

int
property_set::enumerate_properties(std::vector<std::string>& names) const
{
    std::map<std::string, struct property>::const_iterator it;
    for (it = m_mspProperties.begin(); it != m_mspProperties.end(); it++) {
        names.push_back(it->first);
    }

    return VALID;
}

int
property_set::set_preset(const std::string& name, void* ths, propset_fn pre)
{
    struct property* prop = get_property_struct(name);

    if (!prop)
        return INVALID;

    prop->pre = pre;
    prop->this_pre = (unsigned long)ths;

    return VALID;
}

int
property_set::set_postset(const std::string& name, void* ths, propset_fn post)
{
    struct property* prop = get_property_struct(name);
    if (!prop)
        return INVALID;

    prop->post = post;
    prop->this_post = (unsigned long)ths;

    return VALID;
}

int
property_set::call_pre(struct property* prop)
{

    if (!prop)
        return INVALID;

    propset_fn func = prop->pre;
    property_set* ps = reinterpret_cast<property_set*>(prop->this_pre);

    if (!func || !ps) {
        return VALID;
    }

    return (ps->*func)(prop->name);
}

int
property_set::call_post(struct property* prop)
{

    if (!prop)
        return INVALID;

    propset_fn func = prop->post;
    property_set* ps = reinterpret_cast<property_set*>(prop->this_post);

    if (!func || !ps) {
        return VALID;
    }

    return (ps->*func)(prop->name);
}

int
property_set::get_property_type(const std::string& name, unsigned int& type)
const
{
    std::map<std::string, struct property>::const_iterator it;
    it = m_mspProperties.find(name);

    if (it == m_mspProperties.end())
        return INVALID;

    type = it->second.type;

    return VALID;
}

int
property_set::get_property_ucid(const std::string& name, ucid_t& ucid)
const
{
    std::map<std::string, struct property>::const_iterator it;
    it = m_mspProperties.find(name);

    if (it == m_mspProperties.end())
        return INVALID;

    ucid = it->second.ucid;

    return VALID;
}

#define define_add_property(ctype, etype)\
template <> int \
property_set::add_property<ctype>(const std::string& name, ctype& var, int type) \
{\
	struct property prop = {\
		name,\
		(unsigned long)&var,\
		etype,\
		STD|type,\
		0,NULL,0,NULL\
	};\
	std::map<std::string, struct property>::iterator it;\
	it = m_mspProperties.find(name);\
	if (it != m_mspProperties.end())\
		return INVALID;\
	m_mspProperties[name] = prop;\
	return VALID;\
}

define_add_property(int, INT32);
define_add_property(float, FLOAT);
define_add_property(double, DOUBLE);
define_add_property(std::string, STRING);
define_add_property(unsigned long, ULONG);

#define define_get_property(ctype, etype) \
template <> int \
property_set::get_property<ctype>(const std::string& name, ctype& var)\
{\
	struct property* prop = get_property_struct(name);\
	if (!prop || !(prop->type&STD) || prop->ucid != etype) {\
		return INVALID;\
	}\
	ctype* addr = (ctype*)prop->offset;\
	var = *addr;\
	return VALID;\
}

define_get_property(int, INT32);
define_get_property(float, FLOAT);
define_get_property(double, DOUBLE);
define_get_property(std::string, STRING);
define_get_property(unsigned long, ULONG);

#define define_set_property_const(ctype, etype) \
template <> int property_set:: \
set_property_const<ctype>(const std::string& name, ctype var) \
{\
	struct property* prop = get_property_struct(name);\
	if (!prop || prop->ucid != etype || !(prop->type&STD))\
		return INVALID;\
	if (!(prop->type&WO))\
		return INVALID;\
	if (call_pre(prop))\
		return INVALID;\
	ctype* addr = (ctype*)prop->offset;\
	*addr = var;\
	if (call_post(prop))\
		return INVALID;\
	return VALID;\
}

define_set_property_const(int, INT32);
define_set_property_const(float, FLOAT);
define_set_property_const(double, DOUBLE);
define_set_property_const(std::string, STRING);
define_set_property_const(unsigned long, ULONG);

#define define_set_property(ctype, etype)\
template <> int property_set::\
set_property<ctype>(const std::string& name, ctype& var)\
{\
	struct property* prop = get_property_struct(name);\
	if (!prop || prop->ucid != etype || !(prop->type&STD))\
		return INVALID;\
	if (!(prop->type&WO))\
		return INVALID;\
	if (call_pre(prop))\
		return INVALID;\
	ctype* addr = (ctype*)prop->offset;\
	*addr = var;\
	if (call_post(prop))\
		return INVALID;\
	return VALID;\
}

define_set_property(int, INT32);
define_set_property(float, FLOAT);
define_set_property(double, DOUBLE);
define_set_property(std::string, STRING);
define_set_property(unsigned long, ULONG);


#define define_is_type(ctype, etype)\
template <> int \
property_set::is_type<ctype>(ucid_t ucid)\
{\
	if (ucid != etype)\
		return INVALID;\
	return VALID;\
}\
template <> int \
property_set::is_type<ctype>(const std::string& name)\
{\
	std::map<std::string, struct property>::iterator it;\
	it = m_mspProperties.find(name);\
	if (it == m_mspProperties.end())\
		return INVALID;\
	if (it->second.ucid != etype)\
		return INVALID;\
	return VALID;\
}

define_is_type(int, INT32);
define_is_type(float, FLOAT);
define_is_type(double, DOUBLE);
define_is_type(std::string, STRING);
define_is_type(unsigned long, ULONG);

#define define_find_properties(ctype, etype)\
template <> int property_set::\
find_properties<ctype>(std::vector<std::string>& names, int type) const \
{\
    std::map<std::string, struct property>::const_iterator it;\
    for (it = m_mspProperties.begin(); it != m_mspProperties.end(); it++) {\
        if (((it->second.type&(STD|type)) == (unsigned)(STD|type))\
	&& (it->second.ucid == etype))\
            names.push_back(it->first);\
    }\
    return VALID;\
}

define_find_properties(int, INT32);
define_find_properties(float, FLOAT);
define_find_properties(double, DOUBLE);
define_find_properties(std::string, STRING);
define_find_properties(unsigned long, ULONG);

}

