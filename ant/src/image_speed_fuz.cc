#include <ant/image_speed_fuz.h>
#include <ant/member_record.h>
#include <ant/nav_record.h>

#define DEBUGGING DEBUG_FILTER
#include <ant/debug.h>
#include <ant/enums.h>

namespace ant
{

#define HIGH_RISK (255)

define_factory_class(image_speed_fuzzifier, fuzzifier, IMAGE_SPEED_FUZZ_UCID,
                     fuzzifier);

image_speed_fuzzifier::image_speed_fuzzifier():
    fuzzifier()
{
    set_name("FImageSpeed");

    m_rNearImage.set_name("FCNearImageSpeed");
    m_rNearFuzz.set_name("FCNearImageSpeedData");

    m_rFarImage.set_name("FCFarImageSpeed");
    m_rFarFuzz.set_name("FCFarImageSpeedData");

	add_record(&m_rNearImage);
	add_record(&m_rFarImage);

	/* Defuzz is going to be IDLE -> LUDICRAS_SPEED */
    mid_t responses[MAX_INPUT_SPEED] = {
		NO_RISK, LOW_RISK, MED_RISK, HIGH_RISK, EMERGENT_RISK 
		//EMERGENT_RISK, HIGH_RISK, MED_RISK, LOW_RISK, NO_RISK
	};

    int	values[MAX_INPUT_SPEED] = {0, 6, 12, 18, 24};

    m_rNearImage.set_values(MAX_INPUT_SPEED, &responses[0], &values[0]);
    m_rFarImage.set_values(MAX_INPUT_SPEED, &responses[0], &values[0]);
}

image_speed_fuzzifier::~image_speed_fuzzifier()
{
}

int
image_speed_fuzzifier::preprocess()
{
    collection* out = collect(IN);
    if (!out)
        return INVALID;

    integer_array_record* pArray =
        out->get<integer_array_record>("ImageSpeedRegion");
    if (!pArray) {
        FAIL(TLDR, "Failed to retrieve Speed Region Integer Data.\n");
        return INVALID;
    }

    if (pArray->size() != 3) {
        FAIL(TLDR, "Invalid integer array received.\n");
        return INVALID;
    }

    m_rNearFuzz.set_value((*pArray)[2]);
    m_rFarFuzz.set_value((*pArray)[1]);
    return VALID;
}

int
image_speed_fuzzifier::fuzzify()
{
    collection* members = collect(OUT);
    if (!members)
        return INVALID;

    collection* crisp = collect(IN);
    if (!crisp)
        return INVALID;

	nav_record* nav = crisp->get<nav_record>("Navigation");
	if (!nav) {
		FAIL(TLDR, "Failed process input Near Image region!\n");
		return INVALID;
	}

	if (nav->at_destination()) {
		FAIL(DANGER_WILL_ROBINSON, "\n\n\n!!!!!!!!!!! At Destination "
			"!!!!!!!!!!\n\n\n");

		m_rNearImage.map_input(HIGH_RISK);
    	members->update(&m_rNearImage);

		m_rFarImage.map_input(HIGH_RISK);
    	members->update(&m_rFarImage);
		return VALID;
	}

	printf("DKFJ: %d %d\n", m_rNearFuzz.value(), m_rFarFuzz.value());
    m_rNearImage.map_input(&m_rNearFuzz);
    members->update(&m_rNearImage);

    m_rFarImage.map_input(&m_rFarFuzz);
    members->update(&m_rFarImage);

    DEBUG(TLDR, "Fuzzify Complete...\n");
    return VALID;
}

}
