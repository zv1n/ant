#include <ant/erode.h>
#include <ant/frame.h>

#define DEBUGGING DEBUG_ERODE
#include <ant/debug.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

using namespace std;

namespace ant
{

define_factory_class(erode, filter, ERODE_FILTER_UCID, filter);

define_factory_class_description(erode,
                                 "Erosion Filter\n"
                                 "* Input:\n"
                                 "\tImage Input to Erode.");

erode::erode():
    filter(&m_rFrame,1,0),
    m_fInput(NULL)
{
#if DEBUG_ERODE_FILTER
    cout << "Creating erode filter..." << endl;
#endif
    set_name("Erosion Filter");
	m_rFrame.set_name("ErodeFilter");
    add_property<filter>(input_name(0), m_fInput);
    add_property("Description", erode::m_sDescription, RO);
	set_frame(&m_rFrame);
}

erode::~erode()
{
}

int
erode::process()
{
#if DEBUG_ERODE_FILTER
    cout << "processing frame..." << endl;
#endif
    if (!m_fInput)
        return INVALID;

#if DEBUG_ERODE_FILTER
    cout << "getting input frame..." << endl;
#endif
    frame* from = m_fInput->current<frame>();
    if (!from)
        return INVALID;

#if DEBUG_ERODE_FILTER
    cout << "down sampling to erode..." << endl;
#endif

    cv::erode(from->data(), m_rFrame.data(), cv::Mat());

    return filter::process();
}

string
erode::input_name(int i) const
{
    switch(i) {
    case 0:
        return "Image";
    }
    return ant::unused;
}

}
