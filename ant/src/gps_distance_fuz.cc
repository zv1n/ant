#include <ant/gps_distance_fuz.h>
#include <ant/member_record.h>
#include <ant/nav_record.h>

#define DEBUGGING DEBUG_FUZZIFIERS
#include <ant/debug.h>
#include <ant/enums.h>

namespace ant
{

#define MID_VALUE (40)

define_factory_class(gps_distance_fuzzifier, fuzzifier, GPS_DISTANCE_FUZZ_UCID,
                     fuzzifier);

gps_distance_fuzzifier::gps_distance_fuzzifier():
    fuzzifier(&m_rGpsDistance)
{
    set_name("FGpsDistance");
    m_rGpsDistance.set_name("inGpsDistance");
    m_rFuzz.set_name("GpsDistanceData");

    mid_t responses[MAX_INPUT_RISK] = {
        LEFT_LARGE, LEFT_SMALL, ZERO, RIGHT_SMALL, RIGHT_LARGE
    };
    int	values[MAX_INPUT_RISK] = {
        0, 20, 40, 60, 80
    };

    m_rGpsDistance.set_values(MAX_INPUT_RISK, &responses[0], &values[0]);
}

gps_distance_fuzzifier::~gps_distance_fuzzifier()
{
}

int
gps_distance_fuzzifier::preprocess()
{

    return VALID;
}

int
gps_distance_fuzzifier::fuzzify()
{
    collection* members = collect(OUT);
    if (!members)
        return INVALID;

    collection* crisp = collect(IN);
    if (!crisp)
        return INVALID;

	nav_record* nav = crisp->get<nav_record>("Navigation");
	if (!nav) {
		FAIL(MAJOR, "Failed to retreive navigation object!\n");
		return INVALID;
	}

	if (nav->at_destination()) {
    	m_rGpsDistance.map_input(&m_rFuzz);
    	members->update(&m_rGpsDistance);
		return VALID;
	}

    DEBUG(TLDR, "Mapping inputs...\n");
    m_rGpsDistance.map_input(&m_rFuzz);
    DEBUG(TLDR, "Fuzzify Update...\n");
    members->update(&m_rGpsDistance);

    DEBUG(TLDR, "Fuzzify Complete...\n");
    return VALID;
}

}
