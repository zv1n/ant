#include <ant/global_hazard_fuz.h>
#include <ant/global_hazard_collector.h>
#include <ant/member_record.h>
#include <ant/nav_record.h>

#define DEBUGGING DEBUG_GH_FUZ
#include <ant/debug.h>
#include <ant/enums.h>
#define wrap(x) ((x+720)%360)

namespace ant
{

#define MID_VALUE (0)
#define RANGE (180)

define_factory_class(global_hazard_fuz, fuzzifier, GLOBAL_HAZARD_FUZZ_UCID,
                     fuzzifier);

global_hazard_fuz::global_hazard_fuz():
    fuzzifier(&m_rHazards)
{
    set_name("fuzHazard");
    m_rHazards.set_name("FCGlobHazard");

    mid_t responses[MAX_INPUT_BEARING] = {
        LEFT_LARGE, LEFT_SMALL, ZERO, RIGHT_SMALL, RIGHT_LARGE
    };

    int	values[MAX_INPUT_BEARING] = {
        -60, -20, 0, 20, 60
    };

    m_rHazards.set_values(MAX_INPUT_BEARING, &responses[0], &values[0]);
}

global_hazard_fuz::~global_hazard_fuz()
{
}

int
global_hazard_fuz::preprocess()
{
    collection* out = collect(IN);
    if (!out) {
		FAIL(DANGER_WILL_ROBINSON, "Failed to retrieve nav record!\n");
        return INVALID;
	}

	nav_record* nav = out->get<nav_record>("Navigation");
	if (!nav) {
		FAIL(DANGER_WILL_ROBINSON, "Failed to retrieve navigation record!\n");
		return INVALID;
	}

	if (!nav->valid_bearing()) {
		DEBUG(MAJOR, "Invalid Input Bearing!\n");
		m_rHazards.map_input(MID_VALUE);	
		return VALID;
	}

	integer_array_record* iap = out->get<integer_array_record>("GlobHazards");
	if (!iap) {
		FAIL(DANGER_WILL_ROBINSON, "Failed to retrieve global hazards!\n");
		return INVALID;
	}

	integer_array_record& ia = *iap;

	int cnt = ia.size();
	if (cnt != 360) {
		FAIL(DANGER_WILL_ROBINSON, "Failed to retrieve wrong size: %d!\n", 
			ia.size());
		return INVALID;
	}

	int bearing = wrap(nav->rel_bearing());
	int found_idx = wrap(safe_route(bearing, ia));

	if (found_idx < 0) {
    	m_rHazards.map_input(MID_VALUE);
	} else {

		printf("Found Opening: %d\n", found_idx);
		printf("Bear Opening: %d\n", bearing);

		if (found_idx > 180) 
			found_idx -= 360;

		if (bearing > 180) 
			bearing -= 360;

		printf("Found Opening: %d\n", found_idx);
		printf("Bear Opening: %d\n", bearing+found_idx);
		m_rHazards.map_input(bearing+found_idx);
	}

    return VALID;
}

int
global_hazard_fuz::safe_route(int bearing, integer_array_record& ia)
{
	int inv = 1;
	int idx = wrap(bearing);

	if (ia[wrap(idx-1)] == HAZARD_MAX_DISTANCE &&
		ia[wrap(idx)] == HAZARD_MAX_DISTANCE &&
		ia[wrap(idx+1)] == HAZARD_MAX_DISTANCE) {
		DEBUG(DANGER_WILL_ROBINSON, "GH Index (STRAIGHT!): %d\n", idx);
		return 0;
	}

	for (int x=0; x<RANGE; x++) {
		for (int y=0; y<2; y++) {
			idx = wrap(x*inv+bearing);
	
			if (ia[idx] == HAZARD_MAX_DISTANCE) {
				for (int f=0; f<3; f++)
					if (ia[idx+f*inv] == HAZARD_MAX_DISTANCE && f == 2) {
						DEBUG(DANGER_WILL_ROBINSON, "GH Index: %d\n", idx+inv);
						return wrap(x*inv + inv);
					}
			}

			inv *= -1;
		}
	}

	return -1;
}

int
global_hazard_fuz::fuzzify()
{
    collection* members = collect(OUT);
    if (!members)
        return INVALID;

    collection* crisp = collect(IN);
    if (!crisp)
        return INVALID;

    nav_record* nav = crisp->get<nav_record>("Navigation");
    if (!nav) {
        FAIL(MAJOR, "Failed to retreive navigation object!\n");
        return INVALID;
    }

    if (nav->at_destination()) {
		FAIL(DANGER_WILL_ROBINSON, "\n\n\n!!!!!!!!!!! At Destination "
			"!!!!!!!!!!\n\n\n");
       	m_rHazards.map_input(MID_VALUE);
        members->update(&m_rHazards);
        return VALID;
    }

    DEBUG(TLDR, "Fuzzify Update...\n");
    members->update(&m_rHazards);

    DEBUG(TLDR, "Fuzzify Complete...\n");
    return VALID;
}

}
