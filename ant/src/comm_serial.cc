#include <ant/comm_serial.h>
#include <errno.h>
#define DEBUGGING DEBUG_COMM_SERIAL
#include <ant/debug.h>

namespace ant
{

comm_serial::comm_serial()
{
    fd = 0;
    st = DISCONNECTED;
}

comm_serial::~comm_serial()
{
    st = DISCONNECTED;
}

int comm_serial::init(std::string file, int baud, int parity, int stop)
{
	(void)parity; (void)stop;
    filename = file;
    fd = open(file.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);

    if(fd < 0) {
        FAIL(MAJOR, "comm_serial::init(): invalid file descriptor %d \n", fd);
        st = ERROR;
        return 0;
    }

    /* Get the current options for the port... */

    //	fcntl(fd, F_SETFL, F_NDELAY);
    tcgetattr(fd, &options);

    /* Set the baud rates to 19200... */

    cfsetispeed(&options, baud);
    cfsetospeed(&options, baud);

    /* Enable the receiver and set local mode...*/

    options.c_cflag |= (CLOCAL | CREAD);

    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;

    /* Set the new options for the port... */
    tcsetattr(fd, TCSANOW, &options);
    st = CONNECTED;

    return fd;
}

void comm_serial::close()
{
    ::close(fd);
    st = DISCONNECTED;
}

int comm_serial::send(uint8_t* buf, uint16_t size)
{
	 if(st == CONNECTED) 
    {
	 	int n = write(fd, buf, size);
    	if(n == -1) {
        FAIL(MAJOR, "write() of %d byte(s) failed\n", size);
        DEBUG(MAJOR, "comm_serial::recv() %s\n", strerror(errno));
		  close();
        st = ERROR;
    	}
    	return n;
	 }

	else
	{
		DEBUG(TLDR, "comm_serial: no data was sent\n");
		return 0;
	}
}

int comm_serial::recv(uint8_t* buf, uint16_t size)
{
	if (st == CONNECTED)
   {
   		int ret = 0;
		fprintf(stderr, "ST == CONNECTED\n");	
		ret = read(fd, buf, size);
		if(ret == -1) 
        	DEBUG(MAJOR, "recv(): %s\n", strerror(errno));
    	return ret;
	}
	
	#if DEBUG
	else
	{
		DEBUG(TLDR, "comm_serial: no data was sent\n");
		return 0;
	}
	#endif
	return 0;
}

}
