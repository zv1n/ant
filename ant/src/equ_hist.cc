#include <ant/equ_hist.h>
#include <ant/frame.h>

#define DEBUGGING DEBUG_EQU_HIST
#include <ant/debug.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

using namespace std;

namespace ant
{

define_factory_class(equhist, filter, EQUHIST_FILTER_UCID, filter);

define_factory_class_description(equhist,
                                 "Histogram Equalization\n"
                                 "* Input:\n"
                                 "\tImage Input to histogram equalize.");

equhist::equhist():
    filter(&m_rFrame,1,0),
    m_fInput(NULL)
{
    cout << "Creating equhist filter..." << endl;
    set_name("Histogram Equalizer");
    add_property<filter>(input_name(0), m_fInput);
    add_property("Description", equhist::m_sDescription, RO);
}

equhist::~equhist()
{
}

int
equhist::process()
{
    cout << "processing frame..." << endl;
    if (!m_fInput)
        return INVALID;

    cout << "getting input frame..." << endl;
    frame* from = m_fInput->current<frame>();
    if (!from)
        return INVALID;

    cout << "down sampling to equhist..." << endl;
    cv::equalizeHist(from->data(), m_rFrame.data());
    cout << (unsigned long) this << endl;
    return filter::process();
}

string
equhist::input_name(int i) const
{
    switch(i) {
    case 0:
        return "Image";
    }
    return ant::unused;
}

}
