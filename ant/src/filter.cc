#include <ant/filter.h>
#include <ant/frame.h>

#define DEBUGGING DEBUG_FILTER
#include <ant/debug.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

using namespace std;

namespace ant
{

/* declared in minput.h */
std::string unused = "Unused";

define_factory(filter, filter);
define_property_set(filter, FILTER_BASE_UCID);

filter::filter(record* rec, int requested, int required):
    multi_input(requested, required),
    m_pProcess(NULL), m_iTap(0), m_rFrame(NULL)
{
	if (rec)
		m_vRecords.push_back(rec);

    add_property("Required Inputs", m_iRequired, RO);
    add_property("Requested Inputs", m_iRequested, RO);
    add_property("Tap", m_iTap);

    set_postset("Tap", this, (propset_fn)&filter::tap_changed);
}

filter::~filter()
{
}

int
filter::process()
{
    if (m_iTap && m_rFrame) {
        if (!m_rFrame->data().cols || !m_rFrame->data().rows) {
            FAIL(TLDR, "%s: zero sized frame.\n", name().c_str());
            return VALID;
        }

        DEBUG(TLDR, "Displaying image on window: %s\n", name().c_str());
        imshow(name(), m_rFrame->data());
    }

    if (m_vRecords.size()) {
        DEBUG(TLDR, "Logging new value of '%s'\n", name().c_str());
		for (unsigned int x=0; x < m_vRecords.size(); x++) 
        	if (m_vRecords[x] && m_vRecords[x]->log() < 0) {
            	DEBUG(TLDR, "Failed to load value of '%s'\n", name().c_str());
			}
    }

    return VALID;
}

unsigned int
filter::count() const
{
	return m_vRecords.size();
}

processor*
filter::get_processor() const
{
    return m_pProcess;
}

int
filter::set_processor(processor* proc)
{
    m_pProcess = proc;
    return proc?VALID:INVALID;
}

int
filter::reset()
{
    return VALID;
}

int
filter::tap(int tap)
{
    m_iTap = tap;
	unsigned int x = 0;

	for (x=0; x<m_vRecords.size(); x++)
    	if (m_vRecords[x] && m_vRecords[x]->ucid() == frame::record_ucid())
			break;

	if (x == m_vRecords.size())
		return INVALID;

    if (m_iTap) {
        DEBUG(TLDR, "Window name: '%s'\n", name().c_str());
        cv::namedWindow(name(), CV_WINDOW_AUTOSIZE|CV_WINDOW_KEEPRATIO);
    } else
        cv::destroyWindow(name());

    return VALID;
}

int
filter::tap_changed(string& std)
{
	(void)std;
    return tap(m_iTap);
}

int
filter::set_record(record* rec, unsigned int x)
{
    if (!rec)
        return INVALID;

	if (x >= m_vRecords.size())
		return INVALID;

    m_vRecords[x] = rec;

    return VALID;
}

int
filter::add_record(record* rec)
{
	if (!rec)
		return INVALID; 

	m_vRecords.push_back(rec);

	return VALID;
}

int 
filter::set_frame(frame* fr)
{
	if (!fr)
		return INVALID;

	m_rFrame = fr;
	return VALID;
}

int
filter::remove_record(record* rec)
{
	if (!rec)
		return INVALID; 

	for (unsigned int x=0; x<m_vRecords.size(); x++)
		if (m_vRecords[x] == rec) {
			m_vRecords.erase(m_vRecords.begin() + x);
			return VALID;
		}

	return INVALID;
}

template <> record*
filter::current<record>(unsigned int x)
{
	if (x < m_vRecords.size())
    	return m_vRecords[x];
	return NULL;
}

}
