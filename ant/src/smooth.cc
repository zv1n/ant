#include <ant/smooth.h>
#include <ant/frame.h>

#define DEBUGGING DEBUG_SMOOTH
#include <ant/debug.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

using namespace std;

namespace ant
{

define_factory_class(smooth, filter, SMOOTH_FILTER_UCID, filter);

define_factory_class_description(smooth,
                                 "Smooth\n"
                                 "* Input:\n"
                                 "\tImage Input to convert to Gray Scale.");

smooth::smooth():
    filter(&m_rFrame,1,0),
    m_fInput(NULL),
    m_eType(ANT_MEDIAN),
    m_iParam1(3), m_iParam2(0),
    m_dParam3(0.0), m_dParam4(0.0)
{
    set_name("Smooth Filter");
	set_frame(&m_rFrame);
    add_property<filter>(input_name(0), m_fInput);
    add_property("Description", smooth::m_sDescription, RO);

    add_property<int>("Type", m_eType);
    add_property<int>("Param1", m_iParam1);
    add_property<int>("Param2", m_iParam2);
    add_property<double>("Param3", m_dParam3);
    add_property<double>("Param4", m_dParam4);
}

smooth::~smooth()
{
}

int
smooth::process()
{
    if (!m_fInput)
        return INVALID;

    frame* from = m_fInput->current<frame>();
    if (!from)
        return INVALID;

    switch (m_eType) {
    case ANT_MEDIAN:
        cv::medianBlur(from->data(), m_rFrame.data(), m_iParam1);
        break;
    case ANT_GAUSSIAN:
        cv::GaussianBlur(from->data(), m_rFrame.data(),
                         cv::Size(m_iParam1, m_iParam2), m_dParam3, m_dParam4);
        break;
    }

    return filter::process();
}

string
smooth::input_name(int i) const
{
    switch(i) {
    case 0:
        return "Image";
    }
    return ant::unused;
}

}
