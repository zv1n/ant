#include <ant/gps_sim.h>

#define DEBUGGING DEBUG_GPS_SIM
#include <ant/debug.h>

#include <sys/stat.h>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

namespace ant
{
define_factory_class(gps_sim, filter, GPS_SIM_UCID, input);

gps_sim::gps_sim():
    filter(&m_rGps,0,0), m_bIsNmea(true)
{
    configure_properties();
    m_stTimer.set_interval(1000);
    nmea_parser_init(&m_npParser);
    m_rGps.set_name("GPSLocation");
}


gps_sim::~gps_sim()
{
    if (!is_open())
        m_fFile.close();
    nmea_parser_destroy(&m_npParser);
}

int
gps_sim::process()
{
    char buffer[4096] = {0};
    char * line = buffer;
    int line_len = 0;
    int skipped = m_stTimer.update();

	if (m_bIsNmea) {

    	nmeaINFO info;
    	nmea_zero_INFO(&info);

    	for (int x=0; x<skipped; x++) {
    	    if (m_fFile.eof()) {
   	        	DEBUG(MAJOR, "End of GPS Dump.\n");
   	        	return VALID;
        	}

        	m_fFile.getline(line, 4096);

        	line_len = strlen(line);
        	line[line_len] = '\n';
        	line_len++;

        	/* strip the timestamp from the line... */
        	int colcnt = 0;
        	for (int y=0; y<line_len; y++) {
        	    if (line[y] == ':')
        	        colcnt++;
        	    if (colcnt == 3) {
        	        line += ++y;
        	        line_len -= y;
        	        break;
        	    }
        	}
	
	        if (line[0] != '$' || line[3] != 'R') {
	            x--;
	            continue;
	        }
	
	        nmea_parse(&m_npParser, line, line_len, &info);
	        m_rGps.set_longitude(to_degrees(info.lon));
	        m_rGps.set_latitude(to_degrees(info.lat));
	        m_rGps.set_altitude(to_degrees(info.elv));
	        m_rGps.set_reported_bearing(info.direction);
	    }
	} else {
    	if (m_fFile.eof()) {
   	        DEBUG(MAJOR, "End of GPS Dump.\n");
   	        return VALID;
        }	

		double lat = nan("nan");
		double lon = nan("nan");
		double alt = nan("nan");
		double bearing = nan("nan");

        m_fFile.getline(line, 4096);

        line_len = strlen(line);

        /* strip the timestamp from the line... */
		for (int colcnt=0; colcnt<4 && line_len; colcnt++) {
			switch (colcnt) {
				case 0:
					lon = strtod(line, NULL);
					break;
				case 1:
					lat = strtod(line, NULL);
					break;
				case 2:
					alt = strtod(line, NULL);
					break;
				case 3:
					bearing = strtod(line, NULL);
					break;
			}
		
        	for (int y=0; y<=line_len; y++) {
				if (y == line_len) {
					line_len -= y;
					break;
				}
        	    if (line[y] == ',') {
					line += ++y;
					line_len -= y;
					break;
				}
        	}
		}
	
	    m_rGps.set_longitude(lon);
	    m_rGps.set_latitude(lat);
	    m_rGps.set_altitude(alt);
	    m_rGps.set_reported_bearing(bearing);
	}	

    DEBUG(TLDR, "Longtiude: %f\n", m_rGps.longitude());
    DEBUG(TLDR, "Latitude: %f\n", m_rGps.latitude());
    DEBUG(TLDR, "Altitude: %f\n", m_rGps.altitude());
    DEBUG(TLDR, "Reported Bearing: %f\n", m_rGps.reported_bearing());

    return filter::process();
}

int
gps_sim::set_file(std::string file)
{
    struct stat buf;
    if (stat(file.c_str(), &buf))
        return INVALID;
    m_sPath = file;
    return open();
}

int
gps_sim::open()
{
	if (m_sPath.find_last_of(".gps") == m_sPath.length()-1)
		m_bIsNmea = false;
	else 
		m_bIsNmea = true;

    m_fFile.open(m_sPath.c_str(), ifstream::in);
    return is_open();
}

int
gps_sim::is_open()
{
    return (m_fFile.is_open())?VALID:INVALID;
}

int
gps_sim::file_updated(std::string& propname)
{
	(void)propname;
    return open();
}

void
gps_sim::configure_properties()
{
    add_property("Path", m_sPath);
    set_postset("Path", this, (propset_fn)&gps_sim::file_updated);
}

float
gps_sim::to_degrees(float ft)
{
    int degrees = (int)ft;
    degrees /= 100;
    ft /= 100.0f;
    ft -= (float)degrees;
    ft /= 0.60f;
    return (float)degrees + ft;
}

}
