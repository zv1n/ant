#include <ant/filter.h>
#include <ant/collection.h>

#define DEBUGGING DEBUG_COLLECTION
#include <ant/debug.h>

#include <stddef.h>
#include <sys/types.h>
#include <errno.h>

#include <algorithm>

using namespace std;

namespace ant
{

/***************************************************************
**                        ant::record                         **
***************************************************************/

record::record(filter* el):m_eParent(el),m_iFd(0) {}

record::record(const record& rec):named_type(rec),
    m_eParent(rec.m_eParent), m_iFd(0) {}

record::~record()
{
    if (record::s_logManager)
        record::s_logManager->remove_record(this);

    if (m_iFd > 0)
        close(m_iFd);
}

filter*
record::get_filter()
{
    return m_eParent;
}

void
record::set_name(const std::string& name)
{
    named_type::set_name(name);

    if (record::s_logManager)
        record::s_logManager->register_record(this);
}

int
record::set_filter(filter* el)
{
    m_eParent = el;
    return (m_eParent != NULL);
}

std::string
record::type()
{
    return RECORD_BASE_TYPE;
}

ucid_t
record::record_ucid()
{
    return RECORD_BASE_UCID;
}

ucid_t
record::ucid() const
{
    return RECORD_BASE_UCID;
}

int
record::current() const
{
    return m_iUpdates;
}

int
record::update_from(record* rec)
{
    if (!rec)
        return INVALID;
    ++m_iUpdates;

    int ret = log();
    if (ret < 0)
        FAIL(MINOR, "Failed to log record (%s) to file (err:%d).\n",
             name().c_str(), ret);

    return VALID;
}

int
record::open_log_file() const
{
    std::string file;
    int ret = 0;
    int fd = 0;

    if (!record::s_logManager)
        return NO_LOG_MANAGER;

    ret = record::s_logManager->get_file(name(), file);
    if (ret < 0) {
        FAIL(TLDR, "Failed to get file name!\n");
        return INVALID;
    }

    fd = open(file.c_str(), O_WRONLY|O_CREAT, 0664);
    if (fd < 0)
        FAIL(TLDR, "open(%s): %s\n", file.c_str(), strerror(errno));

    m_iFd = fd;

    return fd;
}

int
record::get_fd(bool* fresh) const
{
    if (m_iFd <= 0) {
        if (fresh)
            *fresh = true;
        return open_log_file();
    }

    return m_iFd;
}

log_manager*	record::s_logManager = NULL;

log_manager*
record::get_log_manager()
{
    return record::s_logManager;
}

void
record::set_log_manager(log_manager* lmanager)
{
    if (lmanager)
        DEBUG(TLDR, "Setting Log Manager: %s\n", lmanager->name().c_str());
    else
        DEBUG(TLDR, "Setting NULL Log Manager.\n");
    record::s_logManager = lmanager;
}


/***************************************************************
**                      ant::collection                       **
***************************************************************/

define_property_class(collection, COLLECTION_BASE_UCID);

collection::collection() {}

collection::~collection()
{
    vector<record*>::iterator it;
    for (it=m_vResults.begin(); it<m_vResults.end(); it++) {
        *it = NULL;
    }
}

int
collection::size() const
{
    return m_vResults.size();
}

int
collection::update(record* res)
{
    if (!res) {
        FAIL(MAJOR, "Record is null!\n");
        return INVALID;
    }
    return update(res, STORE, res->name());
}

int
collection::fetch(record* res)
{
    if (!res) {
        FAIL(MAJOR, "Record is null!\n");
        return INVALID;
    }
    return update(res, FETCH, res->name());
}

int
collection::update(record* res, std::string name)
{
    if (!res) {
        FAIL(MAJOR, "Record is null!\n");
        return INVALID;
    }
    return update(res, STORE, name);
}

int
collection::fetch(record* res, std::string name)
{
    if (!res) {
        FAIL(MAJOR, "Record is null!\n");
        return INVALID;
    }
    return update(res, FETCH, name);
}

int
collection::update(record* res, int io, const std::string& name)
{
    DEBUG(TLDR, "Record name: '%s'\n", name.c_str());

    record* active = at(name);
    if (!active) {
        DEBUG(TLDR, "No record with that name in the collection!\n");
        return INVALID;
    }

    int result = VALID;
    DEBUG(TLDR, "Updating record: '%s'\n", active->name().c_str());

    if (io == STORE) {
        DEBUG(TLDR, "Update from: active from res\n");
        active->update_from(res);
    } else if (io == FETCH) {
        DEBUG(TLDR, "Update from: res from active\n");
        res->update_from(active);
    }

    DEBUG(TLDR, "Finished Updating (%d)\n", result);
    return result;
}

int
collection::add(record* res)
{
    if (!res) {
        FAIL(MAJOR, "add: null record!\n");
        return INVALID; /* don't add a null record */
    }

    if (!m_vResults.empty())
        if (at(res->name())) {
            FAIL(MINOR, "add: duplicate!!!\n");
            return INVALID; /*duplicate*/
        }

    DEBUG(TLDR, "add: Adding '%s'\n", res->name().c_str());

    m_vResults.push_back(res);
    return VALID;
}

int
collection::remove(const record* res)
{
    if (!res)
        return INVALID; /* don't add a null record */

    if (m_vResults.empty())
        return INVALID;

    std::string name = res->name();

    vector<record*>::iterator it;
    for (it=m_vResults.begin(); it<m_vResults.end(); it++) {
        if ((*it)->name() == name)
            break;
    }

    if (it == m_vResults.end())
        return INVALID;

    m_vResults.erase(it);
    delete *it;

    return VALID;
}

int
collection::remove(int idx)
{
    if ((signed)m_vResults.size() <= idx || idx < 0)
        return VALID;

    vector<record*>::iterator it = m_vResults.begin() + idx;

    m_vResults.erase(it);
    return VALID;
}

record*
collection::at(int idx)
{
    if ((signed)m_vResults.size() <= idx || idx < 0)
        return NULL;

    vector<record*>::iterator it = m_vResults.begin() + idx;
    return *it;
}

record*
collection::at(const string& name)
{
    vector<record*>::iterator it;
    for (it=m_vResults.begin(); it<m_vResults.end(); it++) {
        if ((*it)->name() == name)
            return *it;
    }
    return NULL;
}

record*
collection::operator[](int idx)
{
    return at(idx);
}

record*
collection::operator[](const string& name)
{
    return at(name);
}

const record*
collection::at(int idx) const
{
    if ((signed)m_vResults.size() <= idx || idx < 0)
        return NULL;

    vector<record*>::const_iterator it = m_vResults.begin() + idx;
    return *it;
}

const record*
collection::at(const string& name) const
{
    vector<record*>::const_iterator it;
    for (it=m_vResults.begin(); it<m_vResults.end(); it++) {
        if ((*it)->name() == name)
            return *it;
    }
    return NULL;
}

const record*
collection::operator[](int idx) const
{
    return at(idx);
}

const record*
collection::operator[](const string& name) const
{
    return at(name);
}

std::vector<record*>::iterator
collection::begin()
{
    return m_vResults.begin();
}

std::vector<record*>::iterator
collection::end()
{
    return m_vResults.end();
}

template <> record*
collection::get<record>(const std::string& str)
{
    return at(str);
}

template <> const record*
collection::get<record>(const std::string& str) const
{
    return at(str);
}

void
collection::dump() const
{
#if DEBUG_DUMP
    DEBUG(ALL, "Collection: %s\n", name().c_str());
    std::vector<record*>::const_iterator it;
    for (it=m_vResults.begin(); it<m_vResults.end(); it++)
        if (*it)
            (*it)->dump();
#endif
}

}
