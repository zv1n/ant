#include <ant/filter.h>
#include <ant/video_in.h>
#include <ant/frame.h>

#define DEBUGGING DEBUG_VIDEOIN
#include <ant/debug.h>

#include <opencv/cv.h>

#include <vector>
#include <sstream>

using namespace std;

namespace ant
{

define_factory_class(videoin, filter, VIDEOIN_FILTER_UCID, input);

videoin::videoin(int cap):
    filter(&m_rFrame,0,0), m_cvCapture(cap)
{
    if (cap == -1) {
        filter::set_name("videocap?");
        return;
    }

    configure_properties();

    stringstream st;
    st << cap;
    string temp = "videocap" + st.str();
    m_iDevice = cap;

	set_frame(&m_rFrame);

    filter::set_name(temp);
}

videoin::videoin():
    filter(&m_rFrame,0,0),
    m_iDevice(-1),
    m_cvCapture()
{
    filter::set_name("videocap?");
    configure_properties();
}

void
videoin::configure_properties()
{
    add_property("Device", m_iDevice);
    set_postset("Device", this, (propset_fn)&ant::videoin::device_changed);
	m_rFrame.set_name("raw");
}

videoin::~videoin()
{
}

int
videoin::process()
{
    if (!m_cvCapture.isOpened())
        return false;

    m_cvCapture >> m_rFrame.data();

    return filter::process();
}

int
videoin::device_changed(std::string& name)
{
	(void)name;
    return set_device(m_iDevice);
}

std::string
videoin::input_name(int i) const
{
	(void)i;
    return "unused";
}

int
videoin::enumerate_devices(vector<int>& inputs)
{
	(void)inputs;
    return 0;
}

int
videoin::set_device(int dev)
{
    if (m_iDevice >= 0 && m_cvCapture.isOpened())
        m_cvCapture.release();

    m_cvCapture.open(dev);
	cout << "Camera: " << dev << endl;

    if (is_opened()) {
        stringstream st;
        st << dev;
        string temp = "videocap" + st.str();

        filter::set_name(temp);
        return VALID;
    }

    m_iDevice = -1;
    return INVALID;
}

bool
videoin::is_opened()
{
    return m_cvCapture.isOpened();
}


}
