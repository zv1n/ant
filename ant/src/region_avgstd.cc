#include <ant/region_avgstd.h>

#define DEBUGGING DEBUG_REGION_AVGSTD
#include <ant/debug.h>

#include <vector>
#include <string>

#include <opencv/highgui.h>

namespace ant
{

define_factory_class(region_avgstd, collector, REGION_AVGSTD_UCID, collector);

region_avgstd::region_avgstd():
    collector(NULL,1,0)
{
    add_property<filter>(input_name(0), m_fInput);
    set_name("Region Expansion");

    m_rSpeedIntegers.set_name("ImageSpeedRegion");
    m_rNearBearingIntegers.set_name("ImageNearBearingRegion");
    m_rFarBearingIntegers.set_name("ImageFarBearingRegion");

    add_record(&m_rSpeedIntegers);
    add_record(&m_rNearBearingIntegers);
    add_record(&m_rFarBearingIntegers);	
}

region_avgstd::~region_avgstd()
{
}

std::string
region_avgstd::input_name(int i)
{
    if (!i)
        return "Image";
    return ant::unused;
}

int
region_avgstd::process()
{
    if (!m_fInput) {
        FAIL(TLDR, "No input is specified!\n");
        return INVALID;
    }

    frame* speed = m_fInput->current<frame>();
    if (!speed) {
        DEBUG(TLDR, "Input frame invalid!\n");
        return INVALID;
    }

    frame* bearing = m_fInput->current<frame>(1);
    if (!bearing) {
        DEBUG(TLDR, "Input frame invalid!\n");
        return INVALID;
	}
   
    int speedArray[3];
    int s_frameWidth;
    int s_frameHeight;
    int s_fuzzyX = 0;
	(void)s_frameWidth;
	(void)s_frameHeight;

    cv::Mat& bearingThresh = bearing->data();

    //Build Bearing Array
    int segmentWidth = bearingThresh.cols;
    int segmentHeight = bearingThresh.rows / 3;

    cv::Mat near(bearingThresh, 
			cv::Rect(0, segmentHeight, segmentWidth, segmentHeight));
    cv::Mat far(bearingThresh,	
			cv::Rect(0, segmentHeight*2, segmentWidth, segmentHeight));	
    
    splitCols(near, m_rNearBearingIntegers);
    splitCols(far, m_rFarBearingIntegers);

    cv::Mat& speedThresh = speed->data();
 
    //Build Speed Array
    int j = 0;
	s_frameHeight = speedThresh.rows/3;
	s_frameWidth = speedThresh.cols-s_fuzzyX;
    cv::Scalar s_mean;
	cv::Scalar s_stdev;

    for( int s_fuzzyY=0; s_fuzzyY<speedThresh.rows; s_fuzzyY+=s_frameHeight) {
		integer_array_record speed;
		int max = 0;
		cv::Mat s_roi(speedThresh, 
				cv::Rect(s_fuzzyX, s_fuzzyY, s_frameWidth, s_frameHeight));
		
		splitCols(s_roi, speed);
		for (int x=0; x<speed.size(); x++)
			if (speed[x] > max)
				max = speed[x];
		speedArray[j] = max;

		j++;
    }

    m_rSpeedIntegers.set_values(3, speedArray);
    for (int i=0; i<=2; i++) {
    	DEBUG(MAJOR, "Speed: %d \n",speedArray[i]);
    }

    collection* out = collect(OUT);
    if (!out) {
        FAIL(TLDR, "No output collection set!\n");
        return INVALID;
    }

    for (int i=0; i<=2; i++) {
    	FAIL(MAJOR, "Speed: %d \n", speedArray[i]);
    }


    for (int i=0; i<m_rNearBearingIntegers.size(); i++) {
    	FAIL(MAJOR, "Bearing (Near): %d \n", m_rNearBearingIntegers.value(i));
	}

    for (int i=0; i<m_rFarBearingIntegers.size(); i++) {
    	FAIL(MAJOR, "Bearing (Far): %d \n", m_rFarBearingIntegers.value(i));
	}

    int ret = 0;
	ret |= out->update(&m_rSpeedIntegers);
    ret |= out->update(&m_rNearBearingIntegers);
	ret |= out->update(&m_rFarBearingIntegers);
    return ret|filter::process();
}

#define COL_COUNT (5)

int
region_avgstd::splitCols(cv::Mat& region, integer_array_record& record)
{
    int i = 0;
    int width = region.cols / COL_COUNT;
    int height = region.rows;
    int array[COL_COUNT] = {0};

    for(int x = 0; x < region.cols; x += width ) {
        cv::Scalar stdev, mean;
        cv::Mat roi(region, cv::Rect(x, 0, width, height));
		cv::meanStdDev(roi, mean, stdev);
		array[i] = 256 - mean[0];
		i++;
     }	 
	
     record.set_values(COL_COUNT, array);
     return true;
}
}
