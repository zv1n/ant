#include <ant/std_records.h>

#define DEBUGGING DEBUG_STD_RECORDS
#include <ant/debug.h>

#include <ant/enums.h>

namespace ant
{

integer_record::integer_record()
{
}

integer_record::~integer_record()
{
}

void
integer_record::dump() const
{
    DUMP(TLDR, "Dumping: %s\n", name().c_str());
    DUMP(TLDR, "Integer: %d\n", m_iInteger);
}

int
integer_record::size() const
{
    return 1;
}

record*
integer_record::new_collection_record() const
{
    return static_cast<record*>(new integer_record(*this));
}

std::string
integer_record::type()
{
    return INTEGER_RECORD_TYPE;
}

ucid_t
integer_record::record_ucid()
{
    return INTEGER_RECORD_UCID;
}

ucid_t
integer_record::ucid() const
{
    return record_ucid();
}

int
integer_record::update_from(record* res)
{
    integer_record* irs = record_cast<integer_record>(res);
    if (!irs)
        return INVALID;

    set_value(irs->value());
    return record::update_from(res);
}

int
integer_record::value() const
{
    return m_iInteger;
}

void
integer_record::set_value(int val)
{
    m_iInteger = val;
}

int
integer_record::log(bool force) const
{
    IS_LOG_REQUIRED(force);

    char buff[256] = {0};
    int fd = get_fd();
    if (fd < 0)
        return INVALID;

    int slen = snprintf(buff, sizeof(buff), "%d\n", m_iInteger);
    int ret = write(fd, buff, slen);
    if (ret < 0) {
        FAIL(TLDR, "Failed to write out integer record data!\n");
        return INVALID;
    }

    return VALID;
}

/** Float Record **/
float_record::float_record()
{
}

float_record::~float_record()
{
}

int
float_record::log(bool force) const
{
    IS_LOG_REQUIRED(force);

    char buff[256] = {0};
    int fd = get_fd();
    if (fd < 0)
        return INVALID;

    int slen = snprintf(buff, sizeof(buff), "%f\n", m_fFloat);
    int ret = write(fd, buff, slen);
    if (ret < 0) {
        FAIL(TLDR, "Failed to write out float record data!\n");
        return INVALID;
    }

    return VALID;
}

void
float_record::dump() const
{
    DUMP(TLDR, "Dumping: %s\n", name().c_str());
    DUMP(TLDR, "Float: %f\n", m_fFloat);
}

int
float_record::size() const
{
    return 1;
}

record*
float_record::new_collection_record() const
{
    return static_cast<record*>(new float_record(*this));
}

std::string
float_record::type()
{
    return FLOAT_RECORD_TYPE;
}

ucid_t
float_record::record_ucid()
{
    return FLOAT_RECORD_UCID;
}

ucid_t
float_record::ucid() const
{
    return record_ucid();
}

int
float_record::update_from(record* res)
{
    float_record* irs = record_cast<float_record>(res);
    if (!irs)
        return INVALID;

    set_value(irs->value());
    return record::update_from(res);
}


float
float_record::value() const
{
    return m_fFloat;
}

void
float_record::set_value(float val)
{
    m_fFloat = val;
}

/** Integer Array Record  **/
integer_array_record::integer_array_record()
{
}

integer_array_record::~integer_array_record()
{
}

void
integer_array_record::dump() const
{
    DUMP(TLDR, "Dumping: %s(%u)\n", name().c_str(), m_vInteger.size());

    std::vector<int>::const_iterator it;
    for (it=m_vInteger.begin(); it<m_vInteger.end(); it++)
        DUMP(TLDR, "Integer: %d\n", (*it));
}

int
integer_array_record::size() const
{
    return (signed)m_vInteger.size();
}

record*
integer_array_record::new_collection_record() const
{
    return static_cast<record*>(new integer_array_record(*this));
}

std::string
integer_array_record::type()
{
    return INTEGER_ARRAY_RECORD_TYPE;
}

ucid_t
integer_array_record::record_ucid()
{
    return INTEGER_ARRAY_RECORD_UCID;
}

ucid_t
integer_array_record::ucid() const
{
    return record_ucid();
}

void
integer_array_record::clear()
{
    m_vInteger.clear();
}

int
integer_array_record::value(int idx) const
{
    if (idx < 0 || idx > (signed)m_vInteger.size())
        return (0x80000000);
    std::vector<int>::const_iterator it = m_vInteger.begin() + idx;
    return (*it);
}

int
integer_array_record::set_value(int idx, int val)
{
    if (idx < 0 || idx > (signed)m_vInteger.size())
        return INVALID;

    std::vector<int>::iterator it;
    for (it=m_vInteger.begin(); it<m_vInteger.end(); it++)
        (*it) = val;

    return VALID;
}

void
integer_array_record::add_value(int v)
{
    m_vInteger.push_back(v);
}

int
integer_array_record::set_values(int size, int* values)
{
    if (!size || size < 0) {
        FAIL(MAJOR, "Invalid Size: %d\n", size);
        return INVALID;
    }

	m_vInteger.resize(size);
    for (int x=0; x<size; x++)
        m_vInteger[x] = values[x];

    return VALID;
}

int
integer_array_record::update_from(record* res)
{
    integer_array_record* irs = record_cast<integer_array_record>(res);
    if (!irs) {
        FAIL(MAJOR, "Input Record is NOT an Integer Array!\n");
        return INVALID;
    }

	if (this == irs)
    	return record::update_from(res);

    m_vInteger.clear();

    std::vector<int>::const_iterator it;
    for (it=irs->begin(); it<irs->end(); it++) {
        m_vInteger.push_back(*it);
    }

    return record::update_from(res);
}


std::vector<int>::iterator
integer_array_record::begin()
{
    return m_vInteger.begin();
}

std::vector<int>::iterator
integer_array_record::end()
{
    return m_vInteger.end();
}

std::vector<int>::const_iterator
integer_array_record::begin()	const
{
    return m_vInteger.begin();
}

std::vector<int>::const_iterator
integer_array_record::end()	const
{
    return m_vInteger.end();
}

int
integer_array_record::operator[](int idx) const
{
    if (idx < 0 || idx >= (signed)m_vInteger.size())
        return (0x80000000);
    return m_vInteger[idx];
}

int&
integer_array_record::operator[](int idx)
{
    return m_vInteger[idx];
}

int
integer_array_record::log(bool force) const
{
    IS_LOG_REQUIRED(force);

    char buff[256] = {0};
    int fd = get_fd();
    if (fd < 0)
        return INVALID;

    int slen = 0;
    char* buffer = &buff[0];
    std::vector<int>::const_iterator it;
    for(it=m_vInteger.begin(); it<m_vInteger.end(); it++) {
        slen += snprintf(buffer, sizeof(buff)-slen, "%i,", *it);
        buffer = &buff[slen];
    }

    int ret = write(fd, buff, slen);
    if (ret < 0) {
        FAIL(TLDR, "Failed to write out float record data!\n");
        return INVALID;
    }

    return VALID;
}

}
