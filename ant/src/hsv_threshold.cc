#include <ant/hsv_threshold.h>
#include <ant/frame.h>

#define DEBUGGING DEBUG_HSV_THRESH
#include <ant/debug.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>
//#include <cvblobslib/BlobResult.h>

using namespace std;

namespace ant
{

define_factory_class(hsv_threshold, filter, HSV_THRESHOLD_FILTER_UCID, filter);

define_factory_class_description(hsv_threshold,
                                 "HSV Threshold Filter\n"
                                 "* Input:\n"
                                 "\tImage Input to Apply HSV THRESH.");

hsv_threshold::hsv_threshold():
    filter(&m_rFrame,1,0),
    m_fInput(NULL)
{
#if DEBUG_HSV_THRESHOLD
    cout << "Creating hsv_threshold filter..." << endl;
#endif
    set_name("HSV Threshold Filter");
    m_rFrame.set_name("HSVFrame");
	m_rBearingFrame.set_name("HSVBearingFrame");
    add_property<filter>(input_name(0), m_fInput);
    add_property("Description", hsv_threshold::m_sDescription, RO);
    add_property("Red", m_red);
    add_property("Blue", m_blue);
    add_property("Yellow", m_yellow);
    add_property("Green", m_green);
    add_property("Orange", m_orange);
    add_property("BlobMin", m_iBlobMin);
    add_property("BlobMax", m_iBlobMax);

    m_iBlobMin = 5000;
    m_iBlobMax = 99999;
    add_record(&m_rBearingFrame);
	set_frame(&m_rFrame);
}

hsv_threshold::~hsv_threshold()
{
}

int
hsv_threshold::process()
{
    if (!m_fInput)
        return INVALID;

    frame* from = m_fInput->current<frame>();
    if (!from)
        return INVALID;

    cv::Mat& input = from->data();
    cv::Mat& thresh = m_rFrame.data();
    cv::Mat& bearing_thresh = m_rBearingFrame.data();

    if (!input.rows || !input.cols)
        return INVALID;

	cv::Mat dilated;

    thresh.release();
    bearing_thresh.release();

    thresh = cv::Mat();
    bearing_thresh = cv::Mat();

    //Thresholding the image based off red
    if(m_red_mask.rows == 0 || m_red_mask.cols == 0) {
    	m_red_mask = cv::Mat(input.size(), 
				CV_8UC1, cv::Scalar(255,255,255));	
    }
    thresh_range(input, bearing_thresh, m_red_mask, 
				cv::Scalar(150,90,0,0), cv::Scalar(255,255,255,0));
    if(m_orange_mask.rows == 0 || m_orange_mask.cols == 0) {
	    m_orange_mask = cv::Mat(input.size(),
		        CV_8UC1, cv::Scalar(255,255,255));
	}
	thresh_range(input, bearing_thresh, m_orange_mask,
	            cv::Scalar(0,50,170,0), cv::Scalar(10,180,256,0));
	cv::erode(bearing_thresh, bearing_thresh, cv::Mat());
	cv::dilate(bearing_thresh, bearing_thresh, 
			cv::Mat(20,20,CV_8UC1, cv::Scalar(255)));
    if(m_red)
		thresh = bearing_thresh.clone();

    //Thresholding the image based off blue
    if(m_blue) {
    	if(m_blue_mask.rows == 0 || m_blue_mask.cols == 0) {
            m_blue_mask = cv::Mat(input.size(), 
					CV_8UC1, cv::Scalar(31,31,31));
    	}
        thresh_range(input, thresh, m_blue_mask, 
					cv::Scalar(80,95,0,0), cv::Scalar(140,255,255,0));
    }



    //Thresholding the image based off yellow
    if (m_yellow) {
        if(m_yellow_mask.rows == 0 || m_yellow_mask.cols == 0) {
            m_yellow_mask = cv::Mat(input.size(), 
						CV_8UC1, cv::Scalar(15,15,15));
        }
		thresh_range(input, thresh, m_yellow_mask, 
					cv::Scalar(30,100,100), cv::Scalar(40,255,255,0));
    }
    
    //Thresholding the image based off green
    if (m_green) {
        if(m_green_mask.rows == 0 || m_green_mask.cols == 0) {
            m_green_mask = cv::Mat(input.size(), 
					CV_8UC1, cv::Scalar(0,0,0));
        }
        thresh_range(input, thresh, m_green_mask, 
				cv::Scalar(70,20,0,0), cv::Scalar(90,255,255,0));
    }
    cv::bitwise_not(thresh, thresh);
    cv::bitwise_not(bearing_thresh, bearing_thresh);
	
    return filter::process();
}

void
hsv_threshold::thresh_range(cv::Mat& input, cv::Mat& output, cv::Mat& mask,
	cv::Scalar bottom, cv::Scalar top)
{

    cv::Mat thresh;
    cv::inRange(input, bottom, top, thresh);

    cv::bitwise_and(thresh, mask, thresh);

    //Invert the threshed image to be more consistant with other ANT filters
    if(output.rows == 0 && output.cols == 0)
		output = thresh;
	else 
		cv::bitwise_or(output, thresh, output);
}

string
hsv_threshold::input_name(int i) const
{
    switch(i) {
    case 0:
        return "Image";
    }
    return ant::unused;
}

}
