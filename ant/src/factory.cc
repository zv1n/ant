#include <ant/nav_record.h>
#include <ant/command_record.h>

#include <ant/bearing_defuz.h>
#include <ant/collection.h>
#include <ant/collector.h>
#include <ant/command_record.h>
#include <ant/commandset.h>
#include <ant/commandset_rover.h>
#include <ant/comm.h>
#include <ant/comm_serial.h>
#include <ant/controller.h>
#include <ant/dim_redux.h>
#include <ant/dynamic_threshold.h>
#include <ant/equ_hist.h>
#include <ant/erode.h>
#include <ant/factory.h>
#include <ant/filter.h>
#include <ant/frame.h>
#include <ant/fuzzy_control.h>
#include <ant/global_hazard_collector.h>
#include <ant/global_hazard_fuz.h>
#include <ant/global_hazard_input.h>
#include <ant/gps_bearing_fuz.h>
#include <ant/gps_collector.h>
#include <ant/gps_distance_fuz.h>
#include <ant/gps.h>
#include <ant/gps_record.h>
#include <ant/gps_sim.h>
#include <ant/gray.h>
#include <ant/hazards_record.h>
#include <ant/hsv.h>
#include <ant/hsv_threshold.h>
#include <ant/image_bearing_fuz.h>
#include <ant/image_speed_fuz.h>
#include <ant/media.h>
#include <ant/member_record.h>
#include <ant/nav_collector.h>
#include <ant/nav_record.h>
#include <ant/region_avgstd.h>
#include <ant/rover_controller.h>
#include <ant/sim_controller.h>
#include <ant/sim_timer.h>
#include <ant/smooth.h>
#include <ant/speed_defuz.h>
#include <ant/std_records.h>
#include <ant/video_in.h>
#include <ant/video_v4l.h>

namespace ant 
{

void
init() 
{
	/* input! */
	register_factory_class(videoin, filter, input);
	register_factory_class(video_v4l, filter, input);
	register_factory_class(media, filter, input);
	register_factory_class(gps, filter, input);
	register_factory_class(gps_sim, filter, input);
	register_factory_class(global_hazard_input, filter, input);

	/* filters */
	register_factory_class(dimredux, filter, filter);
	register_factory_class(dynamic_threshold, filter, filter);
	register_factory_class(equhist, filter, filter);
	register_factory_class(erode, filter, filter);
	register_factory_class(gray, filter, filter);
	register_factory_class(hsv, filter, filter);
	register_factory_class(hsv_threshold, filter, filter);
	register_factory_class(smooth, filter, filter);

	/* collectors */
	register_factory_class(global_hazard_collector, collector, collector);
	register_factory_class(gps_collector, collector, collector);
	register_factory_class(nav_collector, collector, collector);
	register_factory_class(region_avgstd, collector, collector);

	/* fuzzifiers */
	register_factory_class(gps_bearing_fuzzifier, fuzzifier, fuzzifier);
	register_factory_class(gps_distance_fuzzifier, fuzzifier, fuzzifier);
	register_factory_class(image_bearing_fuzzifier, fuzzifier, fuzzifier);
	register_factory_class(image_speed_fuzzifier, fuzzifier, fuzzifier);
	register_factory_class(global_hazard_fuz, fuzzifier, fuzzifier);

	/* defuzzifiers */
	register_factory_class(bearing_defuzzifier, defuzzifier, defuzzifier);
	register_factory_class(speed_defuzzifier, defuzzifier, defuzzifier);
}

}
