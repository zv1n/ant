#include <ant/global_hazard_input.h>
#include <ant/filter.h>
#include <ant/processor.h>

#define DEBUGGING DEBUG_GLOBAL_HAZARDS
#include <ant/debug.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <vector>

using namespace std;

namespace ant
{

define_factory_class(global_hazard_input, filter, GLOBAL_HAZARD_INPUT_UCID, input);

global_hazard_input::global_hazard_input():
	filter(&m_hrHazards,0,0)
{
	configure_properties();
}

global_hazard_input::global_hazard_input(string pth):
	filter(&m_hrHazards,0,0), m_sPath(pth)
{
	m_sPath = pth;
	open();
	configure_properties();
}

global_hazard_input::~global_hazard_input()
{

}

int global_hazard_input::open()
{

    ifstream infile(m_sPath.c_str(), ifstream::in);
    if(!infile.good()) {
        FAIL(TLDR, "global_input_hazard: invalid file path specified.\n");
        return INVALID;
    }

    string line = "";
    while(getline(infile, line)) {
        hazards_record haz;
        double lat = 0;
        double lon = 0;
        int16_t rad = 0;

        stringstream line_stream(line);
        string cell = "";
        if (line.length() > 0 && line[0] == '#')
            continue;
        for(int i = 0; i < 3; i++) {
            getline(line_stream, cell, ',');
            // note: the last cell must be terminated by a ','
            // or else there will be undefined behavior at the
            // atoi call
            switch(i) {
            case 0:
                lat = atof(cell.c_str());
                break;
            case 1:
                lon = atof(cell.c_str());
                break;
            case 2:
                rad = atof(cell.c_str());
                m_hrHazards.add_hazard(lat,lon,rad);
                break;
            }
        }
    }

	FAIL(MAJOR, "Hazards:\n");
	for (int x=0; x<m_hrHazards.size(); x++) {
		FAIL(MAJOR, "Lat: %lf Lon: %lf Radius: %lf\n", 
			m_hrHazards.latitude(x), m_hrHazards.longitude(x), 
			m_hrHazards.radius(x));
	}
	 return VALID;
}


void
global_hazard_input::configure_properties()
{
	add_property("Path", m_sPath, FILE|RW);
	set_postset("Path", this, (propset_fn)&global_hazard_input::open);
}

hazards_record
global_hazard_input::get_hazards_record()
{
    return m_hrHazards;
}

}
