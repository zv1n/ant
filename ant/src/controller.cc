#include <ant/controller.h>

#define DEBUGGING DEBUG_CONTROLLER
#include <ant/debug.h>

namespace ant
{

controller::controller(record* r,int re, int ri):
    collector(r,re,ri), m_cCommandset(NULL)
{
}

controller::~controller()
{
}

int
controller::process()
{
//    int ret = 0;
    if (m_cCommandset)
        /* ret = */m_cCommandset->sendCommand();
    //return ret|collector::process();
    return collector::process();
}

}
