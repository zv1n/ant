#include <ant/filter.h>
#include <ant/media.h>
#include <ant/processor.h>

#define DEBUGGING DEBUG_MEDIA
#include <ant/debug.h>

#include <opencv/cv.h>

#include <sys/stat.h>
#include <vector>
#include <sstream>

#include <libgen.h>

using namespace std;

namespace ant
{

define_factory_class(media, filter, MEDIA_UCID, input);

media::media(const std::string& path):
    filter(&m_rFrame,0,0), m_sPath(path)
{
    set_file(path);
    configure_properties();
}

media::media():
    filter(&m_rFrame,0,0)
{
    configure_properties();
    DEBUG(TLDR, "Creating Media Object File (none)");
    filter::set_name("Media?");
}

void
media::configure_properties()
{
    add_property("Path", m_sPath, FILE|RW);
    set_postset("Path", this, (propset_fn)&media::open);
	set_frame(&m_rFrame);
}

media::~media()
{
}

int
media::file_updated(std::string& pname)
{
	(void)pname;
    return open();
}

int
media::open()
{
    char* dupe = strdup(m_sPath.c_str());
    std::string base = basename(dupe);
    free(dupe);

    DEBUG(TLDR, "Creating Video-from-File Object: %s\n", m_sPath.c_str());

    if (m_sPath.empty()) {
        filter::set_name("media?");
        return INVALID;
    }

    filter::set_name("media(" + base + ")");

    if (m_vcMedia.open(m_sPath))
        return VALID;

    return INVALID;
}

int
media::process()
{
    if (!m_vcMedia.isOpened())
        return INVALID;

    ant::frame* frame = current<ant::frame>();

    if (get_processor() && get_processor()->state() == RUNNING) {
        DEBUG(TLDR, "%s: loading image.\n", name().c_str());
        m_vcMedia >> frame->data();
        DEBUG(TLDR, "Size: %dx%d\n", frame->data().rows, frame->data().cols);
    }

    return filter::process();
}

int
media::set_file(std::string path)
{
    m_sPath = path;
    open();
    return is_open();
}

int
media::reset()
{
    m_vcMedia.release();
    m_vcMedia.open(m_sPath);
    return is_open();
}

int
media::is_open()
{
    return (m_vcMedia.isOpened())?VALID:INVALID;
}


}
