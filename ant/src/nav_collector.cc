#include <ant/nav_collector.h>
#include <ant/std_records.h>

#define DEBUGGING DEBUG_NAV_COLLECTOR
#include <ant/debug.h>

#include <cmath>
#include <string>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>

using namespace std;

namespace ant
{

define_factory_class(nav_collector, collector, NAV_COLLECTOR_UCID, collector);

nav_collector::nav_collector():
    collector(&m_rNav, 1, 0), m_iCurrent(INVALID)
{
	configure_properties();
    m_rDestination.set_name("Destinantion");
    m_rNav.set_name("Navigation");
}

nav_collector::~nav_collector()
{

}

int nav_collector::open()
{

	ifstream infile(m_sPath.c_str(), ifstream::in);
	if(!infile.good()) {
		FAIL(TLDR, "global_input_hazard: invalid file path specified.\n");
		return INVALID;
	}

	m_vllWaypoints.clear();

	string line = "";
	while(getline(infile, line)) {
		struct ll_waypoint waypoint;

		stringstream line_stream(line);
		string cell = "";

		if (line.length() > 0 && line[0] == '#')
			continue;

		for(int i = 0; i < 2; i++) {
			getline(line_stream, cell, ',');
			// note: the last cell must be terminated by a ','
			// or else there will be undefined behavior at the
			// atoi call
			switch(i) {
			case 0:
				waypoint.latitude = atof(cell.c_str());
				break;
			case 1:
				waypoint.longitude = atof(cell.c_str());
				m_vllWaypoints.push_back(waypoint);
				break;
			}
		}
	}

	DEBUG(MAJOR, "Waypoints:\n");
	for (unsigned int x=0; x<m_vllWaypoints.size(); x++) {
		DEBUG(MAJOR, "Lat: %lf Lon: %lf\n", m_vllWaypoints[x].latitude,
				m_vllWaypoints[x].longitude);
	}

	if ( m_vllWaypoints.size()) {
		m_llWaypoint = m_vllWaypoints[0];
		m_iCurrent = 1;
	}

	return VALID;
}


void
nav_collector::configure_properties()
{
    add_property<filter>(input_name(0), m_fGpsInput);
    add_property<filter>(input_name(1), m_fSpeedInput);
	add_property("Path", m_sPath, FILE|RW);
	set_postset("Path", this, (propset_fn)&nav_collector::open);
}



std::string
nav_collector::input_name(int i)
{
	switch(i) {
	case 0:
        return "Navigation Input";
	case 1:
		return "Speed Input";
	default:
    	return ant::unused;
	}
}

int 
nav_collector::set_next_waypoint() 
{
	m_iCurrent++;
	if (m_iCurrent >= m_vllWaypoints.size())
		return INVALID;

	m_llWaypoint = m_vllWaypoints[m_iCurrent];
	return VALID;
}

int
nav_collector::process()
{
    if(!m_fGpsInput || !m_fSpeedInput) {
        FAIL(TLDR, "Invalid inputs supplied.\n");
		exit(3);
        return INVALID;
    }

    gps_record* nav = m_fGpsInput->current<gps_record>();
    if(!nav) {
        DEBUG(TLDR, "Record not a Navigation!\n");
        return INVALID;
    }

    float_record* speed = m_fSpeedInput->current<float_record>();
    if(!speed) {
        DEBUG(TLDR, "Unable to get speed record!!\n");
        return INVALID;
    }

	m_rNav.set_valid_bearing(speed->value() > 0.0f);
	m_rNav.set_true_bearing(nav->reported_bearing());

	m_rNav.set_at_destination(false);

	if (m_iCurrent == (unsigned)INVALID) {
		FAIL(MAJOR, "Destination\n");
		m_rNav.set_at_destination(true);
	} else {
		calculate_nav(nav);

		while (m_rNav.distance() <= 3 && m_rNav.valid_bearing()) {
			if (set_next_waypoint()) {
				FAIL(MAJOR, "Last waypoint reached!\n");
				m_rNav.set_at_destination(true);
				m_iCurrent = INVALID;
				break;
			} else 
				calculate_nav(nav);
		}
	}

	FAIL(MAJOR, "Relative: %d\n", m_rNav.rel_bearing());

    collection* out = collect(OUT);
    if(!out) {
        DEBUG(TLDR, "No output collection!\n");
        return INVALID;
    }

    DEBUG(TLDR, "Updating collection...\n");
    return out->update(&m_rNav);
}

void
nav_collector::calculate_nav(gps_record* local)
{
    if (isnan(local->latitude())) {
		FAIL(MAJOR, "GPS is a NaN!\n");
		m_rNav.set_valid_bearing(false);
        m_rNav.set_rel_bearing(0.0f);
        m_rNav.set_distance(0.0f);
        return;
    }

    // copy to local variable to make code more readable
    double lat1 = local->latitude()*M_PI/180.0;
    double lat2 = m_llWaypoint.latitude*M_PI/180.0;

    double lon1 = local->longitude()*M_PI/180.0;
    double lon2 = m_llWaypoint.longitude*M_PI/180.0;

    const double R = 6372797.560856;  //radius of the earth in meters
    //uses haversine's formula to calculate distance between two points

    double dlon = lon2 - lon1;
    double dlat = lat2 - lat1;
    double a = sin(dlat/2)*sin(dlat/2)+
               cos(lat1)*cos(lat2)*sin(dlon/2)*sin(dlon/2);
    double c = 2*asin(sqrt(a));
    double d = R * c;
    DEBUG(MAJOR, "Nav Distance: %lf\n", d);
    m_rNav.set_distance(d);

    double y = sin(dlon)*cos(lat2);
    double x = cos(lat1)*sin(lat2) - sin(lat1)*cos(lat2)*cos(dlon);
    double bear = atan2(y,x)*180.0/M_PI;
	DEBUG(MAJOR, "Relative Bearing: %lf\n", bear);

	double true_bearing = local->reported_bearing();
	while (true_bearing < 0.0) 
		true_bearing += 360.0;
	if (true_bearing > 180.0)
		true_bearing -= 360.0;
	
	int rel_bearing = (int)(bear - true_bearing);
	if (rel_bearing > 180)
		rel_bearing -= 360;
    m_rNav.set_rel_bearing(rel_bearing);
	printf("Have Bearing: %lf\n", bear);
	printf("True  Bearing: %lf\n", true_bearing);
	printf("Relative Bearing: %d\n", m_rNav.rel_bearing());
	m_rNav.set_waypoint(m_iCurrent);
}
}


