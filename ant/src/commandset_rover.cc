#include <ant/commandset_rover.h>

#define DEBUGGING DEBUG_COMMANDSET_ROVER
#include <ant/debug.h>

#ifndef VALID
#define VALID (0)
#define INVALID (~0x0L)
#endif

#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#define MAX_DELTA (SHRT_MAX>>1)
namespace ant
{

commandset_rover::commandset_rover(comm* c)
{
    //char send_buf[] = HANDSHAKE_KEY;

	 // attach communication object
    com = c;

    //initiate handshake
    //com->send(send_buf, 2);
    m_rCommand.set_name("RoverCommands");
}

commandset_rover::~commandset_rover()
{
    com->close();
}

void commandset_rover::load_command()
{
    commands[0] = m_rCommand.estop();
    commands[1] = m_rCommand.brake();
    commands[2] = m_rCommand.turn();
    commands[3] = m_rCommand.throttle();
    DEBUG(TLDR, "Loaded command string: '%04x %04x %04x %04x'\n",
          0xffff&commands[0], 0xffff&commands[1], 0xffff&commands[2],
          0xffff&commands[3]);
}

int commandset_rover::setcomm(comm* c)
{
    com = c;
    return INVALID;
}

uint16_t commandset_rover::get_brake() const
{
    return m_rCommand.brake();
}

uint16_t commandset_rover::get_estop() const
{
    return m_rCommand.estop();
}

int16_t commandset_rover::get_throttle() const
{
    return m_rCommand.throttle();
}

int16_t commandset_rover::get_turn() const
{
    return m_rCommand.turn();
}

void commandset_rover::set_brake(int16_t brake)
{
    m_rCommand.set_brake(brake);
}

void commandset_rover::set_brakef(float percent)
{
    uint16_t brake = 0;
    if(percent >= -1.0 && percent <= 1.0) {
		brake = ((int16_t)((SHRT_MAX-1) * percent) + 1);
		m_rCommand.set_brake(brake);
	}
}

void commandset_rover::set_estop(bool estop)
{
    m_rCommand.set_estop(estop);
}

void commandset_rover::set_throttle(int16_t  throttle)
{
    m_rCommand.set_throttle(throttle);
}

void commandset_rover::set_throttlef(float percent)
{
    if(percent >=-1.0  && percent <= 1.0) {
        int16_t requested = (int16_t)(SHRT_MAX * percent);
        int16_t actual = m_rCommand.throttle();
        int32_t delta = (requested - actual);

        // will not let change exceed max delta
        if(abs(delta) > MAX_DELTA) {
            if(delta < 0)
                delta = -MAX_DELTA;
            else
                delta = MAX_DELTA;
        }

        actual += delta;
        m_rCommand.set_throttle(actual);
    }
#if DEBUG
    else
        m_rCommand.set_estop(1);
#endif
}

void commandset_rover::set_turn(int16_t turn)
{
    m_rCommand.set_turn(turn);
}

void commandset_rover::set_turnf(float percent)
{
    if(percent >= -1.0 && percent <= 1.0) {
        int16_t requested = (int16_t)(SHRT_MAX * percent);
        int16_t actual = m_rCommand.turn();
        int32_t delta = (requested - actual);

        // will not let change exceed max delta
        if(abs(delta) > MAX_DELTA) {
            if(delta < 0)
                delta = -MAX_DELTA;
            else
                delta = MAX_DELTA;
        }

        actual += delta;
        m_rCommand.set_turn(actual);
    }
#if DEBUG
    else
        m_rCommand.set_estop(1);
#endif
}

void commandset_rover::set_speed(float speed)
{
#if USE_BRAKES
	float brake = 1.0 - speed;
	if (speed <= 0.0) {
		speed = 0.0;
		if (m_iBrakeCount++ < 4)
			brake = 1.0;
		else
			brake = 0.0;
	} else {
		speed = 1.0;
		m_iBrakeCount = 0;
	}

    set_throttlef(speed);
    set_brakef(brake);
	#else

    set_throttlef(speed);
	#endif
}

void commandset_rover::set_bearing(float bearing)
{
    set_turnf(bearing);
}

float
commandset_rover::speed() const
{
	return (float)m_rCommand.throttle()/(float)SHRT_MAX;
}

float
commandset_rover::bearing() const
{
	return (float)m_rCommand.turn()/(float)SHRT_MAX;
}

float
commandset_rover::brake() const
{
	return (float)m_rCommand.brake()/(float)SHRT_MAX;
}

int commandset_rover::sendCommand()
{
    char* command = (char*)commands;
    char recv_buf[10];
    int err = 0;
    uint8_t reply = 0;
    
	 load_command();
   	DEBUG(MAJOR, "sending start byte.\n"); 
	// send start byte 
	 err = com->send((uint8_t*)":", 1);
    // returns number of bytes written or -1 if error
	if(err == -1)
		return INVALID;

   	DEBUG(MAJOR, "sending command packet.\n");
	// send command packet
    err = com->send((uint8_t*)command, sizeof(commands));
    if(err == -1) 
			return INVALID;

    err = VALID;
	usleep(10000);
   	DEBUG(MAJOR, "recieving response\n");
    err = com->recv(&reply, 1);
	if (err < 0 && errno == EAGAIN) {
		usleep(10);
		DEBUG(MAJOR, "recieving response again\n");
        err = com->recv(&reply, 1);
		if (err < 0 && errno == EAGAIN)
		{
			DEBUG(MAJOR, "recieving response failed");
			return INVALID;
		}
	}
	// while (err >= 0)
	//return number of bytes read or -1 if error

    if (reply == ':') {
        memset(recv_buf, 0, sizeof(recv_buf));
        DEBUG(TLDR, "Operation Allowed\n");
        for(unsigned int i = 0; i < 8; i++) {
				// keep reading until there is not an error
   			DEBUG(MAJOR, "recieving ack\n");
            err = com->recv(&reply, 1);
			if (err < 0 && errno == EAGAIN) {
				usleep(10);
				DEBUG(MAJOR, "recieving ack again\n");
            	err = com->recv(&reply, 1);
				if (err < 0 && errno == EAGAIN)
					break;
			}
            recv_buf[i] = reply;
        }

        for(unsigned int i = 0; i < 8; i++) {
            if(recv_buf[i] != command[i]) {
                FAIL(TLDR, "Serial Communication eror. Commands do"
                     "not match\n");
            }
        }
        DEBUG(TLDR, "Command sent: '%#02x %#02x %#02x %#02x %#02x %#02x"
              " %#02x %#02x'\n", 0xff&command[0], 0xff&command[1],
              0xff&command[2], 0xff&command[3], 0xff&command[4],
              0xff&command[5], 0xff&command[6], 0xff&command[7]);

        DEBUG(TLDR, "Command recieved: '%#02x %#02x %#02x %#02x %#02x %#02x"
              " %#02x %#02x'\n", 0xff&recv_buf[0], 0xff&recv_buf[1],
              0xff&recv_buf[2], 0xff&recv_buf[3], 0xff&recv_buf[4],
              0xff&recv_buf[5], 0xff&recv_buf[6], 0xff&recv_buf[7]);
        return sizeof(command);
    } 

	 else if (reply == '#') 
	 {
        err = com->recv(&reply, 1);
		if (err < 0) {
			usleep(10);
           	err = com->recv(&reply, 1);
			if (err < 0)
				return INVALID;
        }

        DEBUG(TLDR, "Error Received: %d\n", reply);
        return 2;
    } 
	 
	 else
        FAIL(TLDR, "Unknown Command Marker: %x\n", reply);

    return 0;
}

}
