#include <ant/collection.h>
#include <ant/processor.h>
#include <ant/sim_timer.h>

#define DEBUGGING DEBUG_PROCESSOR
#include <ant/debug.h>

#include <stddef.h>
#include <sys/types.h>

using namespace std;

namespace ant
{

processor::processor():
    m_iState(STOPPED), m_iFps(0),
    m_iLastUpdate(-1), m_bProfiling(false),
	m_iTotalTime(0), m_pFuzzyControl(NULL)
{
}

processor::~processor()
{
    unsigned int x=0;
	for (x=0; x<m_vfpInputs.size(); x++)
		delete m_vfpInputs[x];
	for (x=0; x<m_vfpFilters.size(); x++)
		delete m_vfpFilters[x];
	for (x=0; x<m_vcpCollectors.size(); x++)
		delete m_vcpCollectors[x];
	for (x=0; x<m_vcpControllers.size(); x++)
		delete m_vcpControllers[x];
}

void
processor::purge()
{
    m_vfpInputs.clear();
    m_vfpFilters.clear();
    m_vcpCollectors.clear();
    m_vcpControllers.clear();
}

int
processor::process()
{
    int ret = 0;
    m_iState = RUNNING;
	
	m_iTotalTime = 0;

    if (ret |= process_inputs())
        FAIL(MAJOR, "Failed to Process Fuzzy Inputs...\n");
    if (ret |= process_filters())
        FAIL(MAJOR, "Processing Filters...\n");
    if (ret |= process_collectors())
        FAIL(MAJOR, "Processing Collectors...\n");
    if (ret |= process_fuzzy_control())
        FAIL(MAJOR, "Processing Fuzzy Controls...\n");
    if (ret |= process_controllers())
        FAIL(MAJOR, "Processing Controllers...\n");

	if (m_bProfiling)
		printf("Profile(Total): %dms\n", m_iTotalTime);

    track_fps();

    return ret;
}

int
processor::fps()
{
    return m_iFps;
}

int
processor::track_fps()
{
    /* Keep track of process fps */
    if (m_iLastUpdate == -1) {
        m_iLastUpdate = sim_timer::get_msecs();
		return m_iFps;
	}

    int tempTime = sim_timer::get_msecs();
	int delta = (tempTime - m_iLastUpdate);
	if (!delta)
		return 0;

    m_iFps = 1000/delta;
    m_iLastUpdate = tempTime;

    return delta;
}

void
processor::set_state(int state)
{
    m_iState = state;
}

int
processor::state() const
{
    return m_iState;
}

int
processor::reset()
{
    int ret = VALID;
    vector<filter*>::iterator itf;

    for (itf=m_vfpFilters.begin(); itf<m_vfpFilters.end(); itf++)
        ret |= (*itf)->reset();
    for (itf=m_vfpInputs.begin(); itf<m_vfpInputs.end(); itf++)
        ret |= (*itf)->reset();
    return ret;
}

property_set*
processor::get_filter(const std::string& name)
{
    vector<filter*>::iterator it;
    for (it=m_vfpFilters.begin(); it<m_vfpFilters.end(); it++)
        if ((*it)->name() == name)
            return (property_set*)(*it);
    return NULL;
}

property_set*
processor::get_input(const std::string& name)
{
    vector<filter*>::iterator it;
    for (it=m_vfpInputs.begin(); it<m_vfpInputs.end(); it++)
        if ((*it)->name() == name)
            return (property_set*)(*it);
    return NULL;
}

property_set*
processor::get_collector(const std::string& name)
{
    vector<collector*>::iterator it;
    for (it=m_vcpCollectors.begin(); it<m_vcpCollectors.end(); it++)
        if ((*it)->name() == name)
            return (property_set*)(*it);
    return NULL;
}

property_set*
processor::get_controller(const std::string& name)
{
    vector<collector*>::iterator it;
    for (it=m_vcpControllers.begin(); it<m_vcpControllers.end(); it++)
        if ((*it)->name() == name)
            return (property_set*)(*it);
    return NULL;
}

int
processor::process_inputs()
{
    vector<filter*>::iterator it;
    for (it=m_vfpInputs.begin(); it<m_vfpInputs.end(); it++) {
        if (!(*it))
            continue;
		int start = sim_timer::get_msecs();
        if ((*it)->process()) {
            FAIL(DANGER_WILL_ROBINSON, "Failure processing: %s\n",
                 (*it)->name().c_str());
            return INVALID;
        }
		int delta = sim_timer::get_msecs() - start;
		m_iTotalTime += delta;
		if (m_bProfiling)
			printf("Profile(%s): %dms\n", (*it)->name().c_str(), delta);
    }
    return VALID;
}

int
processor::process_filters()
{
    vector<filter*>::iterator it;
    for (it=m_vfpFilters.begin(); it<m_vfpFilters.end(); it++) {
        if (!(*it))
            continue;
		int start = sim_timer::get_msecs();
        if ((*it)->process()) {
            FAIL(DANGER_WILL_ROBINSON, "Failure processing: %s\n",
                 (*it)->name().c_str());
            return INVALID;
        }
		int delta = sim_timer::get_msecs() - start;
		m_iTotalTime += delta;
		if (m_bProfiling)
			printf("Profile(%s): %dms\n", (*it)->name().c_str(), delta);
    }
    return VALID;
}

int
processor::process_collectors()
{
    vector<collector*>::iterator it;
    for (it=m_vcpCollectors.begin(); it<m_vcpCollectors.end(); it++) {
        if (!(*it))
            continue;
		int start = sim_timer::get_msecs();
        if ((*it)->process()) {
            FAIL(DANGER_WILL_ROBINSON, "Failure processing: %s\n",
                 (*it)->name().c_str());
            return INVALID;
        }
		int delta = sim_timer::get_msecs() - start;
		m_iTotalTime += delta;
		if (m_bProfiling)
			printf("Profile(%s): %dms\n", (*it)->name().c_str(), delta);
    }
    return VALID;
}

int
processor::process_controllers()
{
    vector<collector*>::iterator it;
    for (it=m_vcpControllers.begin(); it<m_vcpControllers.end(); it++) {
        if (!(*it))
            continue;
		int start = sim_timer::get_msecs();
        if ((*it)->process()) {
            FAIL(DANGER_WILL_ROBINSON, "Failure processing: %s\n",
                 (*it)->name().c_str());
            return INVALID;
        }
		int delta = sim_timer::get_msecs() - start;
		m_iTotalTime += delta;
		if (m_bProfiling)
			printf("Profile(%s): %dms\n", (*it)->name().c_str(), delta);
    }
    return VALID;
}

int
processor::process_fuzzy_control()
{
    if (!m_pFuzzyControl)
        return INVALID;

	int start = sim_timer::get_msecs();
    int ret = m_pFuzzyControl->process();
	int delta = sim_timer::get_msecs() - start;
	m_iTotalTime += delta;
	if (m_bProfiling)
		printf("Profile(FuzzyController): %dms\n", delta);
	return ret;	
}

int
processor::add_input(filter* in)
{
    m_vfpInputs.push_back(in);
    if (in) {
        in->set_processor(this);
        return VALID;
    }
    return INVALID;
}

int
processor::add_filter(filter* filt)
{
    m_vfpFilters.push_back(filt);
    if (filt) {
        filt->set_processor(this);
        return VALID;
    }
    return INVALID;
}

int
processor::add_collector(collector* coll)
{
    m_vcpCollectors.push_back(coll);
    if (coll) {
        coll->set_processor(this);
        return VALID;
    }
    return INVALID;
}

int
processor::add_controller(collector* coll)
{
    m_vcpControllers.push_back(coll);
    if (coll) {
        coll->set_processor(this);
        return VALID;
    }
    return INVALID;
}

int
processor::set_fuzzy_control(fuzzy_controller* ctrl)
{
    if (!ctrl)
        return INVALID;
    m_pFuzzyControl = ctrl;
    return VALID;
}

void
processor::set_profiling(bool prof)
{
	m_bProfiling = prof;
}

}
