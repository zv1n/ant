#include <ant/propertyset.h>
#include <ant/command_record.h>

#define DEBUGGING DEBUG_CMD_RECORD
#include <ant/debug.h>
#include <sys/types.h>

#include <string.h>
#include <math.h>

namespace ant
{

command_record::command_record()
{

}

command_record::~command_record()
{

}

record*
command_record::new_collection_record() const
{
    return static_cast<record*>(new command_record(*this));
}

void
command_record::dump() const
{
#if DEBUG_DUMP
    DUMP("Dumping: %s\n", name().c_str());
    DUMP("Throttle: %d\n", m_sData.throttle);
    DUMP("Turn: %d\n", m_sData.turn);
    DUMP("Brake: %d\n", m_sData.brake);
    DUMP("Estop: %d\n", m_sData.estop);
#endif
}

int
command_record::size() const
{
    return 1;
}

std::string
command_record::type()
{
    return CMD_RECORD_TYPE_NAME;
}

int command_record::update_from(record* rec)
{
    command_record* cmd = record_cast<command_record>(rec);
    if(!cmd) {
        DEBUG(TLDR, "Record type does not match Nav Record!\n");
        return INVALID;
    }

    DUMP();

    set_throttle(cmd->throttle());
    set_turn(cmd->turn());
    set_brake(cmd->brake());
    set_estop(cmd->estop());

    return record::update_from(rec);
}

void
command_record::set_turn(int16_t turn)
{
    m_sData.turn = turn;
}

void
command_record::set_throttle(int16_t throttle)
{
    m_sData.throttle = throttle;
}

void
command_record::set_brake(int16_t brake)
{
    m_sData.brake = brake;
}

void
command_record::set_estop(uint16_t estop)
{
    m_sData.estop = estop;
}

int16_t
command_record::turn() const
{
    return m_sData.turn;
}

int16_t
command_record::throttle() const
{
    return m_sData.throttle;
}

int16_t
command_record::brake() const
{
    return m_sData.brake;
}

uint16_t
command_record::estop() const
{
    return m_sData.estop;
}

int
command_record::log(bool force) const
{
    IS_LOG_REQUIRED(force);

    char buff[256];
    int fd = get_fd();
    if (fd < 0)
        return INVALID;

    snprintf(buff, sizeof(buff), "%u,%d,%d,%d\n", estop(), brake(), turn(),
             throttle());

    int ret = write(fd, buff, strlen(buff));
	if (ret < 0)
		return INVALID;

    return VALID;
}

}
