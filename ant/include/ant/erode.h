#ifndef __ERODE_H__
#define __ERODE_H__

#include <ant/factory.h>
#include <ant/filter.h>
#include <ant/frame.h>

#include <string>

using std::string;

namespace ant
{

class erode : public filter
{
    declare_factory_class(erode, filter);

public:
    erode();
    virtual 		~erode();
    virtual int	process();
    virtual	string  input_name(int i) const;
protected:
    ant::filter* 	m_fInput;
    frame			m_rFrame;
};

}

#endif
