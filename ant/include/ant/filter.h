#ifndef __filter_h__
#define __filter_h__

#include <ant/atypes.h>
#include <ant/factory.h>
#include <ant/propertyset.h>
#include <ant/minput.h>
#include <ant/collection.h>

#include <opencv/cv.h>

#include <string>
#include <vector>
#include <iostream>

namespace ant
{

class frame;
class processor;

/**
 *	@class filter filter.h "ant/filter.h"
 *  @brief Base class for all input and filter objects within the ANT framework.
 *
 *  Some of the class implementation currently hardwires handling for Cv::Mat
 *  image displays for video input.
 */
class filter : public multi_input, public property_set
{
    declare_property_set();
public:
    filter(record* rec = NULL,
           int requested = DEFAULT_INPUT_CNT,
           int required = DEFAULT_INPUT_CNT);
    virtual		~filter();

    template<class T>
    T*	current(unsigned int x = 0);

    template<class T>
    T*	current(std::string name);

    virtual		int		process();
    virtual		int		reset();

	int			add_record(record* rec);
	int			remove_record(record* rec);
	unsigned int count() const;

    int 		tap(int tap);
    int 		set_frame(frame* frame);

    processor*	get_processor() const;
    int			set_processor(processor*);

protected:
    int			tap_changed(std::string& pname);
    int			set_record(record* rc, unsigned int x = 0);

private:
    processor*	m_pProcess;
    int			m_iTap;
	frame* 		m_rFrame;
    std::vector<record*> 	m_vRecords;
};

declare_factory(filter, filter);

template <> record*
filter::current<record>(unsigned int x);

template <> record*
filter::current<record>(std::string name);


/**
 *  @fn template <class T> T* filter::current()
 *  @brief Return the current record casted to type T.
 *  @return Returns a pointer to the current record if one exists of type T.
 */
template <class T> T*
filter::current(unsigned int x)
{
    if (x > m_vRecords.size())
        return NULL;

	if (!m_vRecords[x])
		return NULL;

    if (T::record_ucid() != m_vRecords[x]->ucid()) 
        return NULL;

    return static_cast<T*>(m_vRecords[x]);
}

/**
 *  @fn template <class T> T* filter::current(std::string name)
 *  @brief Return the current record casted to type T.
 *  @param name The name of the the record to look for.
 *  @return Returns a pointer to the current record if one exists of type T.
 */
template <class T> T*
filter::current(std::string name)
{
	unsigned int x = 0;
	record* rec = NULL;

	for (x=0; x<m_vRecords.size(); x++) {
		if (!m_vRecords[x])
			continue;
		if (m_vRecords[x]->name() == name) {
			rec = m_vRecords[x];
			break;
		}
	}

	if (!rec)
		return NULL;

    if (T::record_ucid() != m_vRecords[x]->ucid()) 
        return NULL;

    return static_cast<T*>(m_vRecords[x]);
}


}

#endif /* __filter_h__ */
