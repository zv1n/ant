#ifndef __STRING_HELPER_H__
#define __STRING_HELPER_H__

#include <string>

inline void
trim(std::string& str, std::string wspace = " \t")
{
    size_t foff = 0, loff = 0;
    foff = str.find_first_not_of(wspace);
    if (foff == std::string::npos) {
        str = "";
        return;
    }

    loff = str.find_last_not_of(wspace);
    if (loff == std::string::npos)
        return;

    str = str.substr(foff, loff-foff+1);
    return;
}

#endif /* __STRING_HELPER_H__ */
