#ifndef __ROVER_CONTROLLER_H__
#define __ROVER_CONTROLLER_H__

#include <ant/controller.h>
#include <ant/std_records.h>
#include <ant/frame.h>

namespace ant
{

class rover_controller : public controller
{
public:
    rover_controller(commandset* cs);
    ~rover_controller();

    int process();
	std::string input_name(int x) const;
	void set_command_speed(float speed);
	void set_command_bearing(float bearing);
	void render_response();

	void draw_speed(cv::Mat& cep);
	void draw_bearing(cv::Mat& cep);
	void draw_brakes(cv::Mat& cep);

private:
	frame		 m_rHud;	
	filter*		 m_fInput;

    float_record m_rBearing;
    float_record m_rSpeed;
	float		m_fForceSpeed;
	float 		m_fForceBearing;
};

}

#endif /* __ROVER_CONTROLLER_H__ */
