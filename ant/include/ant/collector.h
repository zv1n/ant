#ifndef __collector_h__
#define __collector_h__

#include <ant/collection.h>
#include <ant/filter.h>

namespace ant
{

/**
 *  @class collector collector.h "ant/collector.h"
 *  @brief collector base class used to collect data from filters and/or input
 *
 *  The collector class is the base class of all collector elements and is
 *  designed to interface with two collections -- one for input and one for
 *  output.
 */
class collector: public filter
{
    declare_property_set();
public:
    collector(record* rec, int required = 0, int requested = 0);
    virtual 	~collector();

public:
    int	set_input(collection*);
    int	set_output(collection*);

protected:
    /**
     *  @enum collector::MODE
     *  @brief Used to determine which collection to use for an operation.
     *  @var collector::MODE collector::IN
     *  @brief Used to specify the Input Collection.
     *  @var collector::MODE collector::OUT
     *  @brief Used to specify the Output Collection.
     */
    enum MODE {
        IN, OUT
    };

    collection* 	collect(int mode);

protected:
    int	prop_remove_collection(std::string& pname);
    int	prop_set_collection(std::string& pname);
    virtual	int	remove_collection(int io);
    virtual	int	set_collection(int io);

protected:
    collection*	m_inCollection;
    collection*	m_outCollection;
};

/* Declare a collector factory for collector objects. */
declare_factory(collector, collector);

}

#endif /* __collector_h__ */
