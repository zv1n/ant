#ifndef __DIMREDUX_H__
#define __DIMREDUX_H__

#include <ant/factory.h>
#include <ant/filter.h>
#include <ant/frame.h>

#include <string>

using std::string;

namespace ant
{

class dimredux : public filter
{
    declare_factory_class(dimredux, filter);

public:
    dimredux();
    virtual 		~dimredux();
    virtual int	process();
    virtual	string  input_name(int i) const;
protected:
    ant::filter* m_fInput;
    frame		m_rFrame;
};

}

#endif
