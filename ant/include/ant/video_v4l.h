#ifndef __video_v4l_h__
#define __video_v4l_h__

#include <ant/filter.h>
#include <ant/frame.h>

#include <iostream>
#include <vector>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <linux/videodev2.h>

#include <pixfc-sse.h>

struct PixFcSSE;

struct v4l_buffer {
	void *	start;
	size_t	length;
};
	
using namespace std;

namespace ant
{

class video_v4l : public filter
{
    declare_factory_class(video_v4l, filter);

public:
    video_v4l(std::string str);
    video_v4l();
    ~video_v4l();

    virtual int			process();
    virtual std::string	input_name(int i) const;

    bool 				is_opened();

protected:
	int					update_frame();
	int					convert(uint8_t *);
	int					open_device();
	int					init_device();
	int					init_v4l();
	int					cleanup_v4l();

protected:
    void				configure_properties();
    int					device_changed(std::string&);

protected:
    std::string				m_sDevice;

	struct v4l_buffer*		m_v4lBuffers;

	int						m_iWidth;
	int						m_iHeight;
	int						m_iDevice;
	uint8_t*				m_upFrame;
	
	unsigned int			m_iBufferCount;

	PixFcSSE*				m_pfcConversion;
	int						m_iUseSSE;
	int						m_iActive;

    frame					m_rFrame;
};


}

#endif /* __video_h__ */
