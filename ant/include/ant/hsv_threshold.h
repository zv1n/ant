#ifndef __HSV_THRESHOLD_H__
#define __HSV_THRESHOLD_H__

#include <ant/factory.h>
#include <ant/filter.h>
#include <ant/frame.h>

#include <string>

using std::string;

namespace ant
{

/**
 *  @class hsv_threshold hsv_threshold.h "ant/hsv_threshold.h"
 *  @brief hsv_threshold thresholds an HSV filtered input.
 *
 *  The hsv_threshold uses the OpenCV library to apply a threshold that
 *  is based off of the HSV color ranges of Red, Blue, Green and Yellow paths.
 */
class hsv_threshold : public filter
{
    declare_factory_class(hsv_threshold, filter);

public:
    hsv_threshold();
    virtual 		~hsv_threshold();
    virtual int	process();
    virtual	string  input_name(int i) const;

protected:
	void thresh_range(cv::Mat& input, cv::Mat& output, cv::Mat& mask, 
			cv::Scalar bottom, cv::Scalar top);

protected:
    /**
     *  @var m_fFrame
     *  The frame record where the filter video frame is stored.
     */
    frame			m_rFrame;
	frame			m_rBearingFrame;
    /**
     *  @var m_fInput
        The input filter on which the smooth filter operates.
     */
    ant::filter* 	 	m_fInput;

	int				m_iBlobMin;
	int				m_iBlobMax;

    cv::Mat m_red_mask;
    cv::Mat m_blue_mask;
    cv::Mat m_green_mask;
    cv::Mat m_yellow_mask;
    cv::Mat m_orange_mask;
    /**
     *  @var m_red
     *  Flag that enables Red color paths as hazards
     */
    int				m_red;
	/**
	 * @var m_orange
	 * Flag that enable Orange color paths as hazards
	 */
    int             m_orange;
    /**
     *  @var m_blue
     *  Flag that enables Blue color paths as hazards
     */
    int				m_blue;
    /**
     *  @var m_green
     *  Flag that enables Green color paths as hazards
     */
    int				m_green;
    /**
     *  @var m_yellow
     *  Flag that enables Yellow color paths as hazards
     */
    int				m_yellow;
};

}

#endif
