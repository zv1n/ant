#ifndef __COMMANDSET_H__
#define __COMMANDSET_H__
#include <ant/named.h>
#include <string>

namespace ant
{
class comm;

/**
 *  @class commandset commandset.h "ant/commandset.h"
 *  @brief Base class used to define interaction between the ANT framework and a
 *    hardware rover component.
 */
class commandset
{
public:
    commandset();
    ~commandset();

public:
    virtual int	sendCommand() = 0;

protected:
    comm* com;
    std::string command_string;
};

}

#endif /*__COMMANDSET_H__*/

