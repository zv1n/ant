#include <ant/commandset.h>
#include <ant/comm.h>
#include <ant/command_record.h>
#include <ant/std_records.h>
#include <stdint.h>
#include <limits.h>

namespace ant
{

#define HANDSHAKE_KEY 0x0f0f

/**
 *  @class commandset_rover commandset_rover.h "ant/commandset_rover.h"
 *  @brief Commandset implementation specific to the Parallax Quad Rover.
 *
 *  The commandset_rover class covers all functional capabilities of the
 *  Parallax quad rover and handles conversions from direction and speed
 *  to rover specific functions.
 */
class commandset_rover : public commandset
{
public:
    commandset_rover(comm*);
    ~commandset_rover();

    virtual	int sendCommand();
    int setcomm(comm*);
    uint16_t get_brake() const;
    uint16_t get_estop() const;
    int16_t get_throttle() const;
    int16_t get_turn() const;
	float speed() const;
	float bearing() const;
	float brake() const;
    void set_brake(int16_t brake);
    void set_brakef(float percent);
    void set_estop(bool estop);
    void set_throttle(int16_t throttle);
    void set_throttlef(float percent);
    void set_turn(int16_t turn);
    void set_turnf(float percent);
    void set_speed(float speed);
    void set_bearing(float bearing);
    void load_command();
private:
    command_record m_rCommand;
    int16_t commands[4];
	int		m_iBrakeCount;
};

}
