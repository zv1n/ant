#ifndef __ATYPES_H__
#define __ATYPES_H__

/**
 *	@def COLLECTION_BASE_UCID
 *	The unique class id associated with the collection class.
 */
#define COLLECTION_BASE_UCID		(0xFF000)

/**
 *	@def FILTER_BASE_UCID
 *	The unique class id associated with the filter class.
 *	@def VIDEOIN_FILTER_UCID
 *	The unique class id associated with the videoin class.
 *	@def MEDIA_FILTER_UCID
 *	The unique class id associated with the media class.
 *	@def EQUHIST_FILTER_UCID
 *	The unique class id associated with the equhist class.
 *	@def ERODE_FILTER_UCID
 *	The unique class id associated with the erode class.
 *	@def SMOOTH_FILTER_UCID
 *	The unique class id associated with the smooth class.
 *	@def GRAY_FILTER_UCID
 *	The unique class id associated with the gray class.
 *	@def HSV_FILTER_UCID
 *	The unique class id associated with the hsv class.
 *	@def GPS_FILTER_UCID
 *	The unique class id associated with the gps class.
 *	@def GPS_SIM_FILTER_UCID
 *	The unique class id associated with the gps_sim class.
 *	@def HSV_THRESHOLD_FILTER_UCID
 *	The unique class id associated with the hsv_threshold class.
 *	@def DYN_THRESHOLD_FILTER_UCID
 *	The unique class id associated with the dyn_threshold class.
 *	@def DIMREDUX_FILTER_UCID
 *	The unique class id associated with the dimredux class.
 */
#define FILTER_BASE_UCID 			(0x10000)
#define VIDEOIN_FILTER_UCID			(FILTER_BASE_UCID + 0x1)
#define MEDIA_FILTER_UCID 			(FILTER_BASE_UCID + 0x2)
#define EQUHIST_FILTER_UCID			(FILTER_BASE_UCID + 0x3)
#define ERODE_FILTER_UCID			(FILTER_BASE_UCID + 0x4)
#define SMOOTH_FILTER_UCID			(FILTER_BASE_UCID + 0x5)
#define GRAY_FILTER_UCID			(FILTER_BASE_UCID + 0x6)
#define HSV_FILTER_UCID				(FILTER_BASE_UCID + 0x7)
#define GPS_FILTER_UCID 			(FILTER_BASE_UCID + 0x8)
#define GPS_SIM_FILTER_UCID 		(FILTER_BASE_UCID + 0x9)
#define HSV_THRESHOLD_FILTER_UCID	(FILTER_BASE_UCID + 0xA)
#define DYN_THRESHOLD_FILTER_UCID	(FILTER_BASE_UCID + 0xB)
#define DIMREDUX_FILTER_UCID		(FILTER_BASE_UCID + 0xC)
#define VIDEOIN_V4L_FILTER_UCID		(FILTER_BASE_UCID + 0xD)

/**
 *	@def RECORD_BASE_UCID
 *	The unique class id associated with the record base class.
 *	@def FRAME_RECORD_UCID
 *	The unique class id associated with the frame class.
 *	@def GPS_RECORD_UCID
 *	The unique class id associated with the gps_record class.
 *	@def MEMBER_RECORD_UCID
 *	The unique class id associated with the member_record class.
 *	@def INTEGER_RECORD_UCID
 *	The unique class id associated with the integer_record class.
 *	@def FLOAT_RECORD_UCID
 *	The unique class id associated with the float_record class.
 *	@def INTEGER_ARRAY_RECORD_UCID
 *	The unique class id associated with the integer_array_record class.
 *	@def COMMAND_RECORD_UCID
 *	The unique class id associated with the command_record class.
 *	@def NAV_RECORD_UCID
 *	The unique class id associated with the nav_record class.
 *	@def HAZARDS_RECORD_UCID
 *	The unique class id associated with the hazards_record class.
 */
#define RECORD_BASE_UCID 			(0x30000)
#define FRAME_RECORD_UCID 			(RECORD_BASE_UCID + 0x1)
#define GPS_RECORD_UCID 			(RECORD_BASE_UCID + 0x2)
#define MEMBER_RECORD_UCID 			(RECORD_BASE_UCID + 0x3)
#define INTEGER_RECORD_UCID 		(RECORD_BASE_UCID + 0x4)
#define FLOAT_RECORD_UCID 			(RECORD_BASE_UCID + 0x5)
#define INTEGER_ARRAY_RECORD_UCID 	(RECORD_BASE_UCID + 0x6)
#define NAV_RECORD_UCID				(RECORD_BASE_UCID + 0x7)
#define COMMAND_RECORD_UCID			(RECORD_BASE_UCID + 0x8)
#define HAZARDS_RECORD_UCID			(RECORD_BASE_UCID + 0x9)

/**
 *	@def COLLECTOR_BASE_UCID
 *	The unique class id associated with the collector base class.
 *	@def REGAVGSTD_COLLECTOR_UCID
 *	The unique class id associated with the region_avgstd class.
 *	@def GPS_COLLECTOR_UCID
 *	The unique class id associated with the gps_collector class.
 */
#define COLLECTOR_BASE_UCID 		(0x40000)
#define REGAVGSTD_COLLECTOR_UCID	(COLLECTOR_BASE_UCID + 0x1)
#define GPS_COLLECTOR_UCID			(COLLECTOR_BASE_UCID + 0x2)
#define NAV_COLLECTOR_UCID       (COLLECTOR_BASE_UCID + 0x3)
#define GLOBAL_HAZARD_COLLECTOR_UCID			(COLLECTOR_BASE_UCID + 0x4)

/**
 *	@def FUZZY_BASE_UCID
 *	The unique class id associated with the fuzzifier base class.
 *	@def IMAGE_BEARING_UCID
 *	The unique class id associated with the image_fuz class.
 */
#define FUZZIFY_BASE_UCID 			(0x50000)
#define GPS_DISTANCE_FUZZ_UCID 		(FUZZIFY_BASE_UCID + 0x2)
#define GPS_BEARING_FUZZ_UCID 		(FUZZIFY_BASE_UCID + 0x3)
#define GLOBAL_HAZARD_FUZZ_UCID 	(FUZZIFY_BASE_UCID + 0x4)
#define IMAGE_BEARING_FUZZ_UCID 	(FUZZIFY_BASE_UCID + 0x6)
#define IMAGE_SPEED_FUZZ_UCID 		(FUZZIFY_BASE_UCID + 0x7)

/**
 *	@def DEFUZZIFY_BASE_UCID
 *	The unique class id associated with the fuzzifier base class.
 *	@def BEARING_DEFUZZ_UCID
 *	The unique class id associated with the bearing_defuz class.
 *	@def SPEED_DEFUZZ_UCID
 *	The unique class id associated with the speed_defuz class.
 */
#define DEFUZZIFY_BASE_UCID 		(0x60000)
#define BEARING_DEFUZZ_UCID 		(DEFUZZIFY_BASE_UCID + 0x1)
#define SPEED_DEFUZZ_UCID 			(DEFUZZIFY_BASE_UCID + 0x2)

#endif /** __ATYPES_H__ */
