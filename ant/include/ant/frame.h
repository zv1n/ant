#ifndef __FRAME_H__
#define __FRAME_H__

#include <ant/atypes.h>
#include <ant/collection.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <string>

namespace ant
{

/**
 *  @def FRAME_RECORD_TYPE
 *  @brief Defines the human readable type name of the frame record type.
 */
#define FRAME_RECORD_TYPE ("frame")

/**
 *  @class frame frame.h "ant/frame.h"
 *  @brief Manages the collection, transfer, and storage of video frames.
 *
 *  The frame class implements all necessary functions to properly handle
 *  video frames.
 */
class frame : public record
{
public:
    frame();
    virtual		~frame();

    virtual		void		dump() const;
    virtual 	int 		size() const;
    virtual 	ucid_t	 	ucid() const;
    virtual		record*		new_collection_record() const;
    virtual		int			update_from(record* rec);
    virtual		int			log(bool force = false)	const;
    static 		std::string 	type();
    static		ucid_t		record_ucid();

    cv::Mat& data();
protected:
    /**
     *  @var m_rFrame
     *	Stores the cv::Mat* frame that stores a video image.
     */
    cv::Mat*		m_rFrame;

    /**
     *	@var m_cvWriter
     *  The VideoWriter object used to log frames to disk.
     */
    mutable cv::VideoWriter m_cvWriter;
};

}

#endif /* __FRAME_H__ */
