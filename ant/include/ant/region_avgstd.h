#ifndef __REGION_STDAVG_H__
#define __REGION_STDAVG_H__

#include <ant/collector.h>
#include <ant/filter.h>
#include <ant/frame.h>
#include <ant/std_records.h>

namespace ant
{

#define REGION_AVGSTD_UCID	(0x30001)

/**
 *  @class region_avgstd.h "ant/region_avgstd.h
 *  @brief region_avgstd takes a thresholded input and splits it into a 5 element aray.
 *
 *  The region_avgstd collector uses the OpenCV library to break the thresholded input
 *  into the five columns. Each of these columns are then averaged for total blackness.
 *  The resultant five element array is sent to the ANT's fuzzyfier.
 */
class region_avgstd : public collector
{
    declare_factory_class(region_avgstd, collector);
public:
    region_avgstd();
    virtual		~region_avgstd();
    virtual		std::string	input_name(int i);
    virtual	int	process();
    virtual     int     splitCols(cv::Mat& region, integer_array_record& record);
protected:
    /**
     *  @var m_fInput
     *  The input filter that is to be averaged
     */
    ant::filter*			m_fInput;

    /**
     *  @var m_rFrame
     *  The frame where the filter video frame is stored.
     */
    frame					m_rFrame;

    /**
     *  @var m_rSpeedIntegers
     *  The array where the averages of the columns are stored
     */
    integer_array_record	m_rSpeedIntegers;
    
    /**
     *  @var m_rBearingIntegers
     *  The array where the averages of the columns are stored
     */
    integer_array_record	m_rNearBearingIntegers;
    integer_array_record	m_rFarBearingIntegers;
};

}

#endif /*__REGION_STDAVG_H__*/
