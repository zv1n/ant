#ifndef __SIMTIMER_H__
#define __SIMTIMER_H__

#include <ant/propertyset.h>

namespace ant
{

class sim_timer : public property_set
{
public:
    sim_timer(int interval = -1);
    ~sim_timer();

public:
    int	set_interval(int interval);
    int	interval();

    int	update();

    static	unsigned long	get_msecs();

protected:
    int	update_interval(std::string& pname);

protected:
    unsigned long	m_ulLastUpdate;
    int		m_iInterval;

};

}

#endif /* __SIMTIMER_H__ */
