#ifndef __MINPUT_H__
#define __MINPUT_H__

#include <ant/named.h>

#include <vector>
#include <string>

namespace ant
{

/**
 *  @def DEFAULT_INPUT_CNT
 *  @brief Defines the number of default inputs.
 */
#define DEFAULT_INPUT_CNT 1

extern std::string unused;

/**
 *  @class multi_input minput.h "ant/minput.h"
 *  @brief multi_input handles the human-readable names of inputs to a filter.
 *
 *  The multi_input class manages the number of expected inputs.  The counts
 *  are separated between 'required inputs' and 'requested inputs'.
 */
class multi_input
{
public:
    multi_input(int required = DEFAULT_INPUT_CNT,
                int requested = 0);
    virtual		~multi_input();

public:
    int			required_inputs() const;
    int			requested_inputs() const;
    virtual		std::string	input_name(int i) const;

protected:
    int			m_iRequested;
    int			m_iRequired;
};


}

#endif /* __MINPUT_H__ */
