#ifndef __RESPONSETABLE_H__
#define __RESPONSETABLE_H__

#include <ant/named.h>
#include <ant/member_record.h>

#include <stdarg.h>

#include <vector>
#include <string>

namespace ant
{

/**
 */
class response_table : public named_type
{
public:
    response_table();
    ~response_table();

public:
    int	add_dimension(int size);
    int	get_dimension(int x, int &size) const;

    int	set_response(mid_t id, int dim1, ...);
    int	get_response(mid_t& id, int dim1, ...) const;
    mid_t	get_response(int dim1, ...) const;

	int get_response(mid_t& id, const std::vector<int>& dims) const;

    int	set_table(int size, mid_t* mdt);
	unsigned int size() const;

protected:
    int	index_of(int dim1, va_list va) const;

protected:
    std::vector<int>	m_vSizes;
    std::vector<mid_t>	m_vIds;
};

}

#endif /* __RESPONSETABLE_H__ */
