#ifndef _STD_RECORDS_H_
#define _STD_RECORDS_H_

#include <ant/collection.h>

#include <string>

namespace ant
{

#define INTEGER_RECORD_TYPE ("integer")

class integer_record : public record
{
public:
    integer_record();
    virtual         ~integer_record();

    virtual			record*			new_collection_record() const;
    virtual			void			dump() const;
    virtual         int             size() const;
    virtual         ucid_t          ucid() const;
    virtual        	int		        update_from(record*);
    virtual			int				log(bool force = false) const;
    static          std::string     type();
    static          ucid_t          record_ucid();

    int	value() const;
    void set_value(int v);
protected:
    int     m_iInteger;
};

#define FLOAT_RECORD_TYPE ("float")

class float_record : public record
{
public:
    float_record();
    virtual         ~float_record();

    virtual			record*			new_collection_record() const;
    virtual			void			dump() const;
    virtual         int             size() const;
    virtual         ucid_t          ucid() const;
    virtual        	int		        update_from(record*);
    virtual			int				log(bool force = false) const;
    static          std::string     type();
    static          ucid_t          record_ucid();

    float	value() const;
    void 	set_value(float v);
protected:
    float     m_fFloat;
};

#define INTEGER_ARRAY_RECORD_TYPE ("integer array")

class integer_array_record : public record
{
public:
    integer_array_record();
    virtual         ~integer_array_record();

    virtual			record*			new_collection_record() const;
    virtual			void			dump() const;
    virtual         int             size() const;
    virtual         ucid_t          ucid() const;
    virtual        	int		        update_from(record*);
    virtual			int				log(bool force = false) const;
    static          std::string     type();
    static          ucid_t          record_ucid();

    int 	value(int idx) const;
    int 	set_value(int v, int idx);
    int		set_values(int size, int* list);
    void 	add_value(int v);
    void	clear();

    std::vector<int>::iterator			begin();
    std::vector<int>::iterator			end();
    std::vector<int>::const_iterator	begin()	const;
    std::vector<int>::const_iterator	end()	const;

    int		operator[](int idx) const;
    int&	operator[](int idx);
protected:
    std::vector<int>	m_vInteger;
};
}

#endif /* _STD_RECORDS_H_ */
