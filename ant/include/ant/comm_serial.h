#ifndef __comm_serial_h__
#define __comm_serial_h__

#include <ant/comm.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <iostream>

namespace ant
{

enum PARITY {
    NOPARITY, ODD, EVEN
};

class comm_serial : public comm
{
public:
    comm_serial();
    ~comm_serial();

    int init(std::string, int baud=B9600, int stop=1, int parity =NOPARITY);
    void close();

    int  send(uint8_t*, uint16_t);	 //parameters TBD.

    /*
     * Provides a wrapper for the read() call in libc to read data from
     * the device connected specified in init().
     * @param buf buffer to store data that was read in
     * @param size amount of data that will be read in
     * @return number of bytes read or -1 if there was an error
     */
    int  recv(uint8_t*, uint16_t);   //return type TBD.

private:
    int fd;
    struct termios options;
    std::string filename;
};

}

#endif

