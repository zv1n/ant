#ifndef __GPS_COLLECTOR_H__
#define __GPS_COLLECTOR_H__

#include <ant/collector.h>
#include <ant/filter.h>
#include <ant/gps_record.h>

namespace ant
{

/**
 *  @class gps_collector gps_collector.h "ant/gps_collector.h"
 *  @brief gps_collector acts as the collector for GPS communications data.
 *
 *  Ideally, this will collect and keep track of all gps points, with
 *  collector output containing the Current GPS coordinate and the approximate
 *  bearing based on all previous points.
 */
class gps_collector : public collector
{
    declare_factory_class(gps_collector, collector);
public:
    gps_collector();
    virtual		~gps_collector();
    virtual		std::string	input_name(int i);
    virtual	int	process();

protected:
    ant::filter*	m_fInput;
    gps_record	m_rLocation;
};

}

#endif /* __GPS_COLLECTOR_H__ */
