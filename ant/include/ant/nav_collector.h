#ifndef __NAV_COLLECTOR_H__
#define __NAV_COLLECTOR_H__

#include <ant/collector.h>
#include <ant/filter.h>
#include <ant/gps_record.h>
#include <ant/nav_record.h>

namespace ant
{

/**
 * 	@class nav_collector nav_collector.h "ant/nav_collector.h"
 * 	@brief nav_collector acts as the collector of all data needed
 * 			 to make navigation decisions.
 *
 * 	This will collect input from the gps_collector and output a
 * 	nav_record object used to input into the fuzzy control system.
 * 	To create the nav_record, current GPS location and bearing is
 * 	recieved from the gps_collector and destination location is
 * 	recieved through other means. The inputs are processed to
 * 	calculate relative bearing to destination and distance.
 */

class nav_collector : public collector
{
    declare_factory_class(nav_collector, collector);
public:
    nav_collector();

    virtual	~nav_collector();
    virtual	std::string input_name(int i);
    virtual int	process();
    void calculate_nav(gps_record* rec);
	int set_next_waypoint();

	int open();
	void configure_properties();

protected:
    filter* m_fGpsInput;
    filter* m_fSpeedInput;

	struct ll_waypoint {
		double latitude;
		double longitude;
	};

	struct ll_waypoint m_llWaypoint;
	unsigned int m_iCurrent;

	std::vector<struct ll_waypoint> m_vllWaypoints;
	std::string	m_sPath;

	nav_record m_rNav;
    gps_record m_rDestination;
};

}

#endif /* __NAV_COLLECTOR_H__ */
