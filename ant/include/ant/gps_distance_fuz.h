#ifndef _GPS_SPEED_FUZ_H_
#define _GPS_SPEED_FUZ_H_

#include <ant/atypes.h>
#include <ant/fuzzy_control.h>
#include <ant/member_record.h>
#include <ant/std_records.h>

namespace ant
{

/**
 *  @class gps_distance_fuzzifier gps_distance_fuz.h "ant/gps_distance_fuz.h"
 */
class gps_distance_fuzzifier : public fuzzifier
{
    declare_factory_class(gps_distance_fuzzifier, fuzzifier);

public:
    gps_distance_fuzzifier();
    virtual		~gps_distance_fuzzifier();

protected:
    virtual	int	preprocess();
    virtual	int	fuzzify();

protected:
    member_record	m_rGpsDistance;
    integer_record	m_rFuzz;
};

}

#endif /* _GPS_SPEED_FUZ_H_ */
