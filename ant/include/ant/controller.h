#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#include <ant/collector.h>
#include <ant/commandset.h>

namespace ant
{

class controller: public collector
{
public:
    controller(record* rec = NULL, int re = 0, int ri = 0);
    ~controller();

public:
    virtual	int	process();

protected:
    commandset*	m_cCommandset;
};

}

#endif /* __CONTROLLER_H__ */
