#ifndef __GLOBAL_HAZARD_INPUT_H__
#define __GLOBAL_HAZARD_INPUT_H__

#include <iostream>
#include <ant/debug.h>
#include <ant/filter.h>
#include <ant/hazards_record.h>
#include <string>

#define GLOBAL_HAZARD_INPUT_UCID (0x500005)

using namespace std;

namespace ant
{

class global_hazard_input : public filter
{
	declare_factory_class(global_hazard_input, filter);
private:
    string m_sPath;
    hazards_record m_hrHazards;

public:
    global_hazard_input();
    global_hazard_input(string);
	 int open();
    ~global_hazard_input();
	 void configure_properties();
    hazards_record get_hazards_record();
};

}

#endif /* __GLOBAL_HAZARD_INPUT_H__*/
