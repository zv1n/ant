#ifndef __processor_h__
#define __processor_h__

#include <ant/filter.h>
#include <ant/input.h>
#include <ant/collector.h>
#include <ant/fuzzy_control.h>

#include <string>
#include <vector>

namespace ant
{

/**
 *  @enum PROCESS_STATES
 *  @brief Defines the state, either running or stopped, of the process loop.
 *  @var RUNNING
 *  @brief Used to signify a process loop is running.
 *  @var STOPPED
 *  @brief Used to signify a process loop is stopped.
 */
enum PROCESSING_STATES {
    RUNNING, STOPPED
};

/**
 *  @class processor processor.h "ant/processor.h"
 *  @brief processor takes inputs and outputs and iterates through each to
 *         process the environment.
 *
 *  The processor class, as implemented is a simple processor which iterates
 *  through each component serially.  Ideally this would be separated into a
 *  subclass of the parent processor, to allow for parallel processing of
 *  input resources.
 */
class processor
{
public:
    processor();
    virtual		~processor();

    void purge();

    virtual		int process();
    int add_input(filter*);
    int add_filter(filter*);
    int add_collector(collector*);
    int set_fuzzy_control(fuzzy_controller*);
    int add_controller(collector*);

    int fps();

	void set_profiling(bool enabled);

    property_set* get_filter(const std::string&);
    property_set* get_input(const std::string&);
    property_set* get_collector(const std::string&);
    property_set* get_controller(const std::string&);

    void	set_state(int state);
    int	state() const;
    int	reset();

protected:
    int track_fps();

    int process_inputs();
    int process_filters();
    int process_collectors();
    int process_fuzzy_control();
    int process_controllers();

protected:
    int m_iState;

    // FPS Variables
    int m_iFps;
    int m_iLastUpdate;

	bool m_bProfiling;
	int	m_iTotalTime;

    ant::fuzzy_controller*	m_pFuzzyControl;

    std::vector<filter*>	m_vfpInputs;
    std::vector<filter*>	m_vfpFilters;
    std::vector<collector*>	m_vcpCollectors;
    std::vector<collector*>	m_vcpControllers;
};
}

#endif /* __processor_h__ */
