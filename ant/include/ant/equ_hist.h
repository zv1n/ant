#ifndef __EQUHIST_H__
#define __EQUHIST_H__

#include <ant/factory.h>
#include <ant/filter.h>
#include <ant/frame.h>

#include <string>

using std::string;

namespace ant
{

class equhist : public filter
{
    declare_factory_class(equhist, filter);

public:
    equhist();
    virtual 		~equhist();
    virtual int	process();
    virtual	string  input_name(int i) const;
    ant::filter* 	m_fInput;
protected:
    frame			m_rFrame;
};

}

#endif
