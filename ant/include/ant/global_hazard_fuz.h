#ifndef _GLOBAL_HAZARD_FUZ_H_
#define _GLOBAL_HAZARD_FUZ_H_

#include <ant/atypes.h>
#include <ant/fuzzy_control.h>
#include <ant/member_record.h>
#include <ant/std_records.h>

namespace ant
{

/**
 *  @class global_hazard_fuz global_hazard_fuz.h "ant/global_hazard_fuz.h"
 */
class global_hazard_fuz : public fuzzifier
{
    declare_factory_class(global_hazard_fuz, fuzzifier);

public:
    global_hazard_fuz();
    virtual		~global_hazard_fuz();

protected:
    virtual	int	preprocess();
    virtual	int	fuzzify();

protected:
	int safe_route(int bearing, integer_array_record& ia);

protected:
    member_record	m_rHazards;
};

}

#endif /* _GLOBAL_HAZARD_FUZ_H_ */
