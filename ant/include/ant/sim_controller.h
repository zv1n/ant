#include <ant/controller.h>
#include <ant/frame.h>
#include <ant/std_records.h>
#include <string>


namespace ant
{

class sim_controller : public controller
{
public:
    sim_controller();
    virtual ~sim_controller();
    virtual int process();
    virtual std::string input_name(int) const;

protected:
    frame m_rFrame;
    filter* m_fInput;
};

}
