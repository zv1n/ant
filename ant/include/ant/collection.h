#ifndef __collection_h__
#define __collection_h__

#include <ant/atypes.h>
#include <ant/factory.h>
#include <ant/named.h>
#include <ant/log_manager.h>

#include <string>
#include <vector>

#include <stdio.h>

namespace ant
{

class filter;

/**
 *  @def RECORD_BASE_TYPE
 *  @brief The base name for the top-level record class.
 */
#define RECORD_BASE_TYPE ("base record")

enum ACTION {
    FETCH,
    STORE
};


/**
 *  @def NO_LOG_MANAGER
 *  @brief Error returned when there is no log manager available to a record.
 */
#define NO_LOG_MANAGER	(0x01000001)

/**
 *  @def NO_LOG_REQUIRED
 *  @brief Error returned when there is no log manager available to a record.
 */
#define NO_LOG_REQUIRED	(0x01000002)

/**
 *  @def IS_LOG_REQUIRED(force)
 *  @brief Test to see if logging is required for this particular record and return INVALID if not.
 */
#define IS_LOG_REQUIRED(x) \
	if (!record::s_logManager) \
		return NO_LOG_MANAGER; \
	if (!x) { \
		if (!record::s_logManager->logging(name())) \
			return NO_LOG_REQUIRED; \
	}

/**
 *  @class record collection.h "ant/collection.h"
 *  @brief Base record class for all data processed by a filter or collector.
 *
 *  Handles all common functions between records including their attachment to
 *  parent filters/collectors.
 */
class record : public named_type
{
public:
    record(filter* el = NULL);
    record(const record& rec);
    virtual     ~record();
    virtual     void			dump() const = 0;
    virtual 	int 			size() const = 0;
    virtual		ucid_t		ucid() const;
    void		set_name(const std::string& name);
    /* new_collection_record must return a 'new' object with the same name!! */
    virtual		record*		new_collection_record() const = 0;
    virtual		int		log(bool force = false) const = 0;
    static		std::string 	type();
    static		ucid_t		record_ucid();

    filter* 		get_filter();
    int			set_filter(filter* el);
    int				current() const;
    virtual		int		update_from(record*);

    int 	open_log_file() const;
    int 	get_fd(bool* fresh = NULL) const;

    static	void set_log_manager(log_manager* lman);
    static	log_manager* get_log_manager();

protected:
    static log_manager* s_logManager;

private:
    filter*		m_eParent;
    int			m_iUpdates;
    mutable int	m_iFd;

};


/**
 *  @fn template <class T> T* record_cast(record* res)
 *  @brief Cast function used to convert between record types.
 *
 *  record_cast uses the stored UCID type of the class in order to safetly
 *  static_cast to the requested class type.  As it is a template class,
 *  attempts to cast to an incompatable class will fail at compile time.
 *
 *  @param T - Class to cast to - must be descendent of record.
 *  @param res - A pointer to a record to cast to type (T*)
 *
 *  @return - Pointer to res cast to the specified format.
 */
template <class T>
T* record_cast(record* res)
{
    if (res->ucid() == T::record_ucid())
        return static_cast<T*>(res);
    return NULL;
}

/**
 *  @class collection
 *  @brief Container class for all records.  Primary indexing is by name.
 *
 *  Collections handle the primary interaction between updates of processing.
 *  A record is tored by name and every update, the same record is updated.
 *  Standard add/remove functions are provided for ease of use.
 */
class collection : public named_type
{
public:
    collection();
    ~collection();

    int size() const;

    int add(record*); /*dupe checks and adds*/
    int	update(record*);
    int	fetch(record*);
    int	update(record*, std::string);
    int	fetch(record*, std::string);
    int	update(record*, int, const std::string& name);
    int remove(const record* res);
    int remove(int idx);

    record* at(int idx);
    record* at(const std::string&);
    template <class T>
    T* get(const std::string&);
    record* operator[](int);
    record* operator[](const std::string&);

    const record* at(int idx) const;
    const record* at(const std::string&) const;
    template <class T>
    const T* get(const std::string&) const;
    const record* operator[](int) const;
    const record* operator[](const std::string&) const;

    void dump() const;

    std::vector<record*>::iterator begin();
    std::vector<record*>::iterator end();

    declare_property_class();

private:
    std::vector<record*>	m_vResults;

};

template <> record*
collection::get<record>(const std::string&);
template <> const record*
collection::get<record>(const std::string&) const;

/**
 *  @fn template <class T> T* collection::get(const std::string& str)
 *  @brief Helper function which returns the requested record cast to the
 *   specified record class.
 *
 *  @param str - The name of the record being requested.
 *
 *  @return - Pointer to the requested record; NULL on error.
 */
template <class T> T*
collection::get(const std::string& str)
{
    record* rec = at(str);
    if (!rec) {
        return NULL;
    }

    if (T::record_ucid() != rec->ucid()) {
        return NULL;
    }

    return static_cast<T*>(rec);
}

/**
 *  @fn template <class T> const T* collection::get(const std::string& str)
 *  @brief Helper function which returns the requested record cast to the
 *   specified record class.
 *
 *  @param str - The name of the record being requested.
 *
 *  @return - Constant pointer to the requested record; NULL on error.
 */
template <class T>  const T*
collection::get(const std::string& str) const
{
    record* rec = at(str);
    if (!rec) {
        return NULL;
    }

    if (T::record_ucid() != rec->ucid()) {
        return NULL;
    }

    return static_cast<T*>(rec);
}

#undef DEBUG

}

#endif /* __collection_h__ */
