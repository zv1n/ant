#ifndef __DYN_THRESHOLD_H__
#define __DYN_THRESHOLD_H__

#include <ant/factory.h>
#include <ant/filter.h>
#include <ant/frame.h>

#include <string>

using std::string;

namespace ant
{

class dynamic_threshold : public filter
{
    declare_factory_class(dynamic_threshold, filter);

public:
    dynamic_threshold();
    virtual 		~dynamic_threshold();
    virtual int	process();
    virtual	string  input_name(int i) const;
protected:
    frame			m_rFrame;
    ant::filter* 	m_fInput;
    int				m_iFooterThresh;
    int				m_iMaxDelay;
    int				m_iFooterDelay;
};

}

#endif
