#ifndef _IMAGE_BEARING_FUZ_H_
#define _IMAGE_BEARING_FUZ_H_

#include <ant/atypes.h>
#include <ant/fuzzy_control.h>
#include <ant/member_record.h>
#include <ant/std_records.h>

namespace ant
{

/**
 *  @class image_bearing_fuzzifier image_bearing_fuz.h "ant/image_bearing_fuz.h"
 *  Fuzzifier used to take information extracted from a given image and produce
 *	the fuzzified member values used by the fuzzy controller.
 */
class image_bearing_fuzzifier : public fuzzifier
{
    declare_factory_class(image_bearing_fuzzifier, fuzzifier);

public:
    image_bearing_fuzzifier();
    virtual		~image_bearing_fuzzifier();

protected:
    virtual	int	preprocess();
    virtual	int	fuzzify();

protected:
	int process_iar(integer_array_record& ia, integer_record& fuz);

protected:
    member_record	m_rNearImage;
    member_record	m_rFarImage;

    integer_record	m_rNearFuzz;
    integer_record	m_rFarFuzz;
};

}

#endif /* _IMAGE_BEARING_FUZ_H_ */
