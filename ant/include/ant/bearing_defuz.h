#ifndef _FCDBEARING_H_
#define _FCDBEARING_H_

#include <ant/fuzzy_control.h>
#include <ant/member_record.h>
#include <ant/std_records.h>

namespace ant
{

/**
 *  @class bearing_defuzzifier bearing_defuzz.h "ant/bearing_defuzz.h"
 *  Defuzzifier used to coalesce member values associated with bearing.
 *
 *	The bearing_defuzzifier uses the member_record member sets stored in the
 *	input collection provided by the Fuzzy Controller.  The members retrieved
 *  are then resolved down to a single output in the inference function
 *	using the built in functions of the member class.
 *	The values used for the defuzzification table is a arbitrarily chosen
 *	symmetric series of numbers.  Finally, the reduced numeric value must then
 *	be adapted for outpout into the system. This is done in the defuzzify stage.
 *  In the case of this class, the output is changed to a floating point value
 *  representing the percent of throttle needed in the turn.
 */
class bearing_defuzzifier : public defuzzifier
{
    declare_factory_class(bearing_defuzzifier, defuzzifier);

public:
    bearing_defuzzifier();
    virtual		~bearing_defuzzifier();

protected:
    virtual	int	inference();
    virtual	int	defuzzify();

protected:
    /**
     *  @var m_rBearing
     *	The float_record used to store the desired output throttle.
     */
    float_record	m_rBearing;

    /**
     *	@var m_rDefuzz
     *  The member_record used to coalesce all data from the fuzzifiers.
     */
    member_record	m_rNearDefuz;
    member_record	m_rFarDefuz;

	/**
	 *	@var m_rtNearTable
	 *	Table for outputs related to the near field inputs.
	 */
    response_table	m_rtNearTable;

	/**
	 *	@var m_rtBHVTable
	 *	Table for outputs related to the far-field, global hazard, and bearing
	 *	 inputs.
	 */
    response_table	m_rtBHVTable;
	
	bool m_bNear;
};

}

#endif /* _FCDBEARING_H_ */
