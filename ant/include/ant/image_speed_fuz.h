#ifndef _IMAGE_SPEED_FUZ_H_
#define _IMAGE_SPEED_FUZ_H_

#include <ant/atypes.h>
#include <ant/fuzzy_control.h>
#include <ant/member_record.h>
#include <ant/std_records.h>

namespace ant
{

/**
 *  @class image_speed_fuzzifier image_fuz.h "ant/image_fuz.h"
 *  Fuzzifier used to take information extracted from a given image and produce
 *	the fuzzified member values used by the fuzzy controller.
 */
class image_speed_fuzzifier : public fuzzifier
{
    declare_factory_class(image_speed_fuzzifier, fuzzifier);

public:
    image_speed_fuzzifier();
    virtual		~image_speed_fuzzifier();

protected:
    virtual	int	preprocess();
    virtual	int	fuzzify();

protected:
    member_record	m_rNearImage;
    integer_record	m_rNearFuzz;
    member_record	m_rFarImage;
    integer_record	m_rFarFuzz;
};

}

#endif /* _IMAGE_SPEED_FUZ_H_ */
