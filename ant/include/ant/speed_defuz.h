#ifndef _SPEED_DEFUZ_H_
#define _SPEED_DEFUZ_H_

#include <ant/fuzzy_control.h>
#include <ant/member_record.h>
#include <ant/std_records.h>

namespace ant
{

/**
 *  @class speed_defuzzifier speed_defuzz.h "ant/speed_defuzz.h"
 *  Defuzzifier used to coalesce member values associated with speed.
 *
 *	The speed_defuzzifier uses the member_record member sets stored in the
 *	input collection provided by the Fuzzy Controller.  The members retrieved
 *  are then resolved down to a single output in the inference function
 *	using the built in functions of the member class.
 *	The values used for the defuzzification table is a arbitrarily chosen
 *	symmetric series of numbers.  Finally, the reduced numeric value must then
 *	be adapted for outpout into the system. This is done in the defuzzify stage.
 *  In the case of this class, the output is changed to a floating point value
 *  representing the percent of throttle needed in the turn.
 */
class speed_defuzzifier : public defuzzifier
{
    declare_factory_class(speed_defuzzifier, defuzzifier);

public:
    speed_defuzzifier();
    virtual		~speed_defuzzifier();

protected:
    virtual	int	inference();
    virtual	int	defuzzify();

protected:
    /**
     *  @var m_rSpeed
     *	The float_record used to store the desired output throttle.
     */
    float_record	m_rSpeed;

    /**
     *	@var m_rDefuzz
     *  The member_record used to coalesce all data from the fuzzifiers.
     */
    member_record	m_rDefuzz;

	/**
 	 *	@var m_rTable
	 *	Handles resolution of fuzzy values based on fuzzy inputs.
	 */
	response_table	m_rtTable;
};

}

#endif /* _FCDBEARING_H_ */
