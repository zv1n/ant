#ifndef __LOG_MANAGER_H__
#define __LOG_MANAGER_H__

#include <ant/named.h>

#include <map>
#include <string>
#include <stdint.h>

namespace ant
{

class record;

#define LMM_ACTIVE 		(0x1)
#define LMM_PASSIVE 	(0x2)

#define LM_DISABLED 	(0x1)
#define LM_ENABLED  	(0x2)
#define LM_UPDATED_FILE (0x4)

#define MAX_DATE (32)
#define MAX_TIME (32)

#define CURWD 	(0)
#define DATE 	(1)
#define TIME 	(2)

/**
 *  @class log_manager log_manager.h "ant/log_manager.h"
 *  @brief Tracks all named records and triggers log reporting
 *
 *  log_manager will store a pointer to each record which
 *  gets named.
 */
class log_manager : public named_type
{
public:
    log_manager(uint8_t mode = LMM_PASSIVE);
    log_manager(const std::string file, uint8_t mode = LMM_PASSIVE);
    ~log_manager();

    /* modify logs */
    int 	load_from_file(const std::string file);

    int 	set_flags(const std::string name, int flag);
    int 	set_interval(const std::string name, int interval);
    int 	set_file(const std::string name, const std::string file);

    int		get_flags(const std::string name, int& flags);
    int 	get_interval(const std::string name, int& interval);
    int 	get_file(const std::string name, std::string& str);

    record*	get_record(const std::string name);

    int 	register_record(record* rec); /* must be named! */
    int 	remove_record(record* rec);
    void 	clear();
    bool	exists(const std::string name) const;

    /* status */
    void 	set_logging(bool);
    bool 	logging() const;
    bool 	logging(const std::string name) const;

    int		log_all();

    static	char*	get_value(int val);

protected:

    int		replace_variables(std::string& str);
    int		process_config(int line, std::string config);

protected:

    struct record_map {
        int flags;
        int interval;
        record* entry;
        std::string file;
    };
    typedef std::map<std::string, record_map>::iterator
    record_map_iter;
    typedef std::map<std::string, record_map>::const_iterator
    const_record_map_iter;

    uint8_t	m_ucMode;
    bool	m_bLog;
    std::map<std::string, record_map>	m_rMap;

};

}

#endif /* __LOG_MANAGER_H__ */
