#ifndef __INPUT_H__
#define __INPUT_H__

#include <ant/factory.h>

#include <vector>
#include <string>

namespace ant
{
class filter;
/* Declare the input factory (video input, file input) */
declare_factory(filter, input);
}

#endif /* __INPUT_H__ */
