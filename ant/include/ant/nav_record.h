#ifndef __NAVRECORD_H__
#define __NAVRECORD_H__

#include <ant/collection.h>
#include <string>

namespace ant
{

#define NAV_RECORD_TYPE ("navigation")

/* @class nav_record nav_record.h "ant/nav_record.h"
 * @brief Describes a navigation element (relative bearing and speed)
 *
 * The navigation system takes as input the current location and
 * destination location to determine a distance from the destination
 * and bearing relative to the current bearing and destinantion.
 *
 */

class nav_record : public record
{
public:
    nav_record();
    virtual ~nav_record();
    virtual record* new_collection_record() const;
    virtual void dump() const;
    virtual int size() const;
    virtual int update_from(record*);
    virtual int log(bool force = false) const;
    static std::string type();

    double distance() const;
    int rel_bearing() const;
	int true_bearing() const;
	bool valid_bearing() const;
	bool at_destination() const;
	int waypoint() const;

    int set_waypoint(int wpt);
    int set_distance(double dist);
    int set_rel_bearing(int bear);
	int set_true_bearing(int bear);
	int set_valid_bearing(bool dest);
	int set_at_destination(bool dest);

private:
	int m_iWaypoint;
    double m_dDistance;
    int m_iRelBearing;
	int m_iTrueBearing;
	bool m_bValBearing;
	bool m_bFinished;
};

}

#endif /* __NAVRECORD_H__ */
