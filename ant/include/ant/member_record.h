#ifndef _MEMBER_RECORD_H_
#define _MEMBER_RECORD_H_

#include <ant/propertyset.h>
#include <ant/collection.h>
#include <ant/enums.h>

namespace ant
{

/**
 *  @typedef int mid_t
 *  @brief Create a type for the integer value of the member id.
 */
typedef int mid_t;

class member_record;

/**
 *  @class member member_record.h "ant/member_record.h"
 *  @brief member is a class which is used to handle the comination of
 */
class member
{
    friend class member_record;
public:
    member(mid_t ms = INVALID, int base = 0);
    member(mid_t ms, float pc);
    member(mid_t ms, const std::vector<float>& percent, int base = 0);
    member(const member& mem);
    ~member() {}

    member& operator=(const member& mem);

    void 	clear() {
        m_vPercent.clear();
    };

    enum { MINIMUM, PRODUCT };

    mid_t	member_id() const	{
        return m_iMemberId;
    }
    float	percent(int type) const	{
        return converge(type);
    }
    int		base() const	{
        return m_iBase;
    }
    void	dump() const;
    void	log(int fd) const;

    const 	std::vector<float>::const_iterator 	begin() const;
    const 	std::vector<float>::const_iterator 	end() const;

    unsigned int	size() const {
        return m_vPercent.size();
    }

    void	add(float x);
    int		set_list(const std::vector<float>&);
    int		append(const std::vector<float>&);
    const std::vector<float>&	get_list()	const;

protected:
    float	converge(int type) const;
    float	min_converge() const;
    float 	prod_converge() const;

protected:
    mid_t				m_iMemberId;
    std::vector<float>	m_vPercent;
    int 				m_iBase;
};

class integer_record;
class response_table;

#define MEMBER_RECORD_TYPE ("member")
#define MEMBER_RECORD_UCID (RECORD_BASE_UCID + 0x3)

class member_record : public record
{
public:
    member_record();
    member_record(const member_record&);
    virtual		~member_record();

public:
    typedef std::vector<member>::const_iterator const_iterator;
    typedef std::vector<member>::iterator iterator;

    virtual record*	new_collection_record() const;
    virtual	int		size()	const;
    virtual	ucid_t	ucid()	const;
    virtual	int		log(bool force = false)	const;
    static	std::string	type();
    static	ucid_t		record_ucid();

	unsigned int member_count() const;
    int			clear();
    const_iterator	begin() const;
    const_iterator	end() const;
    iterator	begin();
    iterator	end();

    int	set_union_type(int type);
    int	union_type() const;
    int	set_defuzz_type(int type);
    int	defuzz_type() const;

    int	set_member(const member& mem);
    int set_values(int size, mid_t* ids, int* values);
    int add_member(mid_t memberId, float percent);
    int get_member(mid_t memberId, member& mem) const;
    int get_member(mid_t memberId, float& percent) const;
	mid_t get_member_id(unsigned int x) const;

	int cart_product(const response_table& table, ...); 
	int cart_product(const response_table& table, 
					std::vector<member_record*>& inputs);

    int	update_from(record* res);

    enum { SUM_OF_PRODUCTS };

    float	defuzz(int type = SUM_OF_PRODUCTS) const;
    int		map_input(const integer_record* rec);
    int		map_input(int val);
    void	dump() const;
    void	log(int fd) const;

protected:
	int recurse_table(const response_table& table, 
						std::vector<member_record*>& rec,
						std::vector<mid_t>& entries, unsigned int idx);
    float			defuzz_sop() const;
    iterator 		get_member(mid_t mid);
    const_iterator 	get_member(mid_t mid) const;

protected:
    int m_iDefuzz;
    int	m_iUnion;
	int	m_iActiveMembers;
    std::vector<member>	m_vMembership;
};

}

#endif /* _MEMBER_RECORD_H_ */
