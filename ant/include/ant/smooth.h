#ifndef __SMOOTH_H__
#define __SMOOTH_H__

#include <ant/factory.h>
#include <ant/filter.h>
#include <ant/frame.h>
#include <ant/enums.h>

#include <string>

#include <opencv/cv.h>

using std::string;

namespace ant
{

/**
 *  @class smooth smooth.h "ant/smooth.h"
 *  @brief smooth takes an input frame_record and outputs a smooth frame_record.
 *
 *  The smooth filter uses the OpenCV library to apply a smooth filter to an
 *  input frame_record which can be fetched at output.
 */
class smooth : public filter
{
    declare_factory_class(smooth, filter);

public:
    smooth();
    virtual 		~smooth();
    virtual int	process();
    virtual	string  input_name(int i) const;

protected:
    /**
     *  @var m_fInput
     *  The input filter on which the smooth filter operates.
     */
    ant::filter* 	m_fInput;

    /**
     *  @var m_rFrame
     *  The frame record where the filter video frame is stored.
     */
    frame			m_rFrame;

protected:

    /**
     *  @var m_eType
     *	Variable used to select which type of smooth operation to perform.
     *	Values are:
     *		- ANT_MEDIAN:	Which performs a cv::medianBlur
     *		- ANT_GAUSSIAN:	Which performs a cv::GaussianBlur
     */
    int				m_eType;

    /**
     *	@var m_iParam1
     *	Property: "Param1"
     *		- ANT_MEDIAN:	Level of blur.
     *		- ANT_GAUSSIAN:	Horizontal size of Gaussian kernel.
     */
    int				m_iParam1;

    /**
     *	@var m_iParam2
     *	Property: "Param2"
     *  	- ANT_MEDIAN:	Unused
     *  	- ANT_GAUSSIAN:	Vertical size of Gaussian kernel.
     */
    int				m_iParam2;

    /**
     *	@var m_dParam3
     *	Property: "Param3"
     *  	- ANT_MEDIAN:	Unused
     *  	- ANT_GAUSSIAN:	Gaussian kernel horizontal standard deviation.
     */
    double			m_dParam3;

    /**
     *	@var m_dParam4
     *	Property: "Param4"
     *  	- ANT_MEDIAN:	Unused
     *  	- ANT_GAUSSIAN:	Gaussian kernel vertical standard deviation.
     */
    double			m_dParam4;
};

}

#endif
