#ifndef __video_h__
#define __video_h__

#include <ant/filter.h>
#include <ant/frame.h>

#include <iostream>
#include <vector>

#include <opencv/cv.h>
#include <opencv/highgui.h>

using namespace std;
namespace ant
{

class videoin : public filter
{
    declare_factory_class(videoin, filter);

public:
    videoin(int cap);
    videoin();
    ~videoin();

    virtual		int		process();
    virtual		std::string	input_name(int i) const;
    static		int			enumerate_devices(std::vector<int>& inputs);

    int		set_device(int dev);
    bool		is_opened();

protected:
    void		configure_properties();
    int		device_changed(std::string&);

protected:
    int			m_iDevice;

	cv::VideoCapture m_cvCapture;
    frame			m_rFrame;
};


}

#endif /* __video_h__ */
