#ifndef __COMM_H__
#define __COMM_H__

#include <ant/named.h>
#include <string>
#include <stdint.h>

namespace ant
{

enum COMM_STATUS {CONNECTED, DISCONNECTED, ERROR};

/*
 *  @class commandset commandset.h "and/commandset.h"
 *  @brief Base class used to define a communication protocol
 *
 *  Comm is a base class to define a communication protocol. Ideally, each method
 *  of communication is derived from this class (i.e. serial, radio, IR, optical, etc.)
 */
class comm : public named_type
{
public:
//		virtual int init(std::string) = 0;
    virtual void  close() = 0;
    virtual int  send(uint8_t*, uint16_t) = 0;
    virtual int  recv(uint8_t*, uint16_t) = 0;
    COMM_STATUS status();

protected:
    COMM_STATUS	st;
};

}

#endif
