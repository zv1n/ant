#ifndef __PROPERTYSET_H__
#define __PROPERTYSET_H__

#include <iostream>
#include <vector>
#include <string>
#include <map>

#include <ant/named.h>
#include <ant/factory.h>

/**
 *	@def declare_property_set()
 *	Used to declare necessary member functions for classes which inherit
 *  property_set.
 */
#define declare_property_set()\
	public: \
static ucid_t	ucid();

/**
 *  @def define_property_set(class, ucid)
 *  Used to define the necessary member functions for classes which inherit
 *	from property_set.
 */
#define define_property_set(cls, sucid)\
	ucid_t	cls::ucid() { return sucid; }

namespace ant
{

/**
 *	@enum PROP_TYPES
 *	Type definitions used to set the property type of each individual property.
 */
enum PROP_TYPES {
    INT32 = 0x1,
    INT64 = 0x2,
    ULONG = 0x3,
    STRING = 0x4,
    FLOAT = 0x5,
    DOUBLE = 0x6,
    STD_TYPE_MASK = 0xFFFF,
    PTR	= 0x1,
    VTYPE	= 0x2,
    STD	= 0x4,
    RO	= 0x8,
    WO	= 0x10,
    RW	= RO|WO,
    READ	= 0x8,
    WRITE	= 0x10,
    FILE	= 0x1000,
    SPEC_MASK = 0xFFFF
};

class property_set;

/**
 *	@typedef int (property_set::*propset_fn)(const std::string&)
 *	Function pointers which are called before and after a property is changed.
 */
typedef int (property_set::*propset_fn)(const std::string&);

/**
 *	@struct property propertyset.h "ant/propertyset.h"
 *  Structure used to store necessary data to manage a property.
 */
struct property {
    std::string		name;
    unsigned long	offset;
    ucid_t			ucid;
    unsigned int	type;
    unsigned long	this_pre;
    propset_fn		pre;
    unsigned long	this_post;
    propset_fn		post;
};

/**
 *	@class property_set propertyset.h "ant/propertyset.h"
 *	@brief Manages sets of properties of almost any type.
 *
 *	property_set is intended to be inherited by more complex classes.
 *  It is designed to provide a human readable user interface for
 *	easy handling from a Graphical User Interface.
 */
class property_set: public named_type
{
public:
    property_set();

protected:
    template <class T>
    int		add_property(const std::string& name, T& var, int type=RW);
    template <class T>
    int		add_property(const std::string& name, T*& var, int type=RW);
    struct property*
    get_property_struct(const std::string& name);

    int		set_preset(const std::string& name, void* ths, propset_fn pre);
    int		set_postset(const std::string& name, void* ths, propset_fn post);

public:
    template <class T>
    int		get_property(const std::string& name, T& var);
    template <class T>
    int		get_property(const std::string& name, T*& var);

public:
    template <class T>
    int		set_property(const std::string& name, T& var);
    template <class T>
    int		set_property(const std::string& name, T*& var);
    template <class T>
    int		set_property_const(const std::string& name, const T var);
    template <class T>
    int		set_property_const(const std::string& name, T* var);

public:
    int		enumerate_properties(std::vector<std::string>& names) const;
    template <class T>
    int		find_properties(std::vector<std::string>&, int t = 0) const;
    int		get_property_type(const std::string& name, unsigned int& type) const;
    int		get_property_ucid(const std::string& name, ucid_t& ucid) const;
    template <class T>	int	is_type(ucid_t ucid);
    template <class T>	int	is_type(const std::string& info);

protected:
    int call_pre(struct property*);
    int call_post(struct property*);

private:
    /**
     *	@var m_mspProperties
     *	The std::map used to associate a property name with its property
     * 	structure.
     */
    std::map<std::string, struct property>		m_mspProperties;

};

/**
 *	@def tempret template <> int_property_set
 *	Macro solely used to simplify the appearace of later macros.
 */
#define tempret template <> int property_set

/**
 *	@def declare_base_property_functions(ctype)
 *	Macro used to declare the necessary property functions associated with a
 *	predefined and managed variable type.
 */
#define declare_base_property_functions(ctype)\
tempret::add_property<ctype>(const std::string& name, ctype&, int type);\
tempret::get_property<ctype>(const std::string& name, ctype&);\
tempret::set_property<ctype>(const std::string& name, ctype&);\
tempret::set_property_const<ctype>(const std::string& name, const ctype);\
tempret::find_properties<ctype>(std::vector<std::string>&, int) const;\
tempret::is_type<ctype>(ucid_t ucid);\
tempret::is_type<ctype>(const std::string& info)

/* Declares the functions associated with: int, float, double, std::string, and
 * unsigned long.
 */
declare_base_property_functions(int);
declare_base_property_functions(float);
declare_base_property_functions(double);
declare_base_property_functions(std::string);
declare_base_property_functions(unsigned long);

/**
 *	@fn int add_property(const std::string& name, T& var, int type)
 *  Template function used to add a property to the property set mapping.
 */
template <class T> int
property_set::add_property(const std::string& name, T& var, int type)
{
    struct property prop = {
        name,
        (unsigned long)&var,
        T::ucid(),
        VTYPE|type
    };

    std::map<std::string, struct property>::iterator it;
    it = m_mspProperties.find(name);
    if (it != m_mspProperties.end())
        return INVALID;

    m_mspProperties[name] = prop;
    return VALID;
}

/**
 *	@fn int add_property(const std::string& name, T*& var, int type)
 *  Template function used to add a pointer property to the property set
 *	mapping.
 */
template <class T> int
property_set::add_property(const std::string& name, T*& var, int type)
{
    struct property prop = {
        name,
        (unsigned long)&var,
        T::ucid(),
        VTYPE|PTR|type,
		0,NULL,0,NULL
    };

    std::map<std::string, struct property>::iterator it;
    it = m_mspProperties.find(name);
    if (it != m_mspProperties.end())
        return INVALID;

    m_mspProperties[name] = prop;
    return VALID;
}

/**
 *	@fn int get_property(const std::string& name, T& var)
 *  Template function used to get a property value from the mapping.
 */
template <class T> int
property_set::get_property(const std::string& name, T& var)
{
    struct property* prop = get_property_struct(name);
    if (!prop || !(prop->type&VTYPE) || prop->ucid != T::ucid())
        return INVALID;

    T* addr = (T*)prop->offset;
    var = *addr;

    return VALID;
}

/**
 *	@fn int get_property(const std::string& name, T*& var)
 *  Template function used to get a property pointer value from the mapping.
 */
template <class T> int
property_set::get_property(const std::string& name, T*& var)
{
    struct property* prop = get_property_struct(name);
    if (!prop || (prop->type&(VTYPE|PTR)) != (VTYPE|PTR) ||
        prop->ucid != T::ucid()) {
        return INVALID;
    }

    T** addr = (T**)prop->offset;
    var = *addr;

    return VALID;
}

/**
 *	@fn int set_property_const{const std::string& name, T* var)
 *	Template function used to set constant properties.
 */
template <class T> int
property_set::set_property_const(const std::string& name, T* var)
{
    struct property* prop = get_property_struct(name);
    if (!prop || (prop->type&(VTYPE|PTR)) != (VTYPE|PTR) ||
        prop->ucid != T::ucid()) {
        std::cout << "'" << name << "': failed verification..." << std::endl;
        return INVALID;
    }

    if (!(prop->type&WRITE)) {
        std::cout << "'" << name << "': read only..." << std::endl;
        return INVALID;
    }

    if (call_pre(prop)) {
        std::cout << "'" << name << "': failed preset..." << std::endl;
        return INVALID;
    }

    T** addr = (T**)prop->offset;
    *addr = var;

    if (call_post(prop)) {
        std::cout << "'" << name << "': failed postset..." << std::endl;
        return INVALID;
    }

    return VALID;
}

/**
 *	@fn int set_property_const(const std::string& name, const T var)
 *	Template function used to set a constant property value.
 */
template <class T> int
property_set::set_property_const(const std::string& name, const T var)
{
    struct property* prop = get_property_struct(name);
    if (!prop || !(prop->type&VTYPE) || prop->ucid != T::ucid())
        return INVALID;

    if (!(prop->type&WRITE))
        return INVALID;

    if (call_pre(prop))
        return INVALID;

    T* addr = (T*)prop->offset;
    *addr = var;

    if (call_post(prop))
        return INVALID;

    return VALID;
}

/**
 *	@fn int set_property(const std::string& name, T& var)
 *	Template function used to set a property value.
 */
template <class T> int
property_set::set_property(const std::string& name, T& var)
{
    struct property* prop = get_property_struct(name);
    if (!prop || !(prop->type&VTYPE) || prop->ucid != T::ucid())
        return INVALID;

    if (!(prop->type&WRITE))
        return INVALID;

    if (call_pre(prop))
        return INVALID;

    T* addr = (T*)prop->offset;
    *addr = var;

    if (call_post(prop))
        return INVALID;

    return VALID;
}

/**
 *	@fn int set_property(const std::string& name, T*& var)
 *	Template function used to set a pointer property value.
 */
template <class T> int
property_set::set_property(const std::string& name, T*& var)
{
    struct property* prop = get_property_struct(name);
    if (!prop || (prop->type&(VTYPE|PTR)) != (VTYPE|PTR) ||
        prop->ucid != T::ucid()) {
        std::cout << "Failed to validate property..." << std::endl;
        return INVALID;
    }

    if (!(prop->type&WRITE)) {
        std::cout << "Read only property!" << std::endl;
        return INVALID;
    }

    if (call_pre(prop)) {
        std::cout << "Failed preset criteria!" << std::endl;
        return INVALID;
    }

    T** addr = (T**)prop->offset;
    *addr = var;

    if (call_post(prop)) {
        std::cout << "Failed postset criteria!" << std::endl;
        return INVALID;
    }

    return VALID;
}

/**
 *	@fn int is_type(ucid_t ucid)
 *	Template function used to validate the UCID of a given type.
 *
 * 	@param ucid The ucid to compare against the type T.
 *
 *	@return VALID if the types match, INVALID otherwise.
 */
template <class T> int
property_set::is_type(ucid_t ucid)
{
    if (ucid != T::ucid())
        return INVALID;

    return VALID;
}

/**
 *	@fn int is_type(const std::string& name)
 *	Template function used to validate the name of a given type.
 *
 * 	@param name The name to compare against the type T.
 *
 *	@return VALID if the types match, INVALID otherwise.
 */
template <class T> int
property_set::is_type(const std::string& name)
{
    std::map<std::string, struct property>::iterator it;
    it = m_mspProperties.find(name);

    if (it == m_mspProperties.end())
        return INVALID;

    if (it->second.type&VTYPE != VTYPE)
        return INVALID;

    if (it->second.ucid != T::ucid())
        return INVALID;

    return VALID;
}

/**
 *	@fn	int find_properties(std::vector<std::string>& names, int type) const
 *	Template function used to enumerate all properties of a given type.
 *
 *	@param names std::vector of std::strings in which the property names are
 *			stored.
 *	@param type Type modifier to associate with the process enumeration (e.g.
 *			WRITE, PTR, etc.)
 *
 *	@return ALways returns VALID.
 */
template <class T> int
property_set::find_properties(std::vector<std::string>& names, int type) const
{
    std::map<std::string, struct property>::const_iterator it;
    for (it = m_mspProperties.begin(); it != m_mspProperties.end(); it++) {
        if (it->second.type&(VTYPE|type) == (VTYPE|type)
            && it->second.ucid == T::ucid())
            names.push_back(it->first);
    }
    return VALID;
}

}

#endif /*__PROPERTYSET_H__*/
