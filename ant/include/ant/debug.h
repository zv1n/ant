#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <stdio.h>
#include <fcntl.h>

#include <string>

/**
 *  @def DEBUGGING
 *  @brief Defined in ant.mk during make to set whether to print debug output.
 */
#if DEBUGGING
/**
 *  @def DEBUG(x, ...)
 *  @brief Macro used to output debug information based on desired debug level.
 */
#define DEBUG(x, ...) ant::std.log(x, __FILE__, __LINE__, __VA_ARGS__)
#else
#define DEBUG(...) do {} while(0)
#endif

/**
 *  @def FAIL(x, ...)
 *  @brief Macro used to output failure info based on desired debug level.
 */
#define FAIL(x, ...) ant::err.log(x, __FILE__, __LINE__, __VA_ARGS__)

namespace ant
{

/**
 *  @enum LOGGER_LEVELS
 *  @brief Defines the desired logging levels.
 *
 *  @var LOGGER_LEVELS ALL
 *  @brief Specify to display all prints.
 *
 *  @var LOGGER_LEVELS TLDR
 *  @brief Specify to display most prints.
 *
 *  @var LOGGER_LEVELS MINOR
 *  @brief Specify to display all minor prints.
 *
 *  @var LOGGER_LEVELS MAJOR
 *  @brief Specify to display all major prints.
 *
 *  @var LOGGER_LEVELS DANGER_WILL_ROBINSON
 *  @brief Specify to display only critical prints.
 *
 *  @var LOGGER_LEVELS NONE
 *  @brief Specify to display only critical prints.
 */
enum LOGGER_LEVELS {
    ALL,
    TLDR,
    MINOR,
    MAJOR,
    DANGER_WILL_ROBINSON,
    NONE
};

/**
 *  @class debug_logger debug.h "ant/debug.h"
 *  @brief Handles all logging from the ANT Framework.
 */
class debug_logger
{
public:
    debug_logger(::FILE* file = NULL);
    ~debug_logger();
public:
    int log(int level, const char* func, int line, const char* fmt, ...);
    int set_file(const std::string filename);
    int set_level(int level);
    int close();
    int flush();
    int isatty();

protected:
    int   m_iLevel;
    ::FILE* m_pFile;
};

extern debug_logger err;
extern debug_logger std;

}

#endif /* __DEBUG_H__ */
