#ifndef __media_h__
#define __media_h__

#include <ant/filter.h>
#include <ant/frame.h>

#include <iostream>
#include <vector>
#include <opencv/cv.h>
#include <opencv/highgui.h>

namespace ant
{

/**
 *  @def MEDIA_UCID
 *  @brief Defines the UCID of the media class.
 */
#define MEDIA_UCID (0x20001)

/**
 *  @class media media.h "ant/media.h"
 *  @brief media handles the processing of videon input from a file.
 *
 *  The media class uses the OpenCV library to load image frames from a file.
 *  The framerate can be set via a property and defaults to 24 fps.
 */
class media : public filter
{
    declare_factory_class(media, filter);
public:
    media(const std::string& path);
    media();
    ~media();

    virtual		int		process();

    int		set_file(std::string path);
    int		is_open();
    int		reset();

protected:
    int		file_updated(std::string&);
    int		open();
    void		configure_properties();

    std::string 		m_sPath;
    frame			m_rFrame;
    cv::VideoCapture	m_vcMedia;
};

}

#endif /* __media_h__ */
