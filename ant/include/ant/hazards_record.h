#ifndef __HAZARDSRECORD_H__
#define __HAZARDSRECORD_H__

#include <ant/collection.h>

#include <string>

namespace ant
{

class gps_record;

#define HAZARDS_RECORD_TYPE ("hazards_record")

class hazards_record : public record
{
public:
    hazards_record();
    virtual		~hazards_record();

    virtual		record*		new_collection_record() const;
    virtual		void		dump() const;
    virtual 	int 		size() const;
    virtual 	ucid_t	 	ucid() const;
    virtual		int			update_from(record*);
    virtual		int			log(bool force = false) const;
    static 		std::string 	type();
    static		ucid_t		record_ucid();

    double longitude(int idx) const;
    double latitude(int idx) const;
    double radius(int idx) const;

    void add_hazard(double lat, double lon, double rad);
    void remove(int idx);

    double bearing(int idx) const;
    double distance(int idx) const;

    void set_rel_position(gps_record* record);
    gps_record* rel_position() const;

protected:
    gps_record*	m_grPosition;

    struct hazard_values {
        double lat, lon, rad;
    };

    std::vector<hazard_values>	m_vhHazards;
};

}

#endif /* __HAZARDSRECORD_H__ */
