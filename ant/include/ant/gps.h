#ifndef __GPS_H__
#define __GPS_H__

#include <ant/filter.h>
#include <ant/input.h>
#include <ant/gps_record.h>
#include <ant/sim_timer.h>

#include <nmea/parser.h>

#include <iostream>
#include <fstream>
#include <vector>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#if CONFIG_GPSD
#include <libgpsmm.h>
#endif

namespace ant
{

/**
 *  @class gps gps.h "ant/gps.h"
 *  @brief gps is the class used to communicate with a GPS device.
 *
 *  Currently, gps uses libgpsd to communicate with GPS devices on the system.
 */
class gps : public filter
{
    declare_factory_class(gps, filter);
public:
    gps();
    ~gps();

    virtual	int		process();
    int		is_open();

protected:
    int		open();

protected:
    bool		m_bOpen;
    gps_record	m_rGps;
#if CONFIG_GPSD
    gpsmm		m_gGpsSession;
#endif
};

}

#endif /* __GPS_H__ */
