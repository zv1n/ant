#ifndef __COMMAND_RECORD_H__
#define __COMMAND_RECORD_H__

#include <ant/collection.h>
#include <string>

namespace ant
{

#define CMD_RECORD_TYPE_NAME ("Command Record")

/* @class command_record command_record.h "ant/command_record.h"
 * @brief Describes a particular command sent to the rover
 *
 *	This is used to store the commands which are sent to the rover.
 */

class command_record : public record
{
public:
    command_record();
    virtual ~command_record();
    virtual record* new_collection_record() const;
    virtual void dump() const;
    virtual int size() const;
    virtual int update_from(record*);
    virtual int log(bool force = false) const;
    static std::string type();

    void set_turn(int16_t turn);
    void set_throttle(int16_t throttle);
    void set_brake(int16_t brake);
    void set_estop(uint16_t estop);
    int16_t turn() const;
    int16_t throttle() const;
    int16_t brake() const;
    uint16_t estop() const;

private:
    struct comm_packet {
        int16_t turn;
        int16_t throttle;
        int16_t brake;
        uint16_t estop;
    };

    struct comm_packet m_sData;
};

}

#endif /* __NAVRECORD_H__ */
