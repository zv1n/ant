#ifndef __GLOBAL_HAZARD_COLLECTOR_H__
#define __GLOBAL_HAZARD_COLLECTOR_H__

#include <ant/collector.h>
#include <ant/filter.h>
#include <ant/std_records.h>
#include <ant/hazards_record.h>
#include <ant/gps_record.h>
#include <ant/frame.h>

namespace ant
{

#define HAZARD_MAX_DISTANCE (150.0)

/**
 *  @class global_hazard_collector global_hazard_collector.h "ant/global_hazard_collector.h"
 *  @brief global_hazard_collector takes a list of global hazards and creates an aggregate hazard image for fuzzy analysis.
 *
 */
class global_hazard_collector : public collector
{
    declare_factory_class(global_hazard_collector, collector);
public:
    global_hazard_collector();
    virtual		~global_hazard_collector();
    virtual		std::string	input_name(int i);
    virtual	int	process();

protected:
	int render_hazards(integer_array_record&);

protected:
    filter*					m_ghInput;
    filter*					m_gpsInput;
    filter*					m_navInput;
    hazards_record 			m_hrHazards;
    integer_array_record	m_iaDistances;
	frame					m_rFrame;
};

}

#endif /* __GLOBAL_HAZARD_COLLECTOR_H__ */
