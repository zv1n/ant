#ifndef __GPS_SIM_H__
#define __GPS_SIM_H__

#include <ant/filter.h>
#include <ant/input.h>
#include <ant/gps_record.h>
#include <ant/sim_timer.h>

#include <nmea/parser.h>

#include <iostream>
#include <fstream>
#include <vector>

#include <opencv/cv.h>
#include <opencv/highgui.h>

namespace ant
{

#define GPS_SIM_UCID (0x20003)

class gps_sim : public filter
{
    declare_factory_class(gps_sim, filter);
public:
    gps_sim();
    ~gps_sim();

    virtual	int		process();
    int		set_file(std::string path);
    int		is_open();
    float	to_degrees(float ft);

protected:
    int		file_updated(std::string& propname);
    int		open();
    void	configure_properties();

protected:
    std::string	m_sPath;
    nmeaPARSER	m_npParser;
    sim_timer	m_stTimer;
    gps_record	m_rGps;
	bool		m_bIsNmea;
    std::ifstream	m_fFile;
};

}

#endif /* __GPS_SIM_H__ */
