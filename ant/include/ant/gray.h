#ifndef __GRAY_H__
#define __GRAY_H__

#include <ant/factory.h>
#include <ant/filter.h>
#include <ant/frame.h>

#include <string>

using std::string;

namespace ant
{

/**
 *  @class gray gray.h "ant/gray.h"
 *  @brief gray takes an input frame_record and outputs a gray frame record.
 *
 *  The gray filter uses the OpenCV library to apply a gray filter to an
 *  input frame record which can be fetched at output.
 */
class gray : public filter
{
    declare_factory_class(gray, filter);

public:
    gray();
    virtual 		~gray();
    virtual int	process();
    virtual	string  input_name(int i) const;
protected:
    /**
     *	@var m_fInput
     *	Input filter on which the gray filter is applied.
     */
    ant::filter* 	m_fInput;

    /**
     *	@var m_rFrame
     *	Frame record in which the filtered frame is stored.
     */
    frame			m_rFrame;
};

}

#endif
