#ifndef __factory_h__
#define __factory_h__

#include <stdint.h>

#include <string>
#include <vector>

#include <iostream>

#if DEBUG_DUMP
#define DUMP(...) printf("DUMP: "__VA_ARGS__)
#else
#define DUMP(...)
#endif

#if DEBUG_FACTORY
#define FACTORY_DEBUG(...) printf("FACTORIES: " __VA_ARGS__)
#else
#define FACTORY_DEBUG(...)
#endif

/**
 *  @def VALID
 *  @brief Defined as 0x0. Represents a valid or succesful return.
 */
#define VALID			(0x0UL)

/**
 *  @def INVALID
 *  @brief Defined as ~0x0 (0xFFFFFFFF). Represents an invalid or failed return.
 */
#define INVALID			(-1)

/**
 *  @typedef uint64_t ucid_t
 *  @brief Define ucid_t tyiope to be 64 bits long.
 */
typedef uint64_t ucid_t;

namespace ant
{

class fitler;
class element;

/**
 *  @struct info
 *  @brief Factory info struct used to store each factory classes constructor.
 */
template <class T>
struct info {
    T* (*creator)();
    std::string name;
    ucid_t ucid;
};

void init();

}

/**
 *  @def define_enum_func(cls, factory)
 *  @brief Defines the factory enumeration function for the given factory.
 *  @param cls - Base class of members of this factory.
 *  @param factory - Name of factory.
 */
#define define_enum_func(cls, factory) \
void    enumerate_##factory##s(std::vector<std::string>& strs) { \
        strs.clear(); \
        std::vector<info<cls> >::iterator it; \
        for (it=factory##_info.begin(); it<factory##_info.end(); it++)\
                strs.push_back((*it).name); \
		FACTORY_DEBUG("Addr(" #factory "): %p\n", &factory##_info);\
}

/**
 *  @define define_ucid_func(cls, factory)
 *  @brief Retrieves the name of a class given a UCID.
 *  @param cls - Base class of members of this factory.
 *  @param factory - Name of factory.
 */
#define define_ucid_func(cls, factory) \
bool    get_##factory##_name(const ucid_t ucid, std::string& strs) { \
        std::vector<info<cls> >::iterator it; \
        for (it=factory##_info.begin(); it<factory##_info.end(); it++)\
			if (ucid == (*it).ucid) { \
				strs = (*it).name; \
				return true; \
			} \
	return false; \
}


/**
 *  @define define_create_func(cls, factory)
 *  @brief Defines the create function for the specified factory.
 *
 *  The function, once generated for a factory, will take a class name and
 *  instantiate a variable of its type.
 *
 *  @param cls - Base class of members of this factory.
 *  @param factory - Name of factory.
 */
#define define_create_func(cls, factory) \
cls* create_##factory (const std::string& name) {\
        std::vector<info<cls> >::iterator it; \
        for (it=factory##_info.begin(); it<factory##_info.end(); it++) {\
		if ((*it).name == name) { \
			FACTORY_DEBUG("Creating " #factory " '%s'...\n", name.c_str()); \
			cls* (*create)() = (*it).creator; \
			return (create()); \
		} \
	} \
	FACTORY_DEBUG("Failed to find " #factory ": '%s'\n", name.c_str()); \
	return NULL; \
}


/**
 *  @define define_add_func(cls, factory)
 *  @brief Defines the add function for a given factory.
 *  @param cls - Base class of members of this factory.
 *  @param factory - Name of factory.
 */
#define define_add_func(cls, factory) \
bool add_##factory(info<cls> xinfo) { \
        std::vector<info<cls> >::iterator it; \
        for (it=factory##_info.begin(); it<factory##_info.end(); it++) {\
                if ((*it).name == xinfo.name)\
			return false; \
		FACTORY_DEBUG("Checking: '%s'\n", (*it).name.c_str()); \
	}\
	FACTORY_DEBUG("Adding(" #factory ": %d): '%s'\n", factory##_info.size(), xinfo.name.c_str()); \
        factory##_info.push_back(xinfo); \
	FACTORY_DEBUG("Addr: %p", &factory##_info); \
	return true;\
}

/*************************************************************
**                     Factory Macros                       **
*************************************************************/

/**
 *  @def declare_property_class()
 *  @brief Declares the functions required for a propertyset based class.
 *
 *  Declares the functions, currently only ucid(), which are required by all
 *  classes derived from propertyset.
 */
#define declare_property_class() \
	static ucid_t ucid();

/**
 *  @def define_property_class(cls, uucid)
 *  @brief Defines the functions required for a propertyset based class.
 *
 *  Defines the functions, currently only ucid(), which are required by all
 *  classes derived from propertyset.
 *
 *  @param cls - The class which is inheriting propertyset
 *  @param uucid - The ID of the class specified class
 */
#define define_property_class(cls, uucid) \
	ucid_t cls::ucid() { return uucid; }

/**
 *  @def declare_factory_class(cls, base)
 *  @brief Declares the functions required for a factory member class.
 *
 *  @param cls - The class which is to be a member of a factory
 *  @param base - The base class of the given class
 */
#define declare_factory_class(cls, base) \
	public: \
	static base* 		create_##cls(); \
	static std::string	base_name(); \
	static std::string	type_name(); \
	static std::string	description(); \
	static ucid_t		ucid(); \
	protected: \
	static std::string	m_sDescription

/**
 *  @def define_factory_class_description()
 *  @brief Defines optional functions used to describe each class.
 *
 *  @param cls - The class which is to be a member of a factory
 *  @param desc - The text description of the class factory
 */
#define define_factory_class_description(cls, desc)\
	std::string cls::description() { return cls::m_sDescription; };\
	std::string cls::m_sDescription = desc

#if OPTIMIZE_CC
#define autoregister_factory_class(cls, uucid, base, factory) /*do nothing...*/
#define register_factory_class(cls, base, factory) { \
	info<base> xinfo = { \
		cls::create_##cls, \
		#cls, \
		cls::ucid() \
	}; \
	if (!add_##factory(xinfo)) { \
		FACTORY_DEBUG("Duplicate " #factory " Factory Class: " #cls "\n");\
	}}
#else
#define autoregister_factory_class(cls, uucid, base, factory) \
	extern bool add_##factory(info<base>); \
	class cls##_adder { \
		public: \
		cls##_adder() { \
			info<base> xinfo = { \
				cls::create_##cls, \
				#cls, \
				uucid \
			}; \
			if (!add_##factory(xinfo)) { \
				FACTORY_DEBUG("Duplicate " #factory " Factory Class: " #cls "\n");\
			} \
		}; \
		~cls##_adder() {}; \
	}; \
	cls##_adder __##cls##_adder
#define register_factory_class(cls, base, factory) /* do nothing... */
#endif

/**
 *  @def define_factory_class(cls, base, uucid, factory)
 *  @brief Defines the functions necessary to establish a factory.
 *
 *  @param cls - The class which is to be a member of a factory
 *  @param base - The base class from which all members of this factory are
 * 		  derived.
 *  @param uucid - The UCID of this class.
 *  @param factory - The name of the factory associated with this class.
 */
#define define_factory_class(cls, base, uucid, factory) \
	base* cls::create_##cls() { cls* ret = new cls(); \
			return static_cast<base*>(ret); }\
	std::string cls::base_name() { return #base; } \
	std::string cls::type_name() { return #factory; } \
	ucid_t	cls::ucid() { return uucid; } \
	autoregister_factory_class(cls, uucid, base, factory);	


/**
 *  @def declare_factory(base, factory)
 *  @brief Declares the functions associated with this factory
 *  @param base - The base class of this factory.
 *  @param factory - The name of the factory associated with this class.
 */
#define declare_factory(base, factory) \
	void enumerate_##factory##s(std::vector<std::string>&); \
	base* create_##factory(const std::string& name); \
	bool get_##factory##_name(const ucid_t ucid, std::string& strs); \
	bool add_##factory(info<base>)

#if OPTIMIZE_CC
#define define_list_var(base, factory) \
	std::vector<info<base> >  factory##_info;
#else
#define define_list_var(base, factory) \
	char __##factory##__mem[sizeof(std::vector<info<base> >)] = {0};\
	std::vector<info<base> >  &factory##_info = *(std::vector<info<base> >*)\
		__##factory##__mem; 
#endif

/**
 *  @def define_factory(base, factory)
 *  @brief Defines the functions associated with this factory
 *  @param base - The base class of this factory.
 *  @param factory - The name of the factory associated with this class.
 */
#define define_factory(base, factory) \
	define_list_var(base, factory) \
	define_add_func(base, factory) \
	define_enum_func(base, factory) \
	define_ucid_func(base, factory) \
	define_create_func(base, factory)

#endif /* __factory_h__ */
