#ifndef __named_h__
#define __named_h__

#include <string>

/**
 *  @namespace ant
 *  Primary namespace of the Autonomous Navigation Toolkit API
`*/
namespace ant
{

/**
 *  @class named_type named.h "ant/named.h"
 *  @brief All main types are derived from named type.
 *
 *  Class named_type handles the names of every object in the ANT framework.
 */
class named_type
{
public:
    named_type():m_sName("") {}
    named_type(const named_type& type):m_sName(type.m_sName) {}
    std::string	name() const {
        return m_sName;
    }

    virtual	void	set_name(const std::string& str) {
        m_sName = str.substr(0, str.length());
    }

protected:
    std::string	m_sName;
};

}

#endif /* __named_h__ */

