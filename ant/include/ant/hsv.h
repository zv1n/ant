#ifndef __HSV_H__
#define __HSV_H__

#include <ant/factory.h>
#include <ant/filter.h>
#include <ant/frame.h>

#include <string>

using std::string;

namespace ant
{

/**
 *  @class hsv hsv.h "ant/hsv.h"
 *  @brief hsv takes an input frame_record and outputs a hsv frame_record.
 *
 *  The hsv filter uses the OpenCV library to apply a hsv filter to a
 *  input frame_record which can be fetched at output.
 */
class hsv : public filter
{
    declare_factory_class(hsv, filter);

public:
    hsv();
    virtual 		~hsv();
    virtual int	process();
    virtual	string  input_name(int i) const;
protected:
    /**
     *  @var m_fInput
     *  The input filter on which the smooth filter operates.
    */
    ant::filter* 	m_fInput;
    /**
     *  @var m_fFrame
     *  The frame record where the filter video frame is stored.
     */
    frame			m_rFrame;
};

}

#endif
