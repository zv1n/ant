#ifndef _GPS_BEARING_FUZ_H_
#define _GPS_BEARING_FUZ_H_

#include <ant/atypes.h>
#include <ant/fuzzy_control.h>
#include <ant/member_record.h>
#include <ant/std_records.h>
#include <ant/nav_record.h>

namespace ant
{

/**
 *  @class gps_bearing_fuzzifier gps_bearing_fuz.h "ant/gps_bearing_fuz.h"
 */
class gps_bearing_fuzzifier : public fuzzifier
{
    declare_factory_class(gps_bearing_fuzzifier, fuzzifier);

public:
    gps_bearing_fuzzifier();
    virtual		~gps_bearing_fuzzifier();

protected:
    virtual	int	preprocess();
    virtual	int	fuzzify();

protected:
    member_record	m_rGpsBearing;
};

}

#endif /* _GPS_BEARING_FUZ_H_ */
