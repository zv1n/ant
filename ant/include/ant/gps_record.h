#ifndef __GPSRECORD_H__
#define __GPSRECORD_H__

#include <ant/collection.h>
#include <opencv/cv.h>

#include <string>

namespace ant
{

#define GPS_RECORD_TYPE ("gps_record")

class gps_record : public record
{
public:
    gps_record();
    virtual		~gps_record();

    virtual		record*		new_collection_record() const;
    virtual		void		dump() const;
    virtual 	int 		size() const;
    virtual 	ucid_t	 	ucid() const;
    virtual		int			update_from(record*);
    virtual		int			log(bool force = false) const;
    static 		std::string 	type();
    static		ucid_t		record_ucid();

    double longitude() const;
    double latitude() const;
    double altitude() const;
    double reported_bearing() const;
    uint8_t hours() const;
    uint8_t minutes() const;
    uint8_t seconds() const;

    int	set_longitude(double lon);
    int	set_latitude(double lat);
    int	set_altitude(double alt);
    int	set_reported_bearing(double br);
    int  set_time(uint8_t h, uint8_t m, uint8_t s);

protected:
    double m_dLongitude;
    double m_dLatitude;
    double m_dAltitude;
    double m_dReportedBearing;
    uint8_t m_uHours;
    uint8_t m_uMinutes;
    uint8_t m_uSeconds;
};

}

#endif /* __GPSRECORD_H__ */
