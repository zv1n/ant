#include <iostream>

#include <ant/video_in.h>
#include <ant/filter.h>
#include <ant/collector.h>
#include <ant/input.h>

using namespace std;

int main(int argc, char** argv)
{
	(void)argc;(void)argv;

    vector<string> list;
    ant::enumerate_collectors(list);
	for (unsigned int x=0; x<list.size(); x++)
		cout << list[x] << endl;
		

    cout << "This application is used to test the linkage of the"
         " ant library..." << endl;

    ant::filter* filt2 = ant::create_filter("dimredux");
    cout << "Required: " << filt2->required_inputs() << endl;
    cout << "Requested: " << filt2->requested_inputs() << endl;

    list.clear();
    ant::enumerate_inputs(list);
    cout << "Input Count: " << list.size() << endl;

    ant::filter* filt = new ant::videoin();

    if (!filt) {
        cout << "Failed to create videoin! (antlink)" << endl;
    } else {

        dynamic_cast<ant::videoin*>(filt)->set_device(0);

        if (filt->process())
            cout << "Successfully Update Frames..." << endl;
        else
            cout << "Failed Update Frames..." << endl;
    }

    return 0;
}
